=== Mime Types Plus ===
Contributors: Katsushi Kawamori
Donate link: https://riverforest-wp.info/donate/
Tags: attachments, filename extention, file type, media, media library, mime, mime types
Requires at least: 3.0.1
Tested up to: 4.9
Stable tag: 1.11
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Add the mime type that can be used in the media library to each file type.

== Description ==

Add the mime type that can be used in the media library to each file type.

== Installation ==

1. Upload `mime-types-plus` directory to the `/wp-content/plugins/` directory
2. Activate the plugin through the 'Plugins' menu in WordPress

== Frequently Asked Questions ==

none

== Screenshots ==

1. Settings
2. Input extension and mime type.
3. Add extension and mime type.
4. Delete selected extension and mime type.

== Changelog ==

= 1.11 =
Removed unnecessary code.

= 1.10 =
Fixed fine problem.

= 1.09 =
Changed donate link.

= 1.08 =
Security measures.

= 1.07 =
Delete unnecessary code.

== Upgrade Notice ==

= 1.08 =
Security measures.

