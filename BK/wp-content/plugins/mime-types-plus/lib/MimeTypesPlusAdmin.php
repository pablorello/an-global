<?php
/**
 * Mime Types Plus
 * 
 * @package    Mime Types Plus
 * @subpackage MimeTypesPlusAdmin Management screen
/*  Copyright (c) 2015- Katsushi Kawamori (email : dodesyoswift312@gmail.com)
    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; version 2 of the License.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

$mimetypesplusadmin = new MimeTypesPlusAdmin();

class MimeTypesPlusAdmin {

	/* ==================================================
	 * Construct
	 * @since	1.10
	 */
	public function __construct() {

		add_filter( 'plugin_action_links', array($this, 'settings_link'), 10, 2 );
		add_action( 'admin_menu', array($this, 'plugin_menu') );
		add_action( 'admin_enqueue_scripts', array($this, 'load_custom_wp_admin_style') );

	}

	/* ==================================================
	 * Add a "Settings" link to the plugins page
	 * @since	1.0
	 */
	public function settings_link($links, $file) {
		static $this_plugin;
		if ( empty($this_plugin) ) {
			$this_plugin = 'mime-types-plus/mimetypesplus.php';
		}
		if ( $file == $this_plugin ) {
			$links[] = '<a href="'.admin_url('options-general.php?page=MimeTypesPlus').'">'.__( 'Settings').'</a>';
		}
		return $links;
	}

	/* ==================================================
	 * Settings page
	 * @since	1.0
	 */
	public function plugin_menu() {
		add_options_page( 'MimeTypesPlus Options', 'Mime Types Plus', 'upload_files', 'MimeTypesPlus', array($this, 'plugin_options') );
	}

	/* ==================================================
	 * Add Css and Script
	 * @since	1.0
	 */
	public function load_custom_wp_admin_style() {
		if ($this->is_my_plugin_screen()) {
			wp_enqueue_style( 'jquery-responsiveTabs', plugin_dir_url( __DIR__ ).'css/responsive-tabs.css' );
			wp_enqueue_style( 'jquery-responsiveTabs-style', plugin_dir_url( __DIR__ ).'css/style.css' );
			wp_enqueue_script('jquery');
			wp_enqueue_script( 'jquery-responsiveTabs', plugin_dir_url( __DIR__ ).'js/jquery.responsiveTabs.min.js' );
			wp_enqueue_script( 'mimetypesplus-js', plugin_dir_url( __DIR__ ).'js/jquery.mimetypesplus.js', array('jquery') );
		}
	}

	/* ==================================================
	 * For only admin style
	 * @since	1.0
	 */
	private function is_my_plugin_screen() {
		$screen = get_current_screen();
		if (is_object($screen) && $screen->id == 'settings_page_MimeTypesPlus') {
			return TRUE;
		} else {
			return FALSE;
		}
	}

	/* ==================================================
	 * Settings page
	 * @since	1.0
	 */
	public function plugin_options() {

		if ( !current_user_can( 'upload_files' ) )  {
			wp_die( __( 'You do not have sufficient permissions to access this page.' ) );
		}

		if( !empty($_POST) ) {
			$post_nonce_field = 'mimetypesplus_tabs';
			if ( isset($_POST[$post_nonce_field]) && $_POST[$post_nonce_field] ) {
				if ( check_admin_referer( 'mtp_settings', $post_nonce_field ) ) {
					$this->options_updated(intval($_POST['mimetypesplus_admin_tabs']));
				}
			}
		}

		?>
		<div class="wrap">
			<h2>Mime Types Plus</h2>
			<div id="mimetypesplus-tabs">
				<ul>
				<li><a href="#mimetypesplus-tabs-1">image</a></li>
				<li><a href="#mimetypesplus-tabs-2">audio</a></li>
				<li><a href="#mimetypesplus-tabs-3">video</a></li>
				<li><a href="#mimetypesplus-tabs-4">document</a></li>
				<li><a href="#mimetypesplus-tabs-5">spreadsheet</a></li>
				<li><a href="#mimetypesplus-tabs-6">interactive</a></li>
				<li><a href="#mimetypesplus-tabs-7">text</a></li>
				<li><a href="#mimetypesplus-tabs-8">archive</a></li>
				<li><a href="#mimetypesplus-tabs-9">code</a></li>
				<li><a href="#mimetypesplus-tabs-10"><?php _e('Donate to this plugin &#187;'); ?></a></li>
				</ul>
				<div id="mimetypesplus-tabs-1">
					<div class="wrap">
						<?php
						$this->main_form(1);
						?>
			  		</div>
		  		</div>

				<div id="mimetypesplus-tabs-2">
					<div class="wrap">
						<?php
						$this->main_form(2);
						?>
					</div>
				</div>

				<div id="mimetypesplus-tabs-3">
					<div class="wrap">
						<?php
						$this->main_form(3);
						?>
					</div>
				</div>

				<div id="mimetypesplus-tabs-4">
					<div class="wrap">
						<?php
						$this->main_form(4);
						?>
					</div>
				</div>

				<div id="mimetypesplus-tabs-5">
					<div class="wrap">
						<?php
						$this->main_form(5);
						?>
					</div>
				</div>

				<div id="mimetypesplus-tabs-6">
					<div class="wrap">
						<?php
						$this->main_form(6);
						?>
					</div>
				</div>

				<div id="mimetypesplus-tabs-7">
					<div class="wrap">
						<?php
						$this->main_form(7);
						?>
					</div>
				</div>

				<div id="mimetypesplus-tabs-8">
					<div class="wrap">
						<?php
						$this->main_form(8);
						?>
					</div>
				</div>

				<div id="mimetypesplus-tabs-9">
					<div class="wrap">
						<?php
						$this->main_form(9);
						?>
					</div>
				</div>

				<div id="mimetypesplus-tabs-10">
				<div class="wrap">
				<?php $this->credit(); ?>
				</div>
				</div>

			</div>
		</div>
		<?php

	}

	/* ==================================================
	 * Credit
	 */
	private function credit() {

		$plugin_name = NULL;
		$plugin_ver_num = NULL;
		$plugin_path = plugin_dir_path( __DIR__ );
		$plugin_dir = untrailingslashit($plugin_path);
		$slugs = explode('/', $plugin_dir);
		$slug = end($slugs);
		$files = scandir($plugin_dir);
		foreach ($files as $file) {
			if($file == '.' || $file == '..' || is_dir($plugin_path.$file)){
				continue;
			} else {
				$exts = explode('.', $file);
				$ext = strtolower(end($exts));
				if ( $ext === 'php' ) {
					$plugin_datas = get_file_data( $plugin_path.$file, array('name'=>'Plugin Name', 'version' => 'Version') );
					if ( array_key_exists( "name", $plugin_datas ) && !empty($plugin_datas['name']) && array_key_exists( "version", $plugin_datas ) && !empty($plugin_datas['version']) ) {
						$plugin_name = $plugin_datas['name'];
						$plugin_ver_num = $plugin_datas['version'];
						break;
					}
				}
			}
		}
		$plugin_version = __('Version:').' '.$plugin_ver_num;
		$faq = __('https://wordpress.org/plugins/'.$slug.'/faq', $slug);
		$support = 'https://wordpress.org/support/plugin/'.$slug;
		$review = 'https://wordpress.org/support/view/plugin-reviews/'.$slug;
		$translate = 'https://translate.wordpress.org/projects/wp-plugins/'.$slug;
		$facebook = 'https://www.facebook.com/katsushikawamori/';
		$twitter = 'https://twitter.com/dodesyo312';
		$youtube = 'https://www.youtube.com/channel/UC5zTLeyROkvZm86OgNRcb_w';
		$donate = __('https://riverforest-wp.info/donate/', $slug);

		?>
		<span style="font-weight: bold;">
		<div>
		<?php echo $plugin_version; ?> | 
		<a style="text-decoration: none;" href="<?php echo $faq; ?>" target="_blank"><?php _e('FAQ'); ?></a> | <a style="text-decoration: none;" href="<?php echo $support; ?>" target="_blank"><?php _e('Support Forums'); ?></a> | <a style="text-decoration: none;" href="<?php echo $review; ?>" target="_blank"><?php _e('Reviews', 'media-from-ftp'); ?></a>
		</div>
		<div>
		<a style="text-decoration: none;" href="<?php echo $translate; ?>" target="_blank"><?php echo sprintf(__('Translations for %s'), $plugin_name); ?></a> | <a style="text-decoration: none;" href="<?php echo $facebook; ?>" target="_blank"><span class="dashicons dashicons-facebook"></span></a> | <a style="text-decoration: none;" href="<?php echo $twitter; ?>" target="_blank"><span class="dashicons dashicons-twitter"></span></a> | <a style="text-decoration: none;" href="<?php echo $youtube; ?>" target="_blank"><span class="dashicons dashicons-video-alt3"></span></a>
		</div>
		</span>

		<div style="width: 250px; height: 180px; margin: 5px; padding: 5px; border: #CCC 2px solid;">
		<h3><?php _e('Please make a donation if you like my work or would like to further the development of this plugin.', $slug); ?></h3>
		<div style="text-align: right; margin: 5px; padding: 5px;"><span style="padding: 3px; color: #ffffff; background-color: #008000">Plugin Author</span> <span style="font-weight: bold;">Katsushi Kawamori</span></div>
		<button type="button" style="margin: 5px; padding: 5px;" onclick="window.open('<?php echo $donate; ?>')"><?php _e('Donate to this plugin &#187;'); ?></button>
		</div>

		<?php

	}

	/* ==================================================
	 * Update wp_options table.
	 * @since	1.0
	 */
	private function options_updated($tabs){

		$mimetypesplus_exts_mimes = array();
		if(isset($_POST['mimetypesplus_exts_mimes'])){ $mimetypesplus_exts_mimes = $_POST['mimetypesplus_exts_mimes']; }
		$mimetypesplus_ext = NULL;
		$mimetypesplus_mime = NULL;
		if(isset($_POST['mimetypesplus_ext'])){ $mimetypesplus_ext = $_POST['mimetypesplus_ext']; }
		if(isset($_POST['mimetypesplus_mime'])){ $mimetypesplus_mime = $_POST['mimetypesplus_mime']; }
		if ( !empty($mimetypesplus_ext) && !empty($mimetypesplus_mime) ) {
			$mimetypesplus_exts_mimes[$mimetypesplus_ext] = $mimetypesplus_mime;
		} elseif ( empty($mimetypesplus_ext) && empty($mimetypesplus_mime) ) {
			// skip
		} else {
			echo '<div class="notice notice-error is-dismissible"><ul><li>'.__('The input data is not enough.', 'mime-types-plus').'</li></ul></div>';
			return;
		}

		$mimetypesplus_deletes = array();
		if(isset($_POST['mimetypesplus_deletes'])){ $mimetypesplus_deletes = $_POST['mimetypesplus_deletes']; }
		if ( !empty($mimetypesplus_exts_mimes) ) {
			$this->update_data($tabs, $mimetypesplus_exts_mimes, $mimetypesplus_deletes);
			echo '<div class="notice notice-success is-dismissible"><ul><li>'.__('Settings').' --> '.__('Changes saved.').'</li></ul></div>';
		}

	}

	/* ==================================================
	 * @param	int		$tabs
	 * @return	none
	 * @since	1.0
	 */
	private function main_form($tabs){

		switch ($tabs) {
			case 1:
				$type = 'image';
				break;
			case 2:
				$type = 'audio';
				break;
			case 3:
				$type = 'video';
				break;
			case 4:
				$type = 'document';
				break;
			case 5:
				$type = 'spreadsheet';
				break;
			case 6:
				$type = 'interactive';
				break;
			case 7:
				$type = 'text';
				break;
			case 8:
				$type = 'archive';
				break;
			case 9:
				$type = 'code';
				break;
		}

		if ( $tabs == 1 ) {
			$scriptname = admin_url('options-general.php?page=MimeTypesPlus');
		} else {
			$scriptname = admin_url('options-general.php?page=MimeTypesPlus'.'#mimetypesplus-tabs-'.$tabs);
		}

		$mimetypesplus_settings = get_option('mimetypesplus_settings');
		$extentions = $this->scan_extensions();

		?>
		<form method="post" action="<?php echo $scriptname; ?>">
			<?php wp_nonce_field('mtp_settings', 'mimetypesplus_tabs'); ?>
			<input type="hidden" name="mimetypesplus_admin_tabs" value = "<?php echo $tabs; ?>">
			<?php submit_button( __('Update Mime Types', 'mime-types-plus'), 'primary', 'Submit', FALSE ); ?>
			<h3><?php _e('Mime Types', 'mime-types-plus'); ?> <?php echo _e('Add'); ?></h3>
			<div style="padding-top: 5px; padding-bottom: 5px;">
			<?php _e('Extension', 'mime-types-plus'); ?>:
			<input type="text" name="mimetypesplus_ext" size="4" />
			</div>
			<div style="padding-top: 5px; padding-bottom: 5px;">
			<?php _e('Mime Types', 'mime-types-plus'); ?>:
			<input type="text" name="mimetypesplus_mime" />
			</div>

			<?php
			$count = 0;
			foreach ( $extentions as $types ) {
				foreach ( $types as $ext => $mimetype ) {
					if ( wp_ext2type($ext) === $type ) {
						if ( $count == 0 ) {
							?>
							<div style="border-bottom: 1px solid; padding-top: 5px; padding-bottom: 5px;"></div>
							<h3><?php _e('Mime Types', 'mime-types-plus'); ?> <?php _e('List', 'mime-types-plus'); ?> & <?php _e('Delete'); ?></h3>
							<div style="padding-top: 5px; padding-bottom: 5px;">
							<input type="checkbox" id="group_mime-types-plus" class="mimetypesplus-checkAll"><?php _e('Select all'); ?>
							</div>
							<?php
						}
						if ( array_key_exists($ext, $mimetypesplus_settings[$type]) && $mimetypesplus_settings[$type][$ext] === $mimetype ) {
							?>
							<div style="padding-top: 5px; padding-bottom: 5px;">
							<input type="checkbox" name="mimetypesplus_deletes[]" class="group_mime-types-plus" value="<?php echo $ext; ?>" />
							<input type="hidden" name="mimetypesplus_exts_mimes[<?php echo $ext; ?>]" value="<?php echo $mimetype; ?>" />
							<?php echo $ext; ?> <?php echo $mimetype; ?>
							</div>
							<?php
						} else {
							?>
							<div style="padding-top: 5px; padding-bottom: 5px;">
							<input type="checkbox" disabled='disabled'>
							<?php echo $ext; ?> <?php echo $mimetype; ?>
							</div>
							<?php
						}
						++$count;
					}
				}
			}
			?>
			<input type="hidden" name="mimetypesplus_admin_tabs" value = "<?php echo $tabs; ?>">
			<?php submit_button( __('Update Mime Types', 'mime-types-plus'), 'primary', 'Submit', FALSE ); ?>
		</form>
		<?php

	}

	/* ==================================================
	 * @param	none
	 * @return	array	$extensions
	 * @since	1.0
	 */
	private function scan_extensions(){

		$extensions = array();
		global $user_ID;
		$mimes = get_allowed_mime_types($user_ID);

		foreach ($mimes as $extselect => $mime) {
			if( strpos( $extselect, '|' ) ){
				$extselects = explode('|',$extselect);
				foreach ( $extselects as $extselect2 ) {
					$extensions[wp_ext2type($extselect2)][$extselect2] = $mime;
				}
			} else {
				$extensions[wp_ext2type($extselect)][$extselect] = $mime;
			}
		}

		return $extensions;

	}

	/* ==================================================
	 * @param	int		$tabs
	 * @param	array	$mimetypesplus_exts_mimes
	 * @param	array	$mimetypesplus_deletes
	 * @return	none
	 * @since	1.0
	 */
	private function update_data($tabs, $mimetypesplus_exts_mimes, $mimetypesplus_deletes){

		switch ($tabs) {
			case 1:
				$type = 'image';
				break;
			case 2:
				$type = 'audio';
				break;
			case 3:
				$type = 'video';
				break;
			case 4:
				$type = 'document';
				break;
			case 5:
				$type = 'spreadsheet';
				break;
			case 6:
				$type = 'interactive';
				break;
			case 7:
				$type = 'text';
				break;
			case 8:
				$type = 'archive';
				break;
			case 9:
				$type = 'code';
				break;
		}

		$mimetypesplus_settings = get_option('mimetypesplus_settings');

		$ext_mime = array();
		$mimetypesplus_exts_mimes = $this->sanitize_array($mimetypesplus_exts_mimes);
		foreach ( $mimetypesplus_exts_mimes as $key => $value ) {
			$ext_mime[$key] = $value;
			if ( !empty($mimetypesplus_deletes) ) {
				$mimetypesplus_deletes = $this->sanitize_array($mimetypesplus_deletes);
				foreach ( $mimetypesplus_deletes as $mimetypesplus_delete ) {
					if ( $key === $mimetypesplus_delete ) {
						unset($ext_mime[$key]);
					}
				}
			}
		}

		$mimetypesplus_settings[$type] = $ext_mime;
		update_option( 'mimetypesplus_settings', $mimetypesplus_settings );

	}

	/* ==================================================
	* Sanitize Array
	* @param	array	$a
	* @return	string	$_a
	* @since	1.08
	*/
	private function sanitize_array($a) {

		$_a = array();
		foreach($a as $key=>$value) {
			if ( is_array($value) ) {
				$_a[$key] = $this->sanitize_array($value);
			} else {
				$_a[$key] = htmlspecialchars($value);
			}
		}

		return $_a;

	}

}

?>