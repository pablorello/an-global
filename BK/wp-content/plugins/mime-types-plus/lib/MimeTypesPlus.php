<?php
/**
 * Mime Types Plus
 * 
 * @package    Mime Types Plus
 * @subpackage MimeTypesPlus Main Functions
/*  Copyright (c) 2015- Katsushi Kawamori (email : dodesyoswift312@gmail.com)
    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; version 2 of the License.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

$mimetypesplus = new MimeTypesPlus();

class MimeTypesPlus {

	private $mimetypesplus_settings;

	/* ==================================================
	 * Construct
	 * @since	1.10
	 */
	public function __construct() {

		$this->mimetypesplus_settings = get_option('mimetypesplus_settings');

		add_filter( 'ext2type', array($this, 'custom_ext2type') );
		add_filter( 'upload_mimes', array($this, 'custom_upload_mimes') );

	}

	/* ==================================================
	* @param	array	$existing_mimes
	* @return	array	$existing_mimes
	* @since	1.0
	*/
	public function custom_upload_mimes ( $existing_mimes=array() ) {

		foreach ( $this->mimetypesplus_settings as $types ) {
			foreach ( $types as $ext => $mimetype ) {
				$existing_mimes[$ext] = $mimetype;
			}
		}

		return $existing_mimes;

	}

	/* ==================================================
	* @param	array	$stack_ext2type
	* @return	array	$stack_ext2type
	* @since	1.0
	*/
	public function custom_ext2type($stack_ext2type) {

		if ( !empty($this->mimetypesplus_settings) ) {
			$types_all = array_keys($this->mimetypesplus_settings);
			foreach ( $this->mimetypesplus_settings as $types ) {
				foreach ( $types as $ext => $mimetype ) {
					foreach ( $types_all as $type ) {
						if ( array_key_exists( $ext, $this->mimetypesplus_settings[$type] ) ) {
							if ( $this->mimetypesplus_settings[$type][$ext] === $mimetype ) {
								$stack_ext2type[$type][] = $ext;
							}
						}
					}
				}
			}
		}

		return $stack_ext2type;

	}

}

?>