
<?php get_header();
  $img_desk= get_field('imagen_nosotros_desk');
  ?>
   <link rel="stylesheet" href="<?php bloginfo('template_url');?>/css/estilosc.css?v=1">
    <div class="bannerInterna internaB" style="background-image: url('<?php echo $img_desk['url']; ?>')">

        <div class="container">

        <div class="row">
          <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
            <h1><?php echo get_the_title(); ?></h1>
          </div>
          <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6"></div>
          <div class="col-12 col-sm-12 col-md-1 col-lg-1 col-xl-1"></div>
          <div class="col-12 col-sm-12 col-md-5 col-lg-5 col-xl-5">
            <div class="texto"><?php echo get_field('texto_banner_investor'); ?></div>
          </div>
          <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6"></div>
       </div>

      </div>
    </div>
    <div class="fact">
        <div class="container">

            <div class="row titlecenter">
                <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 titlecorte">
                    <h2><?php echo get_field('titulo_vision'); ?></h2>
                </div>
            </div>

            <div class="row introfact">
                <div class="col-12 col-sm-12 col-md-12 col-lg-1 col-xl-1">
                </div>
                <div class="col-12 col-sm-12 col-md-12 col-lg-5 col-xl-5">
                    <?php $iconoFact = get_field('icono_vision'); ?>
                        <div class="icono"> <img src="<?php echo $iconoFact['url'] ?>" alt="<?php echo $iconoFact['alt'] ?>" class="svgImage">
                        </div>
                </div>
                <div class="col-12 col-sm-12 col-md-12 col-lg-5 col-xl-5">
                    <?php echo get_field('descripcion_vision'); ?>
                </div>
                <div class="col-12 col-sm-12 col-md-12 col-lg-1 col-xl-1">
                </div>
            </div>
        </div>
    </div>
    <div id="nexxus" class="investor">
      <?php
        $empresa = get_field('logotipo_nexxus');
        $logoEmpresa = get_field('logotipo_empresa',$empresa[0]->ID);
        $estructura = array('3ths','2ths','3ths','2ths','3ths');
      ?>
      <div class="container">
        <div class="row">
          <div class="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
              <h3><?php echo get_field('titulo_nexxus'); ?></h3>
              <div class="txtRI">
                <?php echo get_field('descripcion_nexxus'); ?>
              </div>
          </div>
          <div class="col-12 col-sm-12 col-md-8 col-lg-8 col-xl-8">
            <div class="row">
              <div class="col-12 col-sm-12 col-md-4 col-lg-3ths logotipo">
                <img src="<?php echo $logoEmpresa['url'] ?>" alt="<?php echo $logoEmpresa['alt'] ?>">
              </div>
              <?php
                  $argsa = array(
                      'post_type' => 'nexxus',
                      'order' => 'ASC'
                      );
                      $popularesa = query_posts($argsa);
                      $conteoProyectosa= count($popularesa);
                      for ($i=0; $i < $conteoProyectosa ; $i++) {
                       ?>
                        <div class="col-12 col-sm-12 col-md-4 col-lg-<?php echo $estructura[$i]?>">
                            <span class="ttDatos"><?php echo get_the_title($popularesa[$i]->ID); ?></span>
                            <div class="txtDatos"><?php echo get_field('texto_datos',$popularesa[$i]->ID); ?></div>
                        </div>
                      <?php } ?>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="investor" id="credit">
      <?php
        $empresa = get_field('logotipo_credit');
        $logoEmpresa = get_field('logotipo_empresa',$empresa[0]->ID);
      ?>
      <div class="container">
        <div class="row">
          <div class="col-12 col-sm-12 col-md-8 col-lg-8 col-xl-8">
            <div class="row">
              <?php
              $argsb = array(
                  'post_type' => 'credit_suisse',
                  'order' => 'ASC'
                  );
                  $popularesb = query_posts($argsb);
                  $conteoProyectosb= count($popularesb);
                  $estructura = array('2ths','3ths','3ths','3ths','2ths','3ths');
                  $chunkResult = array_chunk($popularesb, 2);
                  $idesTotal = array();
                  $chunkResult[0][2]["ID"]='especial';
        			for ($a=0; $a < count($chunkResult); $a++) {
               $idesTotal =  array_merge_recursive($idesTotal,$chunkResult[$a]);
              }
                  for ($b=0; $b < count($idesTotal); $b++) {
                   ?>
              <div class="col-12 col-sm-12 col-md-4 col-lg-<?php echo $estructura[$b];?>">
                <?php if($idesTotal[$b]->ID == null){ ?>
                    <img src="<?php echo $logoEmpresa['url'] ?>" alt="<?php echo $logoEmpresa['alt'] ?>">
                <?php } else{ ?>
                    <span class="ttDatos"><?php echo get_the_title($idesTotal[$b]->ID); ?></span>
                    <div class="txtDatos"><?php echo get_field('texto_datos',$idesTotal[$b]->ID); ?></div>
                <?php } ?>
              </div>
            <?php } ?>
            </div>
          </div>
          <div class="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
              <h3><?php echo get_field('titulo_credit'); ?></h3>
              <div class="txtRI">
                <?php echo get_field('descripcion_credit'); ?>
              </div>
          </div>
        </div>
      </div>
    </div>

    <div class="quotes">
      <div class="container">
        <div class="row">
          <?php
          $argsc = array(
              'post_type' => 'quotes',
              'order' => 'ASC'
              );
              $popularesc = query_posts($argsc);
              $conteoProyectosc= count($popularesc);
              for ($e=0; $e < $conteoProyectosc; $e++) {
                $empresa = get_field('empresa_quote',$popularesc[$e]->ID);
                $logoEmpresa = get_field('logotipo_empresa',$empresa[0]->ID);
                $equipo = get_field('equipo_autor',$popularesc[$e]->ID);
                if($equipo == true){
                  $persona = get_field('miembro_equipo',$popularesc[$e]->ID);
                  $nombre = get_the_title($persona[0]->ID);
                  $puesto = get_field('puesto',$persona[0]->ID);
                }else{
                  $nombre = get_field('autor',$popularesc[$e]->ID);
                  $puesto = get_field('puesto_quote',$popularesc[$e]->ID);
                }
           ?>
               <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                 <div class="row">
                   <div class="col-12 empresa <?php echo $empresa[0]->post_name ?>">
                     <img src="<?php echo $logoEmpresa['url'] ?>" alt="<?php echo $logoEmpresa['alt'] ?>">
                   </div>
                   <div class="col-12 textoQuote">
                     <?php echo get_field('texto_quote',$popularesc[$e]->ID); ?>
                   </div>
                   <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                      <span class="nombreQuote">
                        <?php echo $nombre; ?>
                      </span>
                   </div>
                   <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                      <span class="puestoQuote"><?php echo $puesto; ?></span>
                   </div>
                 </div>
               </div>
           <?php } ?>

        </div>
      </div>
    </div>
            <div class="contacta">
                <div class="container">
                    <div class="row">
                        <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                            <div class="txtContacta">
                                <h3><?php echo get_field('titulo_contact',1165); ?></h3></div>
                        </div>
                        <div class="col-12 col-sm-12 col-md-6 col-lg-3">
                            <div class="txtContacta">
                                <p><?php echo get_field('texto_contact',1165); ?></p>
                            </div>
                        </div>
                        <div class="col-12"> <a href="<?php echo get_the_permalink(187); ?>"><?php echo get_field('boton_contact',1165); ?></a> </div>
                    </div>
                </div>
                <div class="triangleCornerBottom dark">
                    <svg xmlns="http://www.w3.org/2000/svg" version="1.1" viewBox="0 0 10 10" preserveAspectRatio="none" class="triangleCornerBottom">
                        <polygon class="fillTriangle" points="1,10 10,1 10,10"></polygon>
                    </svg>
                </div>
            </div>
            <?php get_footer(); ?>
