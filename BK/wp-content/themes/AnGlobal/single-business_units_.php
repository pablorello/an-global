<?php
get_header(); ?>
    <!-- banner de practica-->
    <div class="entry">
        <?php
      $imagenEntry = get_field('imagen_fondo_unidades');
   ?> <img src="<?php echo $imagenEntry['url']; ?>" alt="<?php echo $imagenEntry['alt']; ?>">
            <div class="entryItem">
                <div class="container">
                    <div class="textoEntry" style="background-image: linear-gradient(67deg, <?php echo get_field('color_a'); ?>, <?php echo get_field('color_b'); ?>);">
                        <div class="parrafo">
                            <?php echo get_field('descripcion_unidades'); ?>
                        </div>
                    </div>
                </div>
            </div>
    </div>
    <!-- banner logo de practica-->
    <div class="jumbotron jumbotron-fluid unity">
        <?php
    $imgClaim= get_field('claim_unidades');
  ?>
            <div class="container text-center"> <img src="<?php echo $imgClaim['url'] ?>" alt="<?php echo $imgClaim['alt'] ?>" class="svgImage"> </div>
    </div>
    <!-- caso de exito -->
    <div class="container-fluid casoExito">
        <div class="triangleCornerTop">
            <svg xmlns="http://www.w3.org/2000/svg" version="1.1" viewBox="0 0 10 10" preserveAspectRatio="none" class="triangleCornerTop">
                <polygon class="fillTriangle" points="0,0 8,0 0,8"></polygon>
            </svg>
        </div>
        <div class="container">
            <?php
        $casoExito = get_field('casos_exito_unidades');
      ?>
                <div class="row">
                    <div class="col-12 col-sm-12 col-md-12 col-lg-7"> </div>
                    <div class="col-12 col-sm-12 col-md-12 col-lg-5">
                        <div class="tittleSeccion ladoB black">
                            <h2>CASO DE ÉXITO</h2>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12 col-sm-12 col-md-7 col-lg-7">
                        <div class="videoSuccess">
                            <?php
                  $imgCaso = get_field('imagen_caso',$casoExito[0]->ID);
                 ?> <img src="<?php echo $imgCaso['url']?>" alt="<?php echo $imgCaso['alt']?>"> </div>
                        <div class="customerLogo">
                            <div class="triangleCornerLogoA">
                                <svg xmlns="http://www.w3.org/2000/svg" version="1.1" viewBox="0 0 10 10" preserveAspectRatio="none" class="triangleCornerLogoA">
                                    <polygon class="fillTriangleLogoA" points="0,0 9,0 0,10"></polygon>
                                </svg>
                            </div>
                            <?php
                  $clienteField = get_field('cliente_caso',$casoExito[0]->ID);
                  $imgCliente = get_field('logotipo_clientes',$clienteField[0]->ID);
                ?>
                                <!-- logo cliente--><img src="<?php echo $imgCliente['url'] ?>" alt="<?php echo $imgCliente['alt'] ?>">
                                <div class="triangleCornerLogo">
                                    <svg xmlns="http://www.w3.org/2000/svg" version="1.1" viewBox="0 0 10 10" preserveAspectRatio="none" class="triangleCornerLogo">
                                        <polygon class="fillTriangleLogo" points="1,10 10,0 10,10"></polygon>
                                    </svg>
                                </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-12 col-md-5 col-lg-5">
                        <h3>PROYECTO</h3>
                        <div class="proyectoCE">
                            <?php echo get_field('proyecto_caso',$casoExito[0]->ID); ?>
                        </div>
                        <h3>OBJETIVO</h3>
                        <div class="objetivoCE">
                            <?php echo get_field('objetivo_caso',$casoExito[0]->ID); ?>
                        </div>
                        <h3>SOLUCIONES</h3>
                        <div class="solucionesCE">
                            <?php echo get_field('soluciones_caso',$casoExito[0]->ID); ?>
                        </div>
                    </div>
                </div>
        </div>
        <div class="triangleCornerBottom">
            <svg xmlns="http://www.w3.org/2000/svg" version="1.1" viewBox="0 0 10 10" preserveAspectRatio="none" class="triangleCornerBottom">
                <polygon class="fillTriangle" points="1,10 10,1 10,10"></polygon>
            </svg>
        </div>
    </div>
    <!-- soluciones -->
    <div class="container solutions">
        <div class="row">
            <div class="col-12 col-sm-12 col-md-6 col-lg-4">
                <div class="cardTitle tittleSeccion LadoA">
                    <h2>SOLUCIONES</h2>
                    <div class="lineSolutions">
                        <svg xmlns="http://www.w3.org/2000/svg" version="1.1" viewBox="0 0 10 10" preserveAspectRatio="none" class="lineSolutions">
                            <polyline class="fillLineSolutions" points="0,0 8,0 "></polyline>
                        </svg>
                    </div>
                    <!--                <div class="clearfix"></div>--></div>
            </div>
            <?php
          $solucionesUnidad = get_field('soluciones_unidades');
          for ($i=0; $i < count($solucionesUnidad); $i++) {
        ?>
                <!-- tarjetas soluciones -->
                <div class="col-12 col-sm-12 col-md-6 col-lg-4">
                    <div class="container card">
                        <div class="iconSolU">
                            <?php
                    $iconoSol= get_field('iconos_soluciones',$solucionesUnidad[$i]->ID);
                  ?> <img src="<?php echo $iconoSol['url'] ?>" alt="<?php echo $iconoSol['alt'] ?>" class="svgImage"> </div>
                        <div>
                            <h3><?php echo get_field('titulo_soluciones',$solucionesUnidad[$i]->ID); ?></h3>
                            <div class="txtSolUn">
                                <?php echo get_field('descripcion_soluciones',$solucionesUnidad[$i]->ID); ?>
                            </div>
                            <!--                            <a href="<?php echo get_permalink($solucionesUnidad[$i]->ID); ?>" class="linkLine">VER MÁS</a>--></div>
                    </div>
                </div>
                <?php } ?>
        </div>
    </div>
    <!-- clientes -->
    <div class="container-fluid clientes">
        <div class="triangleCornerTop">
            <svg xmlns="http://www.w3.org/2000/svg" version="1.1" viewBox="0 0 10 10" preserveAspectRatio="none" class="triangleCornerTop">
                <polygon class="fillTriangle" points="0,0 8,0 0,8"></polygon>
            </svg>
        </div>
        <div class="container clientesLogos">
            <div class="row">
                <div class="col-12 col-sm-12 col-md-12 col-lg-8"> </div>
                <div class="col-12 col-sm-12 col-md-12 col-lg-4">
                    <div class="tittleSeccion ladoB">
                        <h2>Clientes</h2>
                        <!--                        <div class="clearfix"></div>--></div>
                </div>
            </div>
            <div class="row">
                <?php
        $clientesUnidad = get_field('clientes_unidades');
        $num = 0;
        for ($a=0; $a < count($clientesUnidad); $a++) {
          $num++;
          $iconoCli= get_field('logotipo_clientes',$clientesUnidad[$a]->ID);
          $middle = ($num == 2) ? 'middleItem' : '';
      ?>
                    <div class="col-6 col-sm-6 col-md-4 col-lg-4">
                        <div class="<?php echo $middle; ?>"> <img src="<?php echo $iconoCli['url'] ?>" alt="<?php echo $iconoCli['alt'] ?>"> </div>
                    </div>
                    <?php
        $num = ($num == 3)?0:$num;
     }
      ?>
            </div>
        </div>
        <div class="triangleCornerBottom">
            <svg xmlns="http://www.w3.org/2000/svg" version="1.1" viewBox="0 0 10 10" preserveAspectRatio="none" class="triangleCornerBottom">
                <polygon class="fillTriangle" points="1,10 10,1 10,10"></polygon>
            </svg>
        </div>
    </div>
    <?php
$certificacionUnidad = get_field('certificaciones_unidades');
if (!empty($certificacionUnidad)) {
?>
        <!-- Certificaciones -->
        <div class="container-fluid seccionLogos">
            <div class="container certificacionesLogos">
                <div class="row">
                    <div class="col-12 col-sm-12 col-md-12 col-lg-8">
                        <div class="tittleSeccion LadoA">
                            <h2>Certificaciones</h2>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-12 col-md-12 col-lg-4"></div>
                </div>
                <div class="row">
                    <?php

        for ($a=0; $a < count($certificacionUnidad); $a++) {
          $iconoCli= get_field('logotipo_ct',$certificacionUnidad[$a]->ID);
      ?>
                        <div class="col-6 col-sm-6 col-md-3 col-lg-3">
                            <div> <img src="<?php echo $iconoCli['url'] ?>" alt="<?php echo $iconoCli['alt'] ?>"> </div>
                        </div>
                        <?php
     }
      ?>
                </div>
            </div>
        </div>
        <?php
}
$tecUnidad = get_field('tecnologias_unidades');
if (!empty($tecUnidad)) {

?>
            <!-- tecnologias -->
            <div class="container-fluid seccionLogos tecnologias">
                <div class="triangleCornerTop">
                    <svg xmlns="http://www.w3.org/2000/svg" version="1.1" viewBox="0 0 10 10" preserveAspectRatio="none" class="triangleCornerTop">
                        <polygon class="fillTriangle" points="0,0 8,0 0,8"></polygon>
                    </svg>
                </div>
                <div class="container tecnologiasLogos">
                    <div class="row">
                        <div class="col-12 col-sm-12 col-md-12 col-lg-8"></div>
                        <div class="col-12 col-sm-12 col-md-12 col-lg-4">
                            <div class="tittleSeccion">
                                <h2>Tecnologías</h2>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <?php

        for ($b=0; $b < count($tecUnidad); $b++) {
          $iconoCli= get_field('logotipo_ct',$tecUnidad[$b]->ID);
      ?>
                            <div class="col-6 col-sm-6 col-md-3 col-lg-3">
                                <div> <img src="<?php echo $iconoCli['url'] ?>" alt="<?php echo $iconoCli['alt'] ?>"> </div>
                            </div>
                            <?php
     }
      ?>
                    </div>
                </div>
                <div class="triangleCornerBottom">
                    <svg xmlns="http://www.w3.org/2000/svg" version="1.1" viewBox="0 0 10 10" preserveAspectRatio="none" class="triangleCornerBottom">
                        <polygon class="fillTriangle" points="1,10 10,1 10,10"></polygon>
                    </svg>
                </div>
            </div>
            <?php
}

$metoUnidad = get_field('metodologias_unidades');
if(!empty($metoUnidad)){
 ?>
                <!-- Certificaciones -->
                <div class="container-fluid seccionLogos">
                    <div class="container metoLogos">
                        <div class="row">
                            <div class="col-12 col-sm-12 col-md-12 col-lg-8">
                                <div class="tittleSeccion LadoA">
                                    <h2>METODOLOGÍAS</h2>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                            <div class="col-12 col-sm-12 col-md-12 col-lg-4"></div>
                        </div>
                        <div class="row">
                            <?php

        for ($c=0; $c < count($metoUnidad); $c++) {
          $iconoCli= get_field('logotipo_ct',$metoUnidad[$c]->ID);
      ?>
                                <div class="col-6 col-sm-6 col-md-4 col-lg-4">
                                    <div> <img src="<?php echo $iconoCli['url'] ?>" alt="<?php echo $iconoCli['alt'] ?>"> </div>
                                </div>
                                <?php
     }
      ?>
                        </div>
                    </div>
                </div>
                <?php } ?>
                    <!-- contacta-->
                    <?php
  $imContacto = get_field('imagen_contacto');
?>
                        <div class="contacta" style="background-image: url('<?php echo $imContacto['url']; ?>');">
                            <div class="container">
                                <div class="row">
                                    <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                                        <div class="txtContacta">
                                            <h3>CONTACTA CON UN<br>ESPECIALISTA</h3></div>
                                    </div>
                                    <div class="col-12 col-sm-12 col-md-6 col-lg-3">
                                        <div class="txtContacta">
                                            <p>Somos tu próximo socio de negocios</p></div>
                                    </div>
                                    <div class="col-12 col-sm-12 col-md-12 col-lg-3"></div>
                                    <div class="col-12 col-sm-12 col-md-12 col-lg-12">
                                            <a href="">Conócenos</a>
                                    </div>
                                </div>
                            </div>
                            <div class="triangleCornerBottom dark">
                                <svg xmlns="http://www.w3.org/2000/svg" version="1.1" viewBox="0 0 10 10" preserveAspectRatio="none" class="triangleCornerBottom">
                                    <polygon class="fillTriangle" points="1,10 10,1 10,10"></polygon>
                                </svg>
                            </div>
                        </div>
                        <?php get_footer(); ?>
