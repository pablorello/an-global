<?php get_header();
$img_desk= get_field('banner_desktop_tec');
$img_mob= get_field('banner_mobile_sol');
?>

<div class="bannerInterna internaB bannerBI" style="background-image: url('<?php echo $img_desk['url']; ?>');">

  <div class="container">
    <div class="row">
      <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
        <h1><?php echo get_field('titulo_tec');?></h1>
      </div>
      <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6"></div>
      <div class="col-12 col-sm-12 col-md-1 col-lg-1 col-xl-1"></div>
      <div class="col-12 col-sm-12 col-md-6 col-lg-5 col-xl-5">
        <div class="texto"><?php echo get_field('texto_tec');?></div>
      </div>
      <div class="col-12 col-sm-12 col-md-5 col-lg-6 col-xl-6"></div>
    </div>
  </div>
</div>
<div class="tecnologias tecnologiasb">
  <div class="container">
      <?php
        $unidades = get_field('selecciona_unidades');
        ?>
        <?php
        for ($i=0; $i < count($unidades); $i++) {
          $tecnologias = get_field('tecnologias_unidades',$unidades[$i]->ID);
          if (!empty($tecnologias)) {
          ?>
          <div class="row eachT">
            <div class="col-12 col-sm-12 col-md-12 col-lg-4 col-xl-4">
              <h2><?php echo str_replace('AN_','',get_the_title($unidades[$i]->ID)); ?></h2>
            </div>
            <div class="col-12 col-sm-12 col-md-12 col-lg-1 col-xl-8"></div>
            <div class="col-12 col-sm-12 col-lg-12">
              <?php
                $cliamE = get_field('claim_unidades',$unidades[$i]->ID);
              ?>
              <div class="claimTec">
                <img src="<?php echo $cliamE['url']; ?>" alt="<?php echo $cliamE['alt']; ?>">
              </div>
            </div>
            <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
              <div class="owl-carousel owl-theme gallery_carousel">

                <?php

                  for ($e=0; $e < count($tecnologias); $e++) {
                    if(!empty($tecnologias[$e])){
                    ?>
                    <div class="item">
                        <div class="contItem">
                          <?php
                            $imgTec =  get_field('logotipo_ct',$tecnologias[$e]->ID);
                            if(!empty($imgTec)){
                              ?>
                              <img src="<?php echo $imgTec['url']?>" alt="<?php echo $imgTec['alt']?>" data-toggle="tooltip" title="<?php echo get_the_title($tecnologias[$e]->ID) ?>">
                              <?php
                            }
                          ?>
                        </div>
                    </div>
                    <?php
                    }
                  }
                ?>
              </div>
            </div>
          </div>
          <?php
          }
        }
      ?>
  </div>
</div>
<div class="contacta">
  <div class="container">
      <div class="row">
          <div class="col-12 col-sm-12 col-md-6 col-lg-6">
              <div class="txtContacta">
                  <h3><?php echo get_field('titulo_contact',1165); ?></h3></div>
          </div>
          <div class="col-12 col-sm-12 col-md-6 col-lg-3">
              <div class="txtContacta">
                  <p><?php echo get_field('texto_contact',1165); ?></p></div>
          </div>
          <div class="col-12 col-sm-12 col-md-12 col-lg-3"></div>
          <div class="col-12 col-sm-12 col-md-12 col-lg-12">
                  <a href="<?php echo get_the_permalink(187); ?>"><?php echo get_field('boton_contact',1165); ?></a>
          </div>
      </div>
  </div>
</div>
<style media="screen">
  <?php
    for ($j=0; $j < count($unidades); $j++) {
      $color = get_field('color_de_unidad',$unidades[$j]->ID);
      echo "#".$unidades[$j]->post_name.' svg path{fill: '.$color.';stroke: '.$color.'; }';
    }
  ?>
</style>
<?php get_footer(); ?>
