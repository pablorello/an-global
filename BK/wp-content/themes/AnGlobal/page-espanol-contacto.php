<?php get_header();
$idHome = 200;
    $img_desk= get_field('banner_desktop_contacto');
  ?>
       <link rel="stylesheet" href="<?php bloginfo('template_url');?>/css/estilosc.css?v=1">
        <div class="bannerInterna internaB bannerBI" style="background-image: url('<?php echo $img_desk['url'];?>')">

        <div class="container">

        <div class="row">
          <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
        <h1><?php echo get_field('titulo_contacto');?></h1>
          </div>
          <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6"></div>
          <div class="col-12 col-sm-12 col-md-1 col-lg-1 col-xl-1"></div>
          <div class="col-12 col-sm-12 col-md-6 col-lg-5 col-xl-5">
            <div class="texto"><?php echo get_field('texto_contacto');?></div>
          </div>
          <div class="col-12 col-sm-12 col-md-5 col-lg-6 col-xl-6"></div>
    </div>

  </div>


</div>
        <div class="especialista">
            <div class="container">
                <div class="row">
                    <div class="col-12 col-sm-12 col-md-12 col-lg-5 col-xl-5">
                        <h2><?php echo get_field('titulo_especialista'); ?></h2>
                        <div class="texto">
                            <?php echo get_field('texto_especialista'); ?>
                        </div>
                    </div>
                    <div class="col-sm-0 col-md-1 col-lg-1 col-xl-1"></div>
                    <div class="col-12 col-sm-12 col-md-12 col-lg-6 col-xl-6">
                        <div class="row">
                            <?php
        $unidades = get_field('unidades_contacto');
        for ($i=0; $i < count($unidades); $i++) {
          ?>
                                <div class="col-12 colsm-12 col-md-6 col-lg-6 col-xl-6">
                                    <div id="<?php echo $unidades[$i]->post_name;?>" class="eachUnidad">
                                        <?php
                  $imgUnidad = get_field('logotipo_claim',$unidades[$i]->ID);
                  if(!empty($imgUnidad)){
                ?> <img src="<?php echo $imgUnidad['url']?>" alt="<?php echo $imgUnidad['alt']?>">
                                            <?php } ?>
                                                <hr>
                                                <ul>
                                                    <li>
                                                        <div class="iconos"> <span class="icon dripicons-mail"></span> </div>
                                                        <div class="info">
                                                            <a href="mailto:<?php echo get_field('correo_unidad',$unidades[$i]->ID); ?>" target="_blank"><?php echo get_field('correo_unidad',$unidades[$i]->ID); ?></a>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="iconos"> <span class="icon dripicons-phone"></span> </div>
                                                        <div class="info">
                                                            <?php echo get_field('telefono_unidad',$unidades[$i]->ID); ?>
                                                        </div>
                                                    </li>
                                                </ul>
                                    </div>
                                </div>
                                <?php } ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="container-fluid mapaSomos mapaContacto">
                <div class="triangleCornerTop">
                    <svg xmlns="http://www.w3.org/2000/svg" version="1.1" viewBox="0 0 10 10" preserveAspectRatio="none" class="triangleCornerTop">
                        <polygon class="fillTriangle" points="0,0 8,0 0,8"></polygon>
                    </svg>
                </div>
                <div class="somos">
                  <div class="container">
                    <div class="row">
                      <div class="col-12 col-sm-12 col-md-12 col-lg-12">

                                            <h2><?php echo get_field('titulo_tenemos_presencia'); ?></h2>
                      </div>
                      <div class="col-12">
                        <div class="mapa">
                          <div class="paises">

                            <div class="paisesListado">
                              <ul data-url="<?php echo get_home_url(); ?>/pais">
                                <?php
                                    $argsb = array(
                                          'post_type' => 'paises',
                                          'order' => 'ASC'
                                          );
                                        $popularesb = query_posts($argsb);
                                        $conteoProyectosb= count($popularesb);
                                          for ($j=0; $j < $conteoProyectosb; $j++) {
                                ?>
                                <li id="<?php echo $popularesb[$j]->ID; ?>" onclick="mapa(this)" class="<?php echo($popularesb[$j]->ID == 220)?'activoPais':''; ?>"><span><?php echo get_field('nomenclatura',$popularesb[$j]->ID); ?></span></li>
                              <?php } ?>
                              </ul>
                              <div class="clearfix"></div>
                            </div>
                            <div class="cambioMapa">
                              <?php
                                $mapa = get_field('mapa',220);
                                $mapaMob = get_field('mapa_mobile',220);
                                $oficina = get_field('oficina',220);
                              ?>
                              <img src="<?php echo $mapa['url']; ?>" alt="<?php echo $mapa['alt']; ?>" class="mapaDesk">
                              <img src="<?php echo $mapaMob['url']; ?>" alt="<?php echo $mapaMob['alt']; ?>" class="mapaMob">
                              <div class="infoOficina">
                                <span class="cerrarMapa cerrar" onclick="cerrarMapa(this)">X</span>
                                  <h6><?php echo get_the_title($oficina[0]->ID); ?></h6>
                                  <div class="dirM">
                                    <div class="iconM">
                                      <span class="icon dripicons-location"></span>
                                    </div>
                                    <div class="textM">
                                      <?php echo get_field('direccion_oficinas',$oficina[0]->ID); ?>
                                    </div>
                                    <div class="clearfix"></div>
                                  </div>
                                  <div class="mailM">
                                    <div class="iconM">
                                      <span class="icon dripicons-mail"></span>
                                    </div>
                                    <div class="textM">
                                      <?php echo get_field('correo_oficinas',$oficina[0]->ID); ?>
                                    </div>
                                    <div class="clearfix"></div>
                                  </div>
                                  <div class="telM">
                                    <div class="iconM">
                                      <span class="icon dripicons-phone"></span>
                                    </div>
                                    <div class="textM">
                                      <?php echo get_field('telefono_oficina',$oficina[0]->ID); ?>
                                    </div>
                                    <div class="clearfix"></div>
                                  </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>

                    </div>
                      </div>
                </div>
                <div class="triangleCornerBottom">
                    <svg xmlns="http://www.w3.org/2000/svg" version="1.1" viewBox="0 0 10 10" preserveAspectRatio="none" class="triangleCornerBottom">
                        <polygon class="fillTriangle" points="1,10 10,1 10,10"></polygon>
                    </svg>
                </div>
            </div>

        <div class="contacta">
            <div class="container">
                <div class="row">
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                        <div class="txtContacta">
                            <h3><?php echo get_field('titulo_work',1165); ?></h3></div>
                    </div>
                    <div class="col-12 col-sm-12 col-md-6 col-lg-4">
                       <div class="txtContacta">
                        <p><?php echo get_field('texto_werk',1165); ?></p> <a href="mailto:<?php echo get_field('mail_work',1165); ?>"><?php echo get_field('mail_work',1165); ?></a>
                    </div>
                    </div>
                    <div class="col-12 col-sm-12 col-md-12 col-lg-2"></div>
                </div>
            </div>
            <div class="triangleCornerBottom dark">
                    <svg xmlns="http://www.w3.org/2000/svg" version="1.1" viewBox="0 0 10 10" preserveAspectRatio="none" class="triangleCornerBottom">
                        <polygon class="fillTriangle" points="1,10 10,1 10,10"></polygon>
                    </svg>
                </div>
        </div>

        <?php get_footer(); ?>
