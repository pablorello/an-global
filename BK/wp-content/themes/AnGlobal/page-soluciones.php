<?php
  $ide = filter_var($_GET['ide'], FILTER_SANITIZE_STRING);
  $practica = filter_var($_GET['unidad'], FILTER_SANITIZE_STRING);
  $icono = get_field('iconos_soluciones',$ide);
  $beneficios = get_field('beneficios_ben_sol',$ide);
  $servicios = get_field('productos_ben',$ide);
  $permalink = get_the_permalink($ide);
  $ideGral = 1414;
  $benefs = array();
  for ($i=0; $i < count($beneficios) ; $i++) {
    $benefs[$i] = array(
      'nombre' => get_the_title($beneficios[$i]->ID),
      'icBen' => get_field('selecciona_icono',$beneficios[$i]->ID)
    );
  }
  $servs = array();
  for ($a=0; $a < count($servicios) ; $a++) {
    $servs[$a] = array(
      'nombre' => get_the_title($servicios[$a]->ID)
    );
  }
    $arr[] = array(
      'titulo' => get_the_title($ide),
      'icono' => $icono['url'],
      'descripcion' => get_field('texto_ben_sol',$ide),
      'colora' => get_field('color_a',$practica),
      'colorb' => get_field('color_b',$practica),
      'benTit' => get_field('titulo_beneficios',$ideGral),
      'beneficios' => $benefs,
      'servTit' => get_field('titulo_servicios',$ideGral),
      'servicios' => $servs,
      'link' => $permalink
    );
header('Content-type: application/json; charset=utf-8');
echo json_encode($arr);
exit();
?>
