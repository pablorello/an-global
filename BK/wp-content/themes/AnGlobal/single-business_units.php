<?php get_header();
$img_desk= get_field('imagen_fondo_unidades');
$img_mob= get_field('banner_mobile_sol');
?>
    <div class="bannerInterna internaB practicas" style="background-image: url('<?php echo $img_desk['url']; ?>');">

        <!-- banner de practica-->
        <div class="entry">
            <div class="entryItem">
                <div class="container">
                    <div class="row">
                        <div class="col-12 col-sm-12 col-md-12 col-lg-8 col-xl-8">
                          <img class="d-md-none" src="<?php echo $img_desk['url']; ?>" />
                            <div class="textoEntry" style="background-image: linear-gradient(67deg, <?php echo get_field('color_a'); ?>, <?php echo get_field('color_b'); ?>);">
                                <div class="parrafo">
                                    <?php $imgLogo = get_field('logotipo_experience'); ?> <img src="<?php echo $imgLogo['url'] ?>" alt="<?php echo $imgLogo['alt'] ?>" class="svgImage">
                                        <?php echo get_field('descripcion_unidades'); ?>
                                </div>
                            </div>

                        </div>
                        <div class="col-12 col-sm-12 col-md-3 col-lg-4 col-xl-4"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- banner logo de practica-->
    <div class="jumbotron jumbotron-fluid unity">
        <?php
    $imgClaim= get_field('claim_unidades');
  ?>
            <div class="container text-center"> <img src="<?php echo $imgClaim['url'] ?>" alt="<?php echo $imgClaim['alt'] ?>" class="svgImage"> </div>
    </div>
    <?php
$imgCaso = get_field('imagen_caso',$casoExito[0]->ID);
$webm = get_field('video_webm',$casoExito[0]->ID);
$ogg = get_field('video_ogg',$casoExito[0]->ID);
$mp4 = get_field('video_mp4',$casoExito[0]->ID);
if (!empty($webm) && !empty($ogg) && !empty($mp4)) { ?>
  <?php /*
    <!-- caso de exito -->
    <?php
$casoExito = get_field('casos_exito_unidades');
if (!empty($casoExito)) {

?>

    <div class="container-fluid casoExito">
        <div class="triangleCornerTop">
            <svg xmlns="http://www.w3.org/2000/svg" version="1.1" viewBox="0 0 10 10" preserveAspectRatio="none" class="triangleCornerTop">
                <polygon class="fillTriangle" points="0,0 8,0 0,8"></polygon>
            </svg>
        </div>
        <div class="container">

                <div class="row">
                    <div class="col-12 col-sm-12 col-md-7 col-lg-7"> </div>
                    <div class="col-12 col-sm-12 col-md-5 col-lg-5">
                        <div class="tittleSeccion ladoB black">
                            <h2><?php _e("[:es]CASOS DE ÉXITO[:en]CASE STUDIES"); ?></h2>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12 col-sm-12 col-md-7 col-lg-7">
                        <div class="videoSuccess">
                            <?php
                  $imgCaso = get_field('imagen_caso',$casoExito[0]->ID);
                  $webm = get_field('video_webm',$casoExito[0]->ID);
                  $ogg = get_field('video_ogg',$casoExito[0]->ID);
                  $mp4 = get_field('video_mp4',$casoExito[0]->ID);
                  if (!empty($webm) && !empty($ogg) && !empty($mp4)) { ?>
                                <a class="videoPlay" onclick="playVid(this)"><div class="icon dripicons-media-play"></div></a>
                                <video width="100%" id="myVideo" onclick="pauseVid()">
                                      <source src="<?php echo $mp4['url']; ?>" type="video/mp4">
                                      <source src="<?php echo $ogg['url']; ?>" type="video/ogg">
                                      <source src="<?php echo $webm['url']; ?>" type="video/webm"> Your browser does not support HTML5 video.
                                    </video>
                                <?php
                }else{
                  ?> <img src="<?php echo $imgCaso['url']?>" alt="<?php echo $imgCaso['alt']?>">
                                    <?php
                }
                 ?>
                        </div>
                        <div class="customerLogo">
                            <?php
                  $clienteField = get_field('cliente_caso',$casoExito[0]->ID);
                  $imgCliente = get_field('logotipo_clientes',$clienteField[0]->ID);
                ?>
                                <!-- logo cliente--><img src="<?php echo $imgCliente['url'] ?>" alt="<?php echo $imgCliente['alt'] ?>">
                                <div class="triangleCornerLogo">
                                    <svg xmlns="http://www.w3.org/2000/svg" version="1.1" viewBox="0 0 10 10" preserveAspectRatio="none" class="triangleCornerLogo">
                                        <polygon class="fillTriangleLogo" points="1,10 10,0 10,10"></polygon>
                                    </svg>
                                </div>
                        </div>
                    </div>

                    <div class="col-12 col-sm-12 col-md-5 col-lg-5">
                        <h3><?php _e("[:es]PROYECTO[:en]PROJECT"); ?></h3>
                        <div class="proyectoCE">
                            <?php echo get_field('proyecto_caso',$casoExito[0]->ID); ?>
                        </div>
                        <h3><?php _e("[:es]OBJETIVO[:en]GOALS"); ?></h3>
                        <div class="objetivoCE">
                            <?php echo get_field('objetivo_caso',$casoExito[0]->ID); ?>
                        </div>
                        <h3><?php _e("[:es]SOLUCIONES[:en]SOLUTIONS"); ?></h3>
                        <div class="solucionesCE">
                            <?php echo get_field('soluciones_caso',$casoExito[0]->ID); ?>
                        </div>
                    </div>

                </div>
        </div>
        <div class="triangleCornerBottom">
            <svg xmlns="http://www.w3.org/2000/svg" version="1.1" viewBox="0 0 10 10" preserveAspectRatio="none" class="triangleCornerBottom">
                <polygon class="fillTriangle" points="1,10 10,1 10,10"></polygon>
            </svg>
        </div>
    </div>
    */ ?>

    <?php }
    $solucionesUnidad = get_field('soluciones_unidades');
    if (!empty($solucionesUnidad)) {

     ?>

    <!-- soluciones -->
    <div class="container solutions solutionsDatos solucC" data-url="<?php echo get_home_url(); ?>/soluciones" data-practica="<?php echo get_the_ID(); ?>">
        <div class="row">
            <div class="col-12 col-sm-12 col-md-6 col-lg-4">
                <div class="cardTitle tittleSeccion LadoA">
                    <h2><?php _e("[:es]SOLUCIONES[:en]SOLUTIONS"); ?></h2>
                    <div class="clearfix"></div>
                </div>
            </div>
            <?php

          for ($i=0; $i < count($solucionesUnidad); $i++) {
        ?>
                <!-- tarjetas soluciones -->
                <div class="col-12 col-sm-12 col-md-6 col-lg-4">
                    <div class="container card">
                        <div class="iconSolU">
                            <?php
                    $iconoSol= get_field('iconos_soluciones',$solucionesUnidad[$i]->ID);
                  ?> <img src="<?php echo $iconoSol['url'] ?>" alt="<?php echo $iconoSol['alt'] ?>" class="svgImage"> </div>
                        <div><div class="titleCont">
                            <h3><?php echo get_the_title($solucionesUnidad[$i]->ID); ?></h3>
                          </div>
                            <div class="txtSolUn">
                                <?php echo get_field('descripcion_soluciones',$solucionesUnidad[$i]->ID); ?>
                            </div>
                        </div>
                        <a class="seeMore" onclick="showSolutions(this)" data-solution="<?php echo $solucionesUnidad[$i]->ID; ?>"><?php _e("[:es]Ver más[:en]More"); ?></a>
                    </div>
                </div>
                <?php } ?>
        </div>
    </div>
    <?php
      }
      $clientesUnidad = get_field('clientes_unidades');
      if (!empty($clientesUnidad)) {

     ?>
    <!-- clientes -->
    <div class="container-fluid clientes">
        <div class="triangleCornerTop">
            <svg xmlns="http://www.w3.org/2000/svg" version="1.1" viewBox="0 0 10 10" preserveAspectRatio="none" class="triangleCornerTop">
                <polygon class="fillTriangle" points="0,0 8,0 0,8"></polygon>
            </svg>
        </div>
        <div class="container clientesLogos">
            <div class="row">
                <div class="col-12 col-sm-12 col-md-12 col-lg-8"> </div>
                <div class="col-12 col-sm-12 col-md-12 col-lg-4">
                    <div class="tittleSeccion ladoB">
                        <h2 class="standard"><?php _e("[:es]Clientes[:en]Clients"); ?></h2>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
            <div class="row">
                <?php
        $clientesUnidad = get_field('clientes_unidades');
        $num = 0;
        for ($a=0; $a < count($clientesUnidad); $a++) {
          $num++;
          $iconoCli= get_field('logotipo_clientes',$clientesUnidad[$a]->ID);
          $middle = ($num == 2) ? 'middleItem' : '';
      ?>
                    <div class="col-6 col-sm-6 col-md-4 col-lg-4">
                        <div class="<?php echo $middle; ?>"> <img src="<?php echo $iconoCli['url'] ?>" alt="<?php echo $iconoCli['alt'] ?>"> </div>
                    </div>
                    <?php
        $num = ($num == 3)?0:$num;
     }
      ?>
            </div>
        </div>
        <div class="triangleCornerBottom">
            <svg xmlns="http://www.w3.org/2000/svg" version="1.1" viewBox="0 0 10 10" preserveAspectRatio="none" class="triangleCornerBottom">
                <polygon class="fillTriangle" points="1,10 10,1 10,10"></polygon>
            </svg>
        </div>
    </div>
    <?php }
      $certificacionUnidad = get_field('certificaciones_unidades');
      if (!empty($certificacionUnidad)) {
        // code...

     ?>
    <!-- Certificaciones -->
    <div class="container-fluid seccionLogos">
        <div class="container certificacionesLogos">
            <div class="row">
                <div class="col-12 col-sm-12 col-md-12 col-lg-8">
                    <div class="tittleSeccion LadoA">
                        <h2 class="standard"><?php _e("[:es]Certificaciones[:en]Certifications"); ?></h2>
                        <div class="clearfix"></div>
                    </div>
                </div>
                <div class="col-12 col-sm-12 col-md-12 col-lg-4"></div>
            </div>
            <div class="row">
                <?php

        for ($a=0; $a < count($certificacionUnidad); $a++) {
          $iconoCli= get_field('logotipo_ct',$certificacionUnidad[$a]->ID);
      ?>
                    <div class="col-6 col-sm-6 col-md-3 col-lg-3">
                        <div> <img src="<?php echo $iconoCli['url'] ?>" alt="<?php echo $iconoCli['alt'] ?>"> </div>
                    </div>
                    <?php
     }
      ?>
            </div>
        </div>
    </div>
    <?php }
    $tecUnidad = get_field('tecnologias_unidades');
    if(!empty($tecUnidad)){
     ?>
    <!-- tecnologias -->
    <div class="container-fluid seccionLogos tecnologias">
        <div class="triangleCornerTop">
            <svg xmlns="http://www.w3.org/2000/svg" version="1.1" viewBox="0 0 10 10" preserveAspectRatio="none" class="triangleCornerTop">
                <polygon class="fillTriangle" points="0,0 8,0 0,8"></polygon>
            </svg>
        </div>
        <div class="container tecnologiasLogos">
            <div class="row">
                <div class="col-12 col-sm-12 col-md-12 col-lg-6"></div>
                <div class="col-12 col-sm-12 col-md-12 col-lg-6">
                    <div class="tittleSeccion">
                        <h2 class="standard"><?php _e("[:es]Tecnologías[:en]Technologies"); ?></h2>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
            <div class="row">
                <?php

        for ($b=0; $b < count($tecUnidad); $b++) {
          $iconoCli= get_field('logotipo_ct',$tecUnidad[$b]->ID);
      ?>
                    <div class="col-6 col-sm-6 col-md-3 col-lg-3">
                        <div> <img src="<?php echo $iconoCli['url'] ?>" alt="<?php echo $iconoCli['alt'] ?>"> </div>
                    </div>
                    <?php
     }
      ?>
            </div>
        </div>
        <div class="triangleCornerBottom">
            <svg xmlns="http://www.w3.org/2000/svg" version="1.1" viewBox="0 0 10 10" preserveAspectRatio="none" class="triangleCornerBottom">
                <polygon class="fillTriangle" points="1,10 10,1 10,10"></polygon>
            </svg>
        </div>
    </div>
    <!-- Certificaciones -->
    <?php
  }
    $metoUnidad = get_field('metodologias_unidades');
    if (!empty($metoUnidad)) {

     ?>
    <div class="container-fluid seccionLogos">
        <div class="container metoLogos">
            <div class="row">
                <div class="col-12 col-sm-12 col-md-12 col-lg-8">
                    <div class="tittleSeccion LadoA">
                        <h2 class="standard"><?php _e("[:es]METODOLOGÍAS[:en]METHODOLOGIES"); ?></h2>
                        <div class="clearfix"></div>
                    </div>
                </div>
                <div class="col-12 col-sm-12 col-md-12 col-lg-4"></div>
            </div>
            <div class="row">
                <?php

        for ($c=0; $c < count($metoUnidad); $c++) {
          $iconoCli= get_field('logotipo_ct',$metoUnidad[$c]->ID);
      ?>
                    <div class="col-6 col-sm-6 col-md-4 col-lg-4">
                        <div> <img src="<?php echo $iconoCli['url'] ?>" alt="<?php echo $iconoCli['title'] ?>"> </div>
                      <p><?php echo get_the_title($metoUnidad[$c]->ID); ?></p>
                    </div>
                    <?php
     }
      ?>
            </div>
        </div>
    </div>
    <!-- contacta-->
    <?php
    }
  $imContacto = get_field('imagen_contacto');
?>
        <div class="contacta" style="background-image: url('<?php echo $imContacto['url']; ?>');">
            <div class="triangleCornerTop">
                <svg xmlns="http://www.w3.org/2000/svg" version="1.1" viewBox="0 0 10 10" preserveAspectRatio="none" class="triangleCornerTop">
                    <polygon class="fillTriangle" points="0,0 8,0 0,8"></polygon>
                </svg>
            </div>
            <div class="container">
                <div class="row">
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                        <h3><?php echo get_field('titulo_contact',1165); ?></h3> </div>
                    <div class="col-12 col-sm-12 col-md-6 col-lg-3">
                        <div class="txtContacta">
                            <p><?php echo get_field('texto_contact',1165); ?></p>
                        </div>
                    </div>
                    <div class="col-12">
                        <div> <a href="<?php echo get_the_permalink(187); ?>"><?php echo get_field('boton_contact',1165); ?></a> </div>
                    </div>
                </div>
            </div>
            <div class="triangleCornerBottom dark">
                <svg xmlns="http://www.w3.org/2000/svg" version="1.1" viewBox="0 0 10 10" preserveAspectRatio="none" class="triangleCornerBottom">
                    <polygon class="fillTriangle" points="1,10 10,1 10,10"></polygon>
                </svg>
            </div>
        </div>


            <?php get_footer(); ?>
