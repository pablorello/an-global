
<?php get_header();
  $img_desk= get_field('imagen_nosotros_desk');
  ?>
   <link rel="stylesheet" href="<?php bloginfo('template_url');?>/css/estilosc.css?v=1">
    <div class="bannerInterna internaB" style="background-image: url('<?php echo $img_desk['url']; ?>')">

        <div class="container">

        <div class="row">
          <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
            <h1><?php echo get_the_title(); ?></h1>
          </div>
          <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6"></div>
          <div class="col-12 col-sm-12 col-md-1 col-lg-1 col-xl-1"></div>
          <div class="col-12 col-sm-12 col-md-5 col-lg-5 col-xl-5">
            <div class="texto"><?php echo get_field('texto_banner_investor'); ?></div>
          </div>
          <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6"></div>
       </div>

      </div>
    </div>
  <div class="teamGlobal"  data-url="<?php echo get_home_url(); ?>/team" >
    <div class="container">

        <?php
        $argsa = array(
                'post_type' => 'jerarquia'
                );
                $popularesa = query_posts($argsa);
                $conteoProyectosa= count($popularesa);
                $idesTeam = array();
                for ($i=0; $i < $conteoProyectosa; $i++) {
                  $argsb = array(
                    'post_type' => 'team_members',
                    'meta_query' => array(
                      array(
                        'key' => 'jerarquia',
                        'value' => 'a:1:{i:0;s:4:"'.$popularesa[$i]->ID.'";}'
                        )
                      )
                  );
                  $popularesb = query_posts($argsb);
                  $conteoPPP = count($popularesb);
                  $numero = 0;

                  if ($popularesa[$i]->ID == 1495) {
                    ?>
                    <div class="row">
                    <?php
                    for ($a=0; $a < $conteoPPP; $a++) {
                      $imgBio = get_field('fotografia',$popularesb[$a]->ID);
                      $practica = get_field('selecciona_practica_bio',$popularesb[$a]->ID);
                      ?>
                      <div class="col-sm-12 col-md-6 col-lg-6">
                        <div class="eachPersona">
                          <img src="<?php echo $imgBio['url']?>" alt="<?php echo $imgBio['alt']?>">
                          <div class="skewP">
                            <div class="inverseSkew">
                              <h6><?php echo get_the_title($popularesb[$a]->ID); ?></h6>
                              <p class="puesto"><?php echo get_field('puesto',$popularesb[$a]->ID); ?></p>
                            </div>
                          </div>
                          <a onclick="showSTeam(this)" data-solution="<?php echo $popularesb[$a]->ID; ?>" data-practica="<?php echo $practica[0]->ID; ?>"> VER MÁS</a>
                        </div>
                      </div>
                      <?php
                    }?>
                  </div>
                    <?php
                  }else{

                    ?>
                    <div class="row">
                      <?php
                      $primo =$i%2;
                      if ($primo == 1) {

                        $idesTotal = array();
                        $idesTotal[0][0]["ID"]='especial';
                        $idesTotal =  array_merge_recursive($idesTotal,$popularesb);

                      }else{

                        $chunkResult = array_chunk($popularesb, 2);
                        $idesTotal = array();
                        $chunkResult[0][2]["ID"]='especial';

                          for ($d=0; $d < count($chunkResult); $d++) {
                           $idesTotal =  array_merge_recursive($idesTotal,$chunkResult[$d]);
                          }
                        }
                        for ($c=0; $c < count($idesTotal); $c++) {
                            $imgBio = get_field('fotografia',$idesTotal[$c]->ID);
                            $practica = get_field('selecciona_practica_bio',$idesTotal[$c]->ID);
                          ?>
                          <div class="col-sm-12 col-md-4 col-lg-4">
                            <div class="eachPersona">
                              <?php
                              if ($idesTotal[$c]->ID == null) {
                                ?>
                                  <h3><?php echo get_the_title($popularesa[$i]->ID); ?></h3>
                                <?php
                              }else{
                                ?>
                                  <img src="<?php echo $imgBio['url']?>" alt="<?php echo $imgBio['alt']?>">
                                  <div class="skewP">
                                    <div class="inverseSkew" style="background:<?php echo get_field('color_de_unidad',$practica[0]->ID) ?>;">
                                      <h6><?php echo get_the_title($idesTotal[$c]->ID); ?></h6>
                                      <p class="puesto"><?php echo get_field('puesto',$idesTotal[$c]->ID); ?></p>
                                    </div>
                                  </div>
                                  <a onclick="showSTeam(this)" data-solution="<?php echo $popularesb[$a]->ID; ?>" data-practica="<?php echo $practica[0]->ID; ?>"> VER MÁS</a>
                                <?php
                              }
                                  ?>
                            </div>
                          </div>
                          <?php
                        }
                      ?>
                    </div>
                  <?php
                }
              }
         ?>
    </div>
  </div>
            <div class="contacta">
                <div class="container">
                    <div class="row">
                        <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                            <div class="txtContacta">
                                <h3><?php echo get_field('titulo_contact',1165); ?></h3></div>
                        </div>
                        <div class="col-12 col-sm-12 col-md-6 col-lg-3">
                            <div class="txtContacta">
                                <p><?php echo get_field('texto_contact',1165); ?></p>
                            </div>
                        </div>
                        <div class="col-12"> <a href="<?php echo get_the_permalink(187); ?>"><?php echo get_field('boton_contact',1165); ?></a> </div>
                    </div>
                </div>
                <div class="triangleCornerBottom dark">
                    <svg xmlns="http://www.w3.org/2000/svg" version="1.1" viewBox="0 0 10 10" preserveAspectRatio="none" class="triangleCornerBottom">
                        <polygon class="fillTriangle" points="1,10 10,1 10,10"></polygon>
                    </svg>
                </div>
            </div>
            
            <?php get_footer(); ?>
