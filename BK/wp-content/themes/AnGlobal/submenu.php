<?php
  require_once('../../../wp-load.php' );
  $ide = filter_var($_GET['ide'], FILTER_SANITIZE_STRING);
  $cliamE = get_field('claim_unidades',$ide);
  $submenu = get_field('soluciones_unidades',$ide);
  $subArr = array();
  for ($i=0; $i < count($submenu); $i++) {
    $subArr[$i] = array(
            'title' => get_the_title($submenu[$i]->ID),
            'link' => get_the_permalink($submenu[$i]->ID)
          );
  }
            if( !empty($submenu)){
            $arr[] = array(
                    'img' => $cliamE['url'],
                    'relacionados' => $subArr
                    );
          }

header('Content-type: application/json; charset=utf-8');
echo json_encode($arr);
exit();
?>
