</div>
<footer class="footer">
		<div id="supfooter">
			<?php $ideActaul = get_the_ID();
				$idHome = 200;
			?>
			<div class="container">
				<div class="row">
						<div class="col-12 col-sm-6 col-md-3 col-lg-3 col-xl-3">
							<div class="eachFooter first">
								<h6><?php echo get_field('titulo_somosAn',$idHome); ?></h6>
								<ul>
									<div class="row">
										<li class="col-6 col-sm-6 col-md-12 col-lg-12"><a class="<?php echo ($ideActaul == 136)?'activeMnu':''; ?>" href="<?php echo get_the_permalink(136); ?>"><?php echo get_the_title(136); ?></a></li>
										<li class="col-6 col-sm-6 col-md-12 col-lg-12"><a class="<?php echo ($ideActaul == 160)?'activeMnu':''; ?>" href="<?php echo get_the_permalink(160); ?>"><?php echo get_the_title(160); ?></a></li>
										<li class="col-6 col-sm-6 col-md-12 col-lg-12"><a class="<?php echo ($ideActaul == 187)?'activeMnu':''; ?>" href="<?php echo get_the_permalink(187); ?>"><?php echo get_the_title(187); ?></a></li>
									</div>

								</ul>
							</div>
						</div>
						<div class="col-12 col-sm-6 col-md-3 col-lg-3 col-xl-3">
							<div class="eachFooter last">
								<h6><?php echo get_field('titulo_seccion_practicas',136); ?></h6>
								<ul>
									<div class="row">
									<?php

										$argsn = array(
	                        'post_type' => 'business_units'
	                        );
	                        $popularesn = query_posts($argsn);
	                        $conteoProyectosn= count($popularesn);
													for ($n=0; $n < $conteoProyectosn; $n++) {
											?>
									<li class="col-6 col-sm-6 col-md-12 col-lg-12"><a class="<?php echo ($ideActaul == $popularesn[$n]->ID)?'activeMnu':''; ?>" href="<?php echo get_the_permalink($popularesn[$n]->ID) ?>" data-ide="<?php echo $popularesn[$n]->ID; ?>"><?php echo str_replace('AN_','',get_the_title($popularesn[$n]->ID)); ?></a></li>
									<?php } ?>
									</div>
								</ul>
							</div>
						</div>
						<div class="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
							<div class="separador"></div>
						</div>
						<div class="col-12 col-sm-12 col-md-2 col-lg-2 col-xl-2">
							<a href="<?php echo get_home_url(); ?>" class="logoFoot"><img src="<?php bloginfo('template_url');?>/images/logo-claim-vertical-AN.svg" alt="An"></a>
						</div>
				</div>
			</div>
		</div>
		<div id="subfooter">
			<div class="container">
				<?php
					$idFooter = 1165;
					$archivoavCl = get_field('archivo_avCl',$idFooter);
					$archivoavCo = get_field('archivo_avCo',$idFooter);
					$archivoavCa = get_field('archivo_avCa',$idFooter);
					$archivoQP = get_field('quality_politics',$idFooter);
					?>
				<div class="row">
					<div class="col-12 col-sm-12 col-md-8 col-lg-8 col-xl-8 order-1 order-sm-1 order-md-0 order-lg-0">
						<ul class="politicas">
							<div class="row">
								<li class="col-12 col-sm-6 col-md-6 col-lg-3"> <a href="<?php echo $archivoavCl['url']; ?>" target="_blank"><?php echo get_field('boton_avCl',$idFooter); ?></a></li>
								<li class="col-12 col-sm-6 col-md-6 col-lg-3"> <a href="<?php echo $archivoavCo['url']; ?>" target="_blank"><?php echo get_field('boton_avCo',$idFooter); ?></a></li>
								<li class="col-12 col-sm-6 col-md-6 col-lg-3"> <a href="<?php echo $archivoavCa['url']; ?>" target="_blank"><?php echo get_field('boton_avCa',$idFooter); ?></a></li>
								<li class="col-12 col-sm-6 col-md-6 col-lg-3"> <a href="<?php echo $archivoQP['url']; ?>" target="_blank"><?php echo get_field('boton_quality_politics',$idFooter); ?></a></li>
							</div>

						</ul>
						<p class="legal"><?php echo get_field('legales',$idFooter); ?></p>
					</div>
					<div class="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4 order-0 order-sm-0 order-md-1 order-lg-1">
						<?php
								$fb = get_field('facebook',$idFooter);
								$tw = get_field('twitter',$idFooter);
								$yt = get_field('youtube',$idFooter);
								$lk = get_field('linkedin',$idFooter);
						 ?>
						<ul class="redes">
							<div class="row">
								<li class="col-3 col-sm-3 col-md-3 col-lg-3">
									<a href="<?php echo get_field('url_facebook',$idFooter); ?>" target="_blank"><img src="<?php echo $fb['url'] ?>" alt="<?php echo $fb['alt'] ?>"></a>
								</li>
								<li class="col-3 col-sm-3 col-md-3 col-lg-3">
									<a href="<?php echo get_field('url_twitter',$idFooter); ?>" target="_blank"><img src="<?php echo $tw['url'] ?>" alt="<?php echo $tw['alt'] ?>"></a>
								</li>
								<li class="col-3 col-sm-3 col-md-3 col-lg-3">
									<a href="<?php echo get_field('url_youtube',$idFooter); ?>" target="_blank"><img src="<?php echo $yt['url'] ?>" alt="<?php echo $yt['alt'] ?>"></a>
								</li>
								<li class="col-3 col-sm-3 col-md-3 col-lg-3">
									<a href="<?php echo get_field('url_linkedin',$idFooter); ?>" target="_blank"><img src="<?php echo $lk['url'] ?>" alt="<?php echo $lk['alt'] ?>"></a>
								</li>
							</div>

							</ul>
						</div>
					</div>
				</div>
		</div>
</footer>
<div id="pop">
	<div class="overlay">
	<div class="content">
		<div id="modalBox" class="container" style="background-image: url('<?php bloginfo('template_url') ?>/images/fondo-solucion-1.jpg')">
		</div>
	</div>
</div>
</div>
<?php wp_footer(); ?>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="<?php bloginfo('template_url');?>/js/utils.js" type="text/javascript"></script>
<script src="<?php bloginfo('template_url');?>/js/tether.min.js" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js"></script>
<script src="<?php bloginfo('template_url');?>/js/bootstrap.min.js" type="text/javascript"></script>
<script src="<?php bloginfo('template_url');?>/js/jquery.svgInject.js" type="text/javascript"></script>
<script src="<?php bloginfo('template_url');?>/js/owl.carousel.min.js" type="text/javascript"></script>
<!--<script src="<?php bloginfo('template_url');?>/js/app.js"></script>-->
<script src="<?php bloginfo('template_url');?>/js/general.js?v=1.1.23" type="text/javascript"></script>
</body>
</html>
