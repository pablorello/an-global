<?php
get_header(); ?>


<!-- banner de practica-->

<div class="entry">

</div>
<!-- banner logo de practica-->

<div class="jumbotron jumbotron-fluid unity">
    <div class="container text-center"> <img src="images/claim_experience.svg" alt="" class="svgImage"> </div>
</div>
<?php /*


<!-- caso de exito -->
<div class="container-fluid casoExito">
    <div class="triangleCornerTop">
        <svg xmlns="http://www.w3.org/2000/svg" version="1.1" viewBox="0 0 10 10" preserveAspectRatio="none" class="triangleCornerTop">
            <polygon class="fillTriangle" points="0,0 8,0 0,8"></polygon>
        </svg>
    </div>
    <div class="cotainer">
      <div class="row">
          <div class="col-12 col-sm-12 col-md-7 col-lg-7">
              <div class="videoSuccess">
                  <!-- video incrustado -->Video </div>
              <div class="customerLogo">
                  <!-- logo cliente--><img src="images/logo-cliente.png" alt="">
                  <div class="triangleCornerLogo">
                      <svg xmlns="http://www.w3.org/2000/svg" version="1.1" viewBox="0 0 10 10" preserveAspectRatio="none" class="triangleCornerLogo">
                          <polygon class="fillTriangleLogo" points="1,10 10,0 10,10"></polygon>
                      </svg>
                  </div>
              </div>
          </div>
          <div class="col-12 col-sm-12 col-md-5 col-lg-5">
              <h2>CASO DE ÉXITO</h2>
              <h3>PROYECTO</h3>
              <P>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Hic accusamus, dolore natus repudiandae dicta provident consequuntur suscipit consequatur aspernatur perspiciatis, excepturi at iure aliquam in perferendis cum! Omnis, itaque, laborum.</P>
              <h3>OBJETIVO</h3>
              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Explicabo accusantium nobis magnam odio consequuntur veniam placeat deserunt, minima, recusandae delectus eaque officia. Facere, quis ducimus nisi distinctio magni ad cum.</p>
              <h3>SOLUCIONES</h3>
              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Placeat illo harum, repudiandae eaque unde magnam recusandae natus soluta magni exercitationem perspiciatis. Voluptate, quae, explicabo. Dicta maxime quas aliquid consequuntur ea.</p>
          </div>
      </div>
    </div>

    <div class="triangleCornerBottom">
        <svg xmlns="http://www.w3.org/2000/svg" version="1.1" viewBox="0 0 10 10" preserveAspectRatio="none" class="triangleCornerBottom">
            <polygon class="fillTriangle" points="1,10 10,1 10,10"></polygon>
        </svg>
    </div>
</div>
*/?>
<!-- soluciones -->
<div class="container solutions">
    <div class="row">
        <div class="col col-lg-4">
            <div class="cardTitle">
                <h2>SOLUCIONES</h2>
                <div class="lineSolutions">
                    <svg xmlns="http://www.w3.org/2000/svg" version="1.1" viewBox="0 0 10 10" preserveAspectRatio="none" class="lineSolutions">
                        <polyline class="fillLineSolutions" points="0,0 8,0 "></polyline>
                    </svg>
                </div>
            </div>
        </div>
        <!-- tarjetas soluciones -->
        <div class="col col-lg-4">
            <div class="container card">
                <div class="col"> <img src="images/solucion-1.svg" alt="" class="svgImage"> </div>
                <div class="col">
                    <h3>DISEÑO DE IDENTIDAD DE MARCA</h3>
                    <p>Construimos el carácter de una marca a través de su identidad visual.</p> <a href="#" class="linkLine">VER MÁS</a> </div>
            </div>
        </div>
        <div class="col col-lg-4">
            <div class="card"> solucion 3 </div>
        </div>
        <div class="col col-lg-4">
            <div class="card"> solucion 4 </div>
        </div>
        <div class="col col-lg-4">
            <div class="card"> solucion 5 </div>
        </div>
        <div class="col col-lg-4">
            <div class="card"> solucion 6 </div>
        </div>
    </div>
</div>

<!-- clientes -->
<div class="container-fluid clientes">

    <div class="container clientesLogos">
      <div class="tittleSeccion">
        <h2>CLIENTES</h2>
      </div>
    <div class="row">
        <div class="col col-lg-4"> <img src="images/colgate.png" alt=""> </div>
        <div class="col col-lg-4"> <img src="images/colgate.png" alt=""> </div>
        <div class="col col-lg-4"> <img src="images/colgate.png" alt=""> </div>
    </div>
</div>
</div>

<?php get_footer(); ?>
