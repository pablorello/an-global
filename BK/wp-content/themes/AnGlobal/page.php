<?php get_header();?>
<div class="bannerInterna">
  <?php
    $img_desk= get_field('banner_desktop_tec');
    $img_mob= get_field('banner_mobile_tec');
  ?>
  <img src="<?php echo $img_desk['url'] ?>" alt="<?php echo $img_desk['alt'] ?>" class="d-none d-sm-none d-md-none d-lg-block d-xl-block">
  <img src="<?php echo $img_mob['url'] ?>" alt="<?php echo $img_desk['alt'] ?>" class="d-block d-sm-block d-md-block d-lg-none d-xl-none">
  <div class="container">
    <div class="row">
      <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
        <h1><?php echo get_field('titulo_tec');?></h1>
        <div class="texto"><?php echo get_field('texto_tec');?></div>
      </div>
      <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6"></div>
    </div>
  </div>
</div>
<div class="tecnologias">
  <div class="container">
      <?php
        $unidades = get_field('selecciona_unidades');
        for ($i=0; $i < count($unidades); $i++) {
          ?>
          <div class="row">
            <div class="col-12 col-sm-12 col-md-5 col-lg-5 col-xl-5">
              <h2><?php echo str_replace('AN_','',get_the_title($unidades[$i]->ID)); ?></h2>
            </div>
            <div class="col-12 col-sm-12 col-md-7 col-lg-7 col-xl-7">
                <?php
                  $tecnologias = get_field('tecnologias_unidades',$unidades[$i]->ID);
                  for ($e=0; $e < count($tecnologias); $e++) {
                    if(!empty($tecnologias[$e])){
                    ?>
                    <div class="row">
                      <div class="col-6 vol-sm-6 col-md-4 col-lg-4 col-xl-4">
                        <?php
                          $imgTec =  get_field('logotipo_ct',$tecnologias[$e]->ID);
                          if(!empty($imgTec)){
                            ?>
                            <img src="<?php echo $imgTec['url']?>" alt="<?php echo $imgTec['alt']?>">
                            <?php
                          }
                        ?>
                      </div>
                      <div class="col-6 vol-sm-6 col-md-4 col-lg-4 col-xl-4">
                        <p><?php echo get_the_title($tecnologias[$e]->ID) ?></p>
                      </div>
                    </div>
                    <?php
                    }
                  }
                ?>
            </div>
          </div>
          <?php
        }
      ?>
  </div>
</div>
<div class="contacta">
    <div class="container">
        <div class="row">
            <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                <div class="txtContacta">
                    <h3>¿QUIERES TRABAJAR CON NOSOTROS?</h3></div>
            </div>
            <div class="col-12 col-sm-12 col-md-6 col-lg-3">
                <p>Escribe a:</p>
                <a href="#">vacantes@anglobal.com</a>
            </div>
            <div class="col-12 col-sm-12 col-md-12 col-lg-3"></div>
        </div>
    </div>
</div>
<style media="screen">
  <?php
    for ($j=0; $j < count($unidades); $j++) {
      $color = get_field('color_de_unidad',$unidades[$j]->ID);
      echo "#".$unidades[$j]->post_name.' svg path{fill: '.$color.';stroke: '.$color.'; }';
    }
  ?>
</style>
<?php get_footer(); ?>
