<?php get_header();
$currentID = get_the_ID();
?>
<?php
    $img_desk= get_field('banne_desk_Sol');
    $img_mob= get_field('banner_mobile_sol');
  ?>
  <?php
    $imgIcon = get_field('iconos_soluciones');
    if(!empty($imgIcon)){
  ?>
  <div id="pop" class="landing">
  	<div class="">
  	<div class="content">
  		<div id="modalBox" style="background-image: url('<?php bloginfo('template_url') ?>/images/fondo-solucion-1.jpg')">
        <div class="gris">
          <div class="container">
            <div class="row">
          <div class="col-3 col-sm-3 col-md-2 col-lg-2 col-xl-2">
            <img src="<?php echo $imgIcon['url'];?>" alt="<?php echo $imgIcon['alt'];?>">
          </div>
          <div class="col-9 col-sm-9 col-md-10 col-lg-10 col-xl-10">
            <h2><?php echo get_the_title();?></h2>
        </div>
      </div>
    </div>
        </div>
        <div class="row">
            <div class="container">
          <div class="col-12 col-sm-12 col-md-1 col-lg-1 col-xl-1"></div>
            <div class="col-12 col-sm-12 col-md-11 col-lg-11 col-xl-11">
              <div class="overflowHidden">
                <div class="copete" style="background-image: linear-gradient(104deg,#222222,#020202);"></div>
                <div class="descPop" style="background-image: linear-gradient(104deg,#222222,#020202);">
                  <div class="row"><div class="col-1"></div>
                  <div class="col-11 col-sm-11 col-md-10 col-lg-10">
                    <div class="txtPop">
                      <p>  <?php echo get_field('texto_ben_sol'); ?></p>
                    </div>
                  </div>
                  <div class="col-12 col-sm-12 col-md-1 col-lg-1"></div>
                </div>
              </div>
            </div>
          </div>
          </div>
        </div>
        <div class="container">




      <div class="row"
      ><div class="col-12 col-sm-12 col-md-1 col-lg-1 col-xl-1"></div>
      <div class="beneficios col-12 col-sm-12 col-md-5 col-lg-5 col-xl-5">

        <h3><?php echo get_field('titulo_ben_sol'); ?></h3>

          <ul>
            <?php
         $beneficios = get_field('beneficios_ben_sol');
         for ($i=0; $i < count($beneficios); $i++) {
           ?>
             <li><div class="ttListB">
               <i class="icon dripicons-<?php echo get_field('selecciona_icono',$beneficios[$i]->ID);?>"></i>
               <span><?php echo get_the_title($beneficios[$i]->ID);?></span>
             </div>
             </li>
           <?php
         }
        ?>

        </ul></div>
          <div class="servicios col-12 col-sm-12 col-md-5 col-lg-5 col-xl-5">
              <h3> <?php echo get_field('titulo_ps_ben'); ?></h3>
                <ul>
                  <?php
                    $productos = get_field('productos_ben');
                     for ($a=0; $a < count($productos); $a++) {
                       ?>
                       <li class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                         <span><?php echo get_the_title($productos[$a]->ID); ?></span>
                       </li>
                       <?php
                     }
                  ?>
                </ul>
              </div><div class="col-12 col-sm-12 col-md-1 col-lg-1 col-xl-1"></div>
      </div>
  		</div>
  	</div>
  </div>
  </div>
</div>
<?php } ?>
<?php /*
<div class="bannerInterna internaB bannerIntSolution" style="background-image: url('<?php echo $img_desk['url']; ?>');">
  <div class="container bannerSolucion">
    <div class="row justify-content-md-center">
      <div class="col-12 col-sm-12 col-md-1 col-lg-1 col-xl-1"></div>
      <div class="col-12 col-sm-12 col-md-3 col-lg-2 col-xl-2">
        <div class="iconoSolucion">
          <img src="<?php echo $imgIcon['url'];?>" alt="<?php echo $imgIcon['alt'];?>" class="svgImage">
        </div>

      </div>
      <div class="col-12 col-sm-12 col-md-1 col-lg-1 col-xl-1"></div>
      <div class="col-12 col-sm-12 col-md-7 col-lg-4 col-xl-4">
        <h1><?php echo get_the_title();?></h1>
      </div>
      <div class="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4"></div>
    </div>
  </div>
</div>
<div class="beneficios">
  <div class="container">
    <div class="row justify-content-md-center titlecenter">
      <div class="titlecorte col-sm-12">
        <h2><?php echo get_field('titulo_ben_sol'); ?></h2>
        <div>
          <?php echo get_field('texto_ben_sol'); ?>
        </div>
      </div>

      <div class="row introfact">
          <?php
       $beneficios = get_field('beneficios_ben_sol');
       for ($i=0; $i < count($beneficios); $i++) {
         ?>
         <div class="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
           <div class="eachBen">
             <div class="icon dripicons-<?php echo get_field('selecciona_icono',$beneficios[$i]->ID);?>"></div>
             <p><?php echo get_the_title($beneficios[$i]->ID);?></p>
           </div>
         </div>
         <?php
       }
      ?>
      </div>

    </div>
  </div>
</div>
<div class="productos">
  <div class="container">
    <div class="row">
      <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
        <h2><?php echo get_field('titulo_ps_ben'); ?></h2>
      </div>
      <ul class="row">
      <?php
        $productos = get_field('productos_ben');
         for ($a=0; $a < count($productos); $a++) {
           ?>
           <li class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
             <span><?php echo get_the_title($productos[$a]->ID); ?></span>
           </li>
           <?php
         }
      ?>
      </ul>
    </div>
  </div>
</div>
*/ ?>






<?php /*
<div class="moreSol morePagSol">
 <div class="triangleCornerTop ">
                <svg xmlns="http://www.w3.org/2000/svg " version="1.1 " viewBox="0 0 10 10 " preserveAspectRatio="none " class="triangleCornerTop ">
                    <polygon class="fillTriangle " points="0,0 8,0 0,8 "></polygon>
                </svg>
            </div>
  <div class="container">
    <div class="row">
      <div class="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
        <h2><?php echo get_field('titulo_moreS',200); ?></h2>
      </div>
      <div class="col-12 col-sm-12 col-md-8 col-lg-8 col-xl-8">
        <div class="owl-carousel">
            <?php
            $argsc = array(
                    'post_type' => 'soluciones',
                    'order' => 'ASC',
                    'post__not_in' => array($currentID),
                    'posts_per_page' => 40
                    );
                    $popularesc = query_posts($argsc);
                    $conteoProyectosc= count($popularesc);
                    for ($e=0; $e < $conteoProyectosc; $e++) {
                      $imgIconM = get_field('iconos_soluciones',$popularesc[$e]->ID);
            ?>
            <div class="eachSol">
              <a href="<?php echo get_permalink($popularesc[$e]->ID); ?>">
              <?php
              if(!empty($imgIconM)){
            ?>
            <div class="iconoSolucionM">
              <img src="<?php echo $imgIconM['url'];?>" alt="<?php echo $imgIconM['alt'];?>">
            </div>

          <?php } ?>
          <span class="ttS"><?php echo get_the_title($popularesc[$e]->ID);?></span>
          </a>
            </div>
          <?php } ?>
        </div>
    </div>
  </div>
  </div>
</div>
*/?>
<div class="contacta">
                <div class="container">
                    <div class="row">
                        <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                            <div class="txtContacta">
                                <h3><?php echo get_field('titulo_contact',1165); ?></h3></div>
                        </div>
                        <div class="col-12 col-sm-12 col-md-6 col-lg-3">
                            <div class="txtContacta">
                                <p><?php echo get_field('texto_contact',1165); ?></p>
                            </div>
                        </div>
                        <div class="col-12"><a href="<?php echo get_the_permalink(187); ?>"><?php echo get_field('boton_contact',1165); ?></a></div>
                    </div>
                </div>
                <div class="triangleCornerBottom dark">
                    <svg xmlns="http://www.w3.org/2000/svg" version="1.1" viewBox="0 0 10 10" preserveAspectRatio="none" class="triangleCornerBottom">
                        <polygon class="fillTriangle" points="1,10 10,1 10,10"></polygon>
                    </svg>
                </div>
            </div>
<?php get_footer(); ?>
