<?php get_header();?>

<?php
$idHome = 200;
$img_desk= get_field('banner_desktop_home',$idHome);
$fondoMp4 = get_field('fondo_mp4',$idHome);
$fondoOgg = get_field('fondo_ogg',$idHome);
$fondoWebm = get_field('fondo_webm',$idHome);
$bannerD = get_field('imagen_desktop_hm',$idHome);
$bannerM = get_field('imagen_mobile_hm',$idHome);
?>
<div class="bannerInterna internaB" style="background:url('<?php echo $img_desk['url']?>');">
  <video autoplay="autoplay" loop id="video_background" preload="metadata" volume="0"/>
    <source src="<?php echo $fondoMp4['url']; ?>" type="video/mp4" />
    <source src="<?php echo $fondoOgg['url']; ?>" type="video/ogg" />
    <source src="<?php echo $fondoWebm['url']; ?>" type="video/webm" />
  </video/>
  <div class="txtHome">
        <img src="<?php echo $bannerD['url'] ?>" alt="<?php echo $bannerD['alt'] ?>" class="textoDsk d-none d-sm-none d-md-block d-lg-block">
        <img src="<?php echo $bannerM['url'] ?>" alt="<?php echo $bannerM['alt'] ?>" class="textoMob d-block d-sm-block d-md-none d-lg-none">
  </div>
</div>
<div class="container-fluid casoExito">

		<div class="powerOf">
  <div class="container">
    <div class="row">
      <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
		 <img src="<?php bloginfo('template_url');?>/images/claim_AN.svg" alt="" class="claim">
      </div>
      <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 powerOf-text">
        <?php echo get_field('texto_power_of',$idHome); ?>
      </div>
    </div>
  </div>
</div>
        <div class="triangleCornerBottom dark">
            <svg xmlns="http://www.w3.org/2000/svg" version="1.1" viewBox="0 0 10 10" preserveAspectRatio="none" class="triangleCornerBottom">
                <polygon class="fillTriangle" points="1,10 10,1 10,10"></polygon>
            </svg>
        </div>
    </div>


<div class="container-fluid casoVerde">
        <div class="triangleCornerTop">
            <svg xmlns="http://www.w3.org/2000/svg" version="1.1" viewBox="0 0 10 10" preserveAspectRatio="none" class="triangleCornerTop">
                <polygon class="fillTriangleV" points="0,0 8,0 0,8"></polygon>
            </svg>
        </div>
<div class="principiosHome">
  <div class="container">
    <div class="row">
      <div class="col-12 col-sm-12 col-md-12 col-lg-12">
        <h2><?php echo get_field('titulo_principios',$idHome); ?></h2>
      </div>
      <?php
      $argsd = array(
              'post_type' => 'principios',
              'posts_per_page' => 3
              );
              $popularesd = query_posts($argsd);
              $conteoProyectosd= count($popularesd);
              for ($e=0; $e < $conteoProyectosd; $e++) {
      ?>
      <div class="col-12 col-sm-12 col-md-4 col-lg-4">
        <div class="imagesPrin">
          <?php
          $imagen = get_field('fotografia_principios',$popularesd[$e]->ID);
          if(!empty($imagen)){ ?>
            <img src="<?php echo $imagen['url'] ?>" alt="<?php echo $imagen['alt'] ?>">
          <?php } ?>
          <h3 class="label"><?php echo get_the_title($popularesd[$e]->ID); ?></h3>
        </div>

        <div class="contentPrincipios">
          <?php echo get_field('descripcion_corta_princ',$popularesd[$e]->ID); ?>
        </div>
      </div>
      <?php } ?>
    </div>
  </div>
</div>
<div class="triangleCornerBottom">
            <svg xmlns="http://www.w3.org/2000/svg" version="1.1" viewBox="0 0 10 10" preserveAspectRatio="none" class="triangleCornerBottom">
                <polygon class="fillTriangle" points="1,10 10,1 10,10"></polygon>
            </svg>
        </div>
    </div>

<div class="practica">
  <div class="container">
    <div class="row">
      <div class="col-12 col-sm-12 col-md-12 col-lg-6 col-xl-6">
        <div class="tittleSeccion LadoA">
                        <h2><?php echo get_field('titulo_seccion_practicas',136); ?></h2>
                        <div class="clearfix"></div>
                    </div>
                    <div class="txtInsTitle">
                      <?php echo get_field('texto_practicas',$idHome); ?>
                    </div>
      </div>
      <!--<div class="col-12 col-sm-12 col-md-12 col-lg-1 col-xl-1"></div>-->
      <div class="col-12 col-sm-12 col-md-12 col-lg-6 col-xl-6">
        <div class="row">
          <?php
          $argsc = array(
                  'post_type' => 'business_units',
                  'order' => 'ASC'
                  );
                  $popularesc = query_posts($argsc);
                  $conteoProyectosc= count($popularesc);
                  for ($h=0; $h < $conteoProyectosc; $h++) {
                    ?>
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                      <div id="<?php echo $popularesc[$h]->post_name;?>" class="eachPractica" onclick="location.href='<?php echo get_the_permalink($popularesc[$h]->ID); ?>';" onMouseOver="this.style.borderColor='<?php echo get_field('color_de_unidad',$popularesc[$h]->ID); ?>'" onMouseOut="this.style.borderColor='#fff'">
                        <?php
                          $imgUnidad = get_field('logotipo_claim',$popularesc[$h]->ID);
                          if(!empty($imgUnidad)){
                        ?>
                        <img src="<?php echo $imgUnidad['url']?>" alt="<?php echo $imgUnidad['alt']?>">
                        <?php } ?>
                        <a><?php echo get_field('verM_bp',$ideGralTexto); ?></a>
                      </div>
                    </div>
                    <?php
                  }
                ?>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="container-fluid mapaSomos">
        <div class="triangleCornerTop">
            <svg xmlns="http://www.w3.org/2000/svg" version="1.1" viewBox="0 0 10 10" preserveAspectRatio="none" class="triangleCornerTop">
                <polygon class="fillTriangle" points="0,0 8,0 0,8"></polygon>
            </svg>
        </div>
        <div class="somos">
          <div class="container">
            <div class="row">
              <div class="col-12 col-sm-12 col-md-12 col-lg-12">

                                    <h2><?php echo get_field('titulo_somosAn',$idHome); ?></h2>
              </div>
              <div class="col-12">
                <div class="mapa">
                  <div class="paises">

                    <div class="paisesListado">
                      <ul data-url="<?php echo get_home_url(); ?>/pais">
                        <?php
                            $argsb = array(
                                  'post_type' => 'paises',
                                  'order' => 'ASC'
                                  );
                                $popularesb = query_posts($argsb);
                                $conteoProyectosb= count($popularesb);
                                  for ($j=0; $j < $conteoProyectosb; $j++) {
                        ?>
                        <li id="<?php echo $popularesb[$j]->ID; ?>" onclick="mapa(this)" class="<?php echo($popularesb[$j]->ID == 220)?'activoPais':''; ?>"><span><?php echo get_field('nomenclatura',$popularesb[$j]->ID); ?></span></li>
                      <?php } ?>
                      </ul>
                      <div class="clearfix"></div>
                    </div>
                    <div class="cambioMapa">
                      <?php
                        $mapa = get_field('mapa',220);
                        $mapaMob = get_field('mapa_mobile',220);
                        $oficina = get_field('oficina',220);
                      ?>
                      <img src="<?php echo $mapa['url']; ?>" alt="<?php echo $mapa['alt']; ?>" class="mapaDesk">
                      <img src="<?php echo $mapaMob['url']; ?>" alt="<?php echo $mapaMob['alt']; ?>" class="mapaMob">
                      <div class="infoOficina">
                        <span class="cerrarMapa cerrar" onclick="cerrarMapa(this)">X</span>
                          <h6><?php echo get_the_title($oficina[0]->ID); ?></h6>
                          <div class="dirM">
                            <div class="iconM">
                              <span class="icon dripicons-location"></span>
                            </div>
                            <div class="textM">
                              <?php echo get_field('direccion_oficinas',$oficina[0]->ID); ?>
                            </div>
                            <div class="clearfix"></div>
                          </div>
                          <div class="mailM">
                            <div class="iconM">
                              <span class="icon dripicons-mail"></span>
                            </div>
                            <div class="textM">
                              <?php echo get_field('correo_oficinas',$oficina[0]->ID); ?>
                            </div>
                            <div class="clearfix"></div>
                          </div>
                          <div class="telM">
                            <div class="iconM">
                              <span class="icon dripicons-phone"></span>
                            </div>
                            <div class="textM">
                              <?php echo get_field('telefono_oficina',$oficina[0]->ID); ?>
                            </div>
                            <div class="clearfix"></div>
                          </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>

            </div>
              </div>
        </div>
        <div class="triangleCornerBottom">
            <svg xmlns="http://www.w3.org/2000/svg" version="1.1" viewBox="0 0 10 10" preserveAspectRatio="none" class="triangleCornerBottom">
                <polygon class="fillTriangle" points="1,10 10,1 10,10"></polygon>
            </svg>
        </div>
    </div>
    <div class="container-fluid">
            <div class="clientesCont">
              <div class="container">
                <div class="row">
                  <div class="col-12 col-sm-12 col-md-12 col-lg-12">

                                        <h2><?php echo get_field('titulo_clientes',$ideGralTexto); ?></h2>
                  </div>
                  <div class="listadoClientes">
                    <div class="row">
                      <?php
                        $clientes = get_field('clientes_home',$idHome);
                        for ($k=0; $k < count($clientes) ; $k++) {
                          $iconoCli= get_field('logotipo_clientes',$clientes[$k]->ID);
                        ?>
                        <div class="col-6 col-sm-4 col-lg-2 col-xl-2">
                          <div class="contenedorLogo">
                            <img src="<?php echo $iconoCli['url']; ?>" alt="<?php echo $iconoCli['alt']; ?>">
                          </div>
                        </div>
                        <?php
                        }
                      ?>
                    </div>
                  </div>

                </div>
              </div>
            </div>
        </div>
    <div class="contacta">
        <div class="container">
            <div class="row">
                <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                    <div class="txtContacta">
                        <h3><?php echo get_field('titulo_contact',1165); ?></h3></div>
                </div>
                <div class="col-12 col-sm-12 col-md-6 col-lg-3">
                    <div class="txtContacta">
                        <p><?php echo get_field('texto_contact',1165); ?></p>
                    </div>
                </div>
                <div class="col-12"> <a href="<?php echo get_the_permalink(187); ?>"><?php echo get_field('boton_contact',1165); ?></a> </div>
            </div>
        </div>
        <div class="triangleCornerBottom dark">
            <svg xmlns="http://www.w3.org/2000/svg" version="1.1" viewBox="0 0 10 10" preserveAspectRatio="none" class="triangleCornerBottom">
                <polygon class="fillTriangle" points="1,10 10,1 10,10"></polygon>
            </svg>
        </div>
    </div>
<?php get_footer(); ?>
