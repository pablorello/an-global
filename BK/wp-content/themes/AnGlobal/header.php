<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta http-equiv="Content-Language" content="es"/>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="apple-mobile-web-app-capable" content="yes" />
<meta names="apple-mobile-web-app-status-bar-style" content="black-translucent" />


  <title><?php echo get_the_title(); ?></title>
  <link rel="icon" type="image/png" href="<?php bloginfo('template_url');?>/images/favicon.png" />
  <link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url');?>/css/bootstrap.min.css">
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Roboto+Slab:100,300,400,700" rel="stylesheet">
<link rel="stylesheet" href="<?php bloginfo('template_url');?>/css/webfont.css">
<link rel="stylesheet" href="<?php bloginfo('template_url');?>/css/owl.carousel.min.css">
<link rel="stylesheet" href="<?php bloginfo('template_url');?>/css/owl.theme.default.min.css">
  <link rel="stylesheet" href="<?php bloginfo('template_url');?>/css/jquery.smartmenus.bootstrap-4.css">
  <link rel="stylesheet" href="<?php bloginfo('template_url');?>/css/estilos.css?v=1.1.34">
  <link rel="stylesheet" href="<?php bloginfo('template_url');?>/css/estilosb.css?v=1.1.9">
  <link rel="stylesheet" href="<?php bloginfo('template_url');?>/css/menu.css?v=1.2.10">
  <link rel="stylesheet" href="<?php bloginfo('template_url');?>/css/home.css?v=1.1.14">
  <link rel="stylesheet" href="<?php bloginfo('template_url');?>/css/estilosc.css?v=1.1.4">
  <!--prueba-->
  <?php
  wp_head();
  global $currentLang;
  $currentLang = qtrans_getLanguage();
  $idiomas = array('es','en');
  $another = array('en','es');
  $existe = array_search($currentLang, $idiomas);
$ideActaul = get_the_ID();
  $enabled_languages = get_option('qtranslate_enabled_languages');
$language_names = get_option('qtranslate_language_names');
$txtIdioma = array('ENGLISH','ESPAÑOL');

  ?>
</head>

<body>
  <header class="header topMenu">
    <div class="container no-padding">


      <nav class="navbar navbar-expand-lg navbar-light" id="shadowOver">
          <a class="navbar-brand" href="<?php echo get_home_url(); ?>"> <img src="<?php bloginfo('template_url');?>/images/logo_an.svg" alt=""></a>
          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTextA" aria-controls="navbarTextA" aria-expanded="false" aria-label="Toggle navigation">
            <span class="a"></span>
            <span class="b"></span>
            <span class="c"></span>
          </button>
          <div class="collapse navbar-collapse" id="navbarTextA">

            <div id="navbarText">
              <ul class="navbar-nav mr-auto bu" data-url="<?php bloginfo('template_url');?>/submenu.php">
                <?php
                $argsm = array(
                        'post_type' => 'business_units'
                        );
                        $popularesm = query_posts($argsm);
                        $conteoProyectosm= count($popularesm);
                        for ($m=0; $m < $conteoProyectosm; $m++) { ?>
                          <li class="nav-item" onclick="showMenu(this)">
                            <a class="thisA nav-link <?php echo ($ideActaul == $popularesm[$m]->ID)?'activeMnu':''; ?>" href="<?php echo get_the_permalink($popularesm[$m]->ID) ?>" data-url="<?php echo get_the_permalink($popularesm[$m]->ID) ?>" data-ide="<?php echo $popularesm[$m]->ID; ?>"><?php echo str_replace('AN_','',get_the_title($popularesm[$m]->ID)); ?></a>
                            <div class="submenu<?php echo strtolower(str_replace('AN_',' ',get_the_title($popularesm[$m]->ID))); ?>">
                                <div class="container">
                                  <div class="row">
                                    <div class="col-12 col-sm-12 col-md-7 col-lg-7 col-xl-7">
                                      <div class="claimMenu">
                                        <?php
                                          $cliamE = get_field('logotipo_claim',$popularesm[$m]->ID);
                                        ?>
                                        <div class="eachClaim">
                                          <img src="<?php echo $cliamE['url'] ?>" alt="<?php echo $cliamE['alt'] ?>">
                                        </div>
                                        <div class="descMenu  d-none d-md-block">
                                          <div class="containerDescM">
                                            <?php echo get_field('descripcion_menu',$popularesm[$m]->ID); ?>
                                          </div>
                                        </div>
                                        <div class="conoceMenu">
                                          <p><?php _e("[:es]CONOCE MÁS SOBRE[:en]GO TO"); ?> <a class="regular" href="<?php echo get_the_permalink($popularesm[$m]->ID) ?>"><?php echo get_the_title($popularesm[$m]->ID); ?></a><i class="icon dripicons-arrow-thin-right"></i></p>
                                        </div>
                                      </div>
                                    </div>
                                    <div class="col-12 col-sm-12 col-md-1 col-lg-1 col-xl-1"></div>
                                    <div class="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
                                      <ul class="row submenuHere">
                                      <?php
                                        $submenu = get_field('soluciones_unidades',$popularesm[$m]->ID);
                                        $conteo = count($submenu);
                                        for ($i=0; $i < 5; $i++) {
                                          ?>
                                              <li class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                                                <a href="<?php echo get_the_permalink($submenu[$i]->ID); ?>" ><?php echo get_the_title($submenu[$i]->ID); ?></a>
                                              </li>
                                          <?php
                                        }
                                      ?>
                                      <li class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12"><a class="seeMore" href="<?php echo get_the_permalink($popularesm[$m]->ID) ?>"><?php _e("[:es]VER MÁS SOLUCIONES...[:en]MORE SOLUTIONS..."); ?></a><i class="icon dripicons-arrow-thin-right"></i></li>
                                      </ul>
                                    </div>
                                  </div>
                                </div>
                              </div>
                          </li>

                      <?php
                        }
                ?>

              </ul>
            </div>
            <div class="secondMenu">
              <ul class="navbar-nav mr-auto">

                <li class="nav-item">
                  <a class="<?php echo ($ideActaul == 136)?'activeMnu':''; ?>" href="<?php echo get_the_permalink(136); ?>"><?php echo get_the_title(136); ?></a>
                  <!--<div class="submenu special">
                    <ul>
                      <li>
                    <a class="<?php echo ($ideActaul == 136)?'activeMnu':''; ?>" href="<?php echo get_the_permalink(1465); ?>"><?php echo get_the_title(1465); ?></a>
                    </li>
                    <li>
                    <a class="<?php echo ($ideActaul == 136)?'activeMnu':''; ?>" href="<?php echo get_the_permalink(1419); ?>"><?php echo get_the_title(1419); ?></a>
                  </li>
                </ul>
              </div>-->
                </li>
                <li class="nav-item">
                  <a class="<?php echo ($ideActaul == 160)?'activeMnu':''; ?>" href="<?php echo get_the_permalink(160); ?>"><?php echo get_the_title(160); ?></a>
                </li>
                <li class="nav-item">
                  <a class="<?php echo ($ideActaul == 187)?'activeMnu':''; ?>" href="<?php echo get_the_permalink(187); ?>"><?php echo get_the_title(187); ?></a>
                </li>
                <li class="nav-item idioma">
                  <a href="<?php echo qts_get_url($another[$existe]);?>"><?php echo $txtIdioma[$existe]; ?><span class="icon"></span></a>
                </li>
              </ul>
            </div>
          </div>

      </nav>
    </div>

  </header>
  <!--<div class="submenu">
      <div class="container">
        <div class="claimMenu">
          <?php
            $idUnidad = 35;
            $cliamE = get_field('claim_unidades',$idUnidad);
          ?>
          <div class="eachClaim">
            <img src="<?php echo $cliamE['url'] ?>" alt="<?php echo $cliamE['alt'] ?>">
          </div>
        </div>
        <ul class="row submenuHere">
        <?php
          $submenu = get_field('soluciones_unidades',$idUnidad);
          $conteo = count($submenu);
          for ($i=0; $i < $conteo; $i++) {
            ?>
                <li class="col-12 col-sm-12 col-md-6 col-lg-4 col-xl-4">
                  <a href="<?php echo get_permalink($submenu[$i]->ID); ?>"><?php echo get_the_title($submenu[$i]->ID); ?></a>
                </li>
            <?php
          }
        ?>
        </ul>
      </div>
    </div>-->



  <div class="gralContainer">
