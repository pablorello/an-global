<?php // INDEX ENGLISH ?>
<?php require_once('idioma.php'); ?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="apple-mobile-web-app-capable" content="yes" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="apple-mobile-web-app-status-bar-style" content="black" />
        <meta name="apple-mobile-web-app-status-bar-style" content="black" />
        <meta name="keywords" content="AN, AN Global, ANGLOBAL, The power of AN, Digital Marketing, Analytics, Cloud Services, Consultoría IT, IT, Analítica Avanzada, Cloud, Análisis de data, herramientas de medicion digital, Desarrollo de software, administracion de aplicaciones e infraestructura, desarrollo de aplicaciones, diseño de plataformas digitales">
        <link rel="shortcut icon" href="../static/images/favicon.ico?v=1">

        <link rel="stylesheet" href="../startup/flat-ui/bootstrap/css/bootstrap.css">
        <link href="https://fonts.googleapis.com/css?family=Roboto+Slab:300,400,700|Open+Sans:300,800i" rel="stylesheet">

        <link rel="stylesheet" href="../startup/flat-ui/css/flat-ui.css">
        <!-- Using only with Flat-UI (free)-->
        <link rel="stylesheet" href="../startup/common-files/css/icon-font.css">
        <!-- end -->
        <link rel="stylesheet" href="../startup/common-files/css/animations.css">
        <link rel="stylesheet" href="../static/css/owl.carousel.css">
        <link rel="stylesheet" href="../static/css/style.css?v=1.1.5">
        <link rel="stylesheet" href="../static/css/flag-icon.css">

        <title></title>
        <script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-99908869-1', 'auto');
  ga('send', 'pageview');

</script>
    </head>

    <body>
        <div class="page-wrapper"><header class="header-23">
    <div class="mask">&nbsp;</div>
    <div class="popup-video">
        <span id="tache" onclick="cerrarModal()"><img src="http://www.anglobal.com/static/images/delete-button.png" alt=""></span>
        <iframe id="pPlayer" src="https://www.youtube.com/embed/RziSstMyR9U?enablejsapi=1&amp;controls=0&amp;rel=0" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
    </div>
    <div class="container">
        <div class="row">
            <div class="navbar col-sm-12" role="navigation">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle"></button>
                    <img src="http://www.anglobal.com/static/images/anglobal.png" alt="anglobal">
                </div>
                <div class="navbar-collapse collapse">
                    <ul class="nav pull-right">
                        <li><a href="#about" class="goto"><?php echo str_replace('"','',$textos[0]); ?></a></li>
                        <li><a href="#we_are" class="goto"><?php echo str_replace('"','',$textos[1]); ?></a></li>
                        <li><a href="#business" class="goto"><?php echo str_replace('"','',$textos[2]); ?></a></li>
                        <li><a href="#services" class="goto"><?php echo str_replace('"','',$textos[3]); ?></a></li>
                        <li><a href="#clients" class="goto"><?php echo str_replace('"','',$textos[4]); ?></a></li>
                    </ul>

                </div>
            </div>
        </div>
    </div>
</header>
<section class="header-23-sub bg-midnight-blue">
    <div id="bgVideo" class="background">&nbsp;</div>
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="hero-unit">
                    <a class="btn-play"><img src="http://www.anglobal.com/static/images/powerof.png" alt=""></a>
                    <a class="btn-play"  id="play" onclick="videoModal(this)" data-video='https://www.youtube.com/embed/UnKWIDwA0ek?enablejsapi=1&amp;controls=0&amp;rel=0'><img src="http://www.anglobal.com/static/images/playHome.png?v=1.1.1" alt=""></a>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="content-6" id="about">
    <div>
        <div class="container">

            <div class="contenedroslider">
                <h3><?php echo str_replace('"','',$textos[5]); ?></h3>
                <div class="owl-carousel owl-theme">
                    <div class="item">
                        <img src="http://www.anglobal.com/static/images/keyof.jpg?v=1.1.1" alt="" class="img-responsive hidden-xs hidden-sm">
                        <img src="http://www.anglobal.com/static/images/keyof_m.jpg" alt="" class="img-responsive hidden-md hidden-lg">
                        <div class="infoSlider">
                            <h3 class="whiteColor"><?php echo str_replace('"','',$textos[6]); ?></h3>
                            <p><?php echo str_replace('"','',$textos[7]); ?></p>
                        </div>
                    </div>
                    <div class="item">
                        <img src="http://www.anglobal.com/static/images/innovation.jpg?v=1" alt="" class="img-responsive hidden-xs hidden-sm">
                        <img src="http://www.anglobal.com/static/images/innovation.png" alt="" class="img-responsive hidden-md hidden-lg">
                        <div class="infoSlider">
                            <h3 class="nasoftColor"><?php echo str_replace('"','',$textos[8]); ?></h3>

                <p><?php echo str_replace('"','',$textos[9]); ?></p>
                        </div>
                    </div>
                    <div class="item">
                        <img src="http://www.anglobal.com/static/images/people.jpg" alt="" class="img-responsive hidden-xs hidden-sm">
                        <img src="http://www.anglobal.com/static/images/people.png" alt="" class="img-responsive hidden-md hidden-lg">
                        <div class="infoSlider">
                            <h3 class="nasoftColor"><?php echo str_replace('"','',$textos[10]); ?></h3>

                <p><?php echo str_replace('"','',$textos[11]); ?></p>
                        </div>
                    </div>
                    <div class="item">
                        <img src="http://www.anglobal.com/static/images/simplicity.jpg?v=1.1.1" alt="" class="img-responsive hidden-xs hidden-sm">
                        <img src="http://www.anglobal.com/static/images/simplicity_m.jpg" alt="" class="img-responsive hidden-md hidden-lg">
                        <div class="infoSlider">
                            <h3 class="nasoftColor"><?php echo str_replace('"','',$textos[12]); ?></h3>

                <p><?php echo str_replace('"','',$textos[13]); ?></p>
                        </div>
                    </div>

                </div>
            </div><!--contentslider-->
        </div>
    </div>
</section>
<section class="projects-4 weare" id="we_are">
    <div class="container">
        <h3 class="whiteColor"><?php echo str_replace('"','',$textos[14]); ?></h3>
        <span class="subtitt nasoftColor"><?php echo str_replace('"','',$textos[15]); ?></span>
        <p><?php echo str_replace('"','',$textos[16]); ?></p>

        <div class="projects row">
            <div class="project-wrapper col-sm-2"></div>
            <div class="project-wrapper col-sm-1 col-xs-4">
                <div class="project">
                    <div class="photo-wrapper">
                        <div class="photo"></div>
                        <a class="overlay"><img src="http://www.anglobal.com/static/images/mexico.png" alt=""></a>
                    </div>
                    <div class="info">
                        <?php echo str_replace('"','',$textos[17]); ?>
                    </div>
                </div>
            </div>

            <div class="project-wrapper col-sm-1 col-xs-4">
                <div class="project">
                    <div class="photo-wrapper">
                        <div class="photo"></div>
                        <a class="overlay"><img src="http://www.anglobal.com/static/images/usa.png" alt=""></a>
                    </div>
                    <div class="info">
                        <?php echo str_replace('"','',$textos[18]); ?>
                    </div>
                </div>
            </div>

            <div class="project-wrapper col-sm-1 col-xs-4">
                <div class="project">
                    <div class="photo-wrapper">
                        <div class="photo"></div>
                        <a class="overlay"><img src="http://www.anglobal.com/static/images/costa_rica.png" alt=""></a>
                    </div>
                    <div class="info">
                      <?php echo str_replace('"','',$textos[19]); ?>
                    </div>
                </div>
            </div>

            <div class="project-wrapper col-sm-1 col-xs-4">
                <div class="project">
                    <div class="photo-wrapper">
                        <div class="photo"></div>
                        <a class="overlay"><img src="http://www.anglobal.com/static/images/brasil.png" alt=""></a>
                    </div>
                    <div class="info">
                        <?php echo str_replace('"','',$textos[20]); ?>
                    </div>
                </div>
            </div>
             <div class="project-wrapper col-sm-1 col-xs-4">
                <div class="project">
                    <div class="photo-wrapper">
                        <div class="photo"></div>
                        <a class="overlay"><img src="http://www.anglobal.com/static/images/argentina.png" alt=""></a>
                    </div>
                    <div class="info">
                        <?php echo str_replace('"','',$textos[21]); ?>
                    </div>
                </div>
            </div>

            <div class="project-wrapper col-sm-1 col-xs-4">
                <div class="project">
                    <div class="photo-wrapper">
                        <div class="photo"></div>
                        <a class="overlay"><img src="http://www.anglobal.com/static/images/uk.png" alt=""></a>
                    </div>
                    <div class="info">
                      <?php echo str_replace('"','',$textos[22]); ?>
                    </div>
                </div>
            </div>
            <div class="project-wrapper col-sm-1 col-xs-4">
                <div class="project">
                    <div class="photo-wrapper">
                        <div class="photo"></div>
                        <a class="overlay"><img src="http://www.anglobal.com/static/images/espana.png" alt=""></a>
                    </div>
                    <div class="info">
                        <?php echo str_replace('"','',$textos[23]); ?>
                    </div>
                </div>
            </div>
            <div class="project-wrapper col-sm-1 col-xs-4">
                <div class="project">
                    <div class="photo-wrapper">
                        <div class="photo"></div>
                        <a class="overlay"><img src="http://www.anglobal.com/static/images/portugal.png" alt=""></a>
                    </div>
                    <div class="info">
                        <?php echo str_replace('"','',$textos[24]); ?>
                    </div>
                </div>
            </div>
            <div class="project-wrapper col-sm-2"></div>
        </div>
    </div>
    <!--/.container-->
</section>

<section class="content-6 v-center" id="business">
    <div>
        <div class="container">
            <h3><?php echo str_replace('"','',$textos[25]); ?></h3>

            <div class="row features">
                <div class="col-sm-6">
                    <div class="eachUnidad integration" onclick="videoModal(this)" data-video='https://www.youtube.com/embed/ZmET_IKVkPo?enablejsapi=1&amp;controls=0&amp;rel=0'>
                        <img src="http://www.anglobal.com/static/images/anglobal_02.png?V=1" alt="" class="img-responsive">

                        <p><?php echo str_replace('"','',$textos[26]); ?></p>


                        <a class="playB">
                            <img src="http://www.anglobal.com/static/images/play-button.png?v=1.1.1" alt="">
                        </a>
                        <div class="clearfix"></div>
                    </div>

                </div>
                <div class="col-sm-6">
                    <div class="eachUnidad experience" onclick="videoModal(this)" data-video='https://www.youtube.com/embed/5uyfYY6AiWk?enablejsapi=1&amp;controls=0&amp;rel=0'>
                        <img src="http://www.anglobal.com/static/images/anglobal_01.png?V=1" alt="" class="img-responsive">

                        <p><?php echo str_replace('"','',$textos[27]); ?></p>
                        <a class="playB">
                            <img src="http://www.anglobal.com/static/images/play-button.png?v=1.1.1" alt="">
                        </a>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
            <div class="row features">
                <div class="col-sm-6">
                    <div class="eachUnidad analytics" onclick="videoModal(this)" data-video='https://www.youtube.com/embed/xptnRTlkeHA?enablejsapi=1&amp;controls=0&amp;rel=0'>
                        <img src="http://www.anglobal.com/static/images/anglobal_04.png?V=1" alt="" class="img-responsive">

                        <p><?php echo str_replace('"','',$textos[28]); ?></p>
                       <a class="playB">
                            <img src="http://www.anglobal.com/static/images/play-button.png?v=1.1.1" alt="">
                        </a>
                        <div class="clearfix"></div>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="eachUnidad cloud" onclick="videoModal(this)" data-video='https://www.youtube.com/embed/BO9alvO-OPM?enablejsapi=1&amp;controls=0&amp;rel=0'>
                        <img src="http://www.anglobal.com/static/images/anglobal_03.png?V=1" alt="" class="img-responsive">

                        <p><?php echo str_replace('"','',$textos[29]); ?></p>
                       <a class="playB">
                            <img src="http://www.anglobal.com/static/images/play-button.png?v=1.1.1" alt="">
                        </a>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="images">
    <img src="http://www.anglobal.com/static/images/weare_desk.jpg" alt="" class="img-responsive hidden-xs hidden-sm">
    <img src="http://www.anglobal.com/static/images/weare_mob.jpg" alt="" class="img-responsive hidden-md hidden-lg">
</section>
<section class="projects-4 serviciosSec" id="services">
    <div class="container">
        <h3><?php echo str_replace('"','',$textos[30]); ?></h3>
        <div class="projects">
            <div class="project-wrapper col-sm-2 col-xs-4">
                <div class="project">
                    <div class="photo-wrapper ">
                        <div class="photo"></div>
                        <a class="overlay"><img src="http://www.anglobal.com/static/images/servicios/desarrolloSoftware.png" alt="Software development"></span></a>
                    </div>
                    <div class="info"><?php echo str_replace('"','',$textos[31]); ?>
                    </div>
                </div>
            </div>

            <div class="project-wrapper col-sm-2 col-xs-4">
                <div class="project">
                    <div class="photo-wrapper ">
                        <div class="photo"></div>
                        <a class="overlay"><img src="http://www.anglobal.com/static/images/servicios/media.png" alt="Media"></a>
                    </div>
                    <div class="info">
                       <?php echo str_replace('"','',$textos[32]); ?>
                    </div>
                </div>
            </div>

            <div class="project-wrapper col-sm-2 col-xs-4">
                <div class="project">
                    <div class="photo-wrapper ">
                        <div class="photo"></div>
                        <a class="overlay"><img src="http://www.anglobal.com/static/images/servicios/socialmedia.png" alt="Social Media"></a>
                    </div>
                    <div class="info">
                      <?php echo str_replace('"','',$textos[33]); ?>
                    </div>
                </div>
            </div>

            <div class="project-wrapper col-sm-2 col-xs-4">
                <div class="project">
                    <div class="photo-wrapper ">
                        <div class="photo"></div>
                        <a class="overlay"><img src="http://www.anglobal.com/static/images/servicios/amonApp.png" alt="Administration of applications and infrastructure"></a>
                    </div>
                    <div class="info">
                        <?php echo str_replace('"','',$textos[34]); ?>
                    </div>
                </div>
            </div>
             <div class="project-wrapper col-sm-2 col-xs-4">
                <div class="project">
                    <div class="photo-wrapper ">
                        <div class="photo"></div>
                        <a class="overlay"><img src="http://www.anglobal.com/static/images/servicios/consultoria.png" alt="Services of consultancy"></a>
                    </div>
                    <div class="info">
                        <?php echo str_replace('"','',$textos[35]); ?>
                    </div>
                </div>
            </div>

            <div class="project-wrapper col-sm-2 col-xs-4">
                <div class="project">
                    <div class="photo-wrapper ">
                        <div class="photo"></div>
                        <a class="overlay"><img src="http://www.anglobal.com/static/images/servicios/internet.png" alt="Internet of the things"></a>
                    </div>
                    <div class="info">
                      <?php echo str_replace('"','',$textos[36]); ?>
                    </div>
                </div>
            </div>

        </div>
        <div class="clearfix"></div>
        <div class="projects">
            <div class="project-wrapper col-sm-2 col-xs-4">
                <div class="project">
                    <div class="photo-wrapper ">
                        <div class="photo"></div>
                        <a class="overlay"><img src="http://www.anglobal.com/static/images/servicios/customer.png" alt="Customer experience"></span></a>
                    </div>
                    <div class="info">
                      <?php echo str_replace('"','',$textos[37]); ?>
                    </div>
                </div>
            </div>

            <div class="project-wrapper col-sm-2 col-xs-4">
                <div class="project">
                    <div class="photo-wrapper ">
                        <div class="photo"></div>
                        <a class="overlay"><img src="http://www.anglobal.com/static/images/servicios/content.png" alt="Content management"></a>
                    </div>
                    <div class="info">
                        <?php echo str_replace('"','',$textos[38]); ?>
                    </div>
                </div>
            </div>

            <div class="project-wrapper col-sm-2 col-xs-4">
                <div class="project">
                    <div class="photo-wrapper ">
                        <div class="photo"></div>
                        <a class="overlay"><img src="http://www.anglobal.com/static/images/servicios/prescriptiva.png" alt="Analytics Prescriptive"></a>
                    </div>
                    <div class="info">
                        <?php echo str_replace('"','',$textos[39]); ?>

                    </div>
                </div>
            </div>

            <div class="project-wrapper col-sm-2 col-xs-4">
                <div class="project">
                    <div class="photo-wrapper ">
                        <div class="photo"></div>
                        <a class="overlay"><img src="http://www.anglobal.com/static/images/servicios/avanzada.png" alt="Analytics Advanced"></a>
                    </div>
                    <div class="info">
                        <?php echo str_replace('"','',$textos[40]); ?>
                    </div>
                </div>
            </div>
             <div class="project-wrapper col-sm-2 col-xs-4">
                <div class="project">
                    <div class="photo-wrapper ">
                        <div class="photo"></div>
                        <a class="overlay"><img src="http://www.anglobal.com/static/images/servicios/appDevelopment.png" alt="Consulting marketing"></a>
                    </div>
                    <div class="info">
                        <?php echo str_replace('"','',$textos[41]); ?>
                    </div>
                </div>
            </div>

            <div class="project-wrapper col-sm-2 col-xs-4">
                <div class="project">
                    <div class="photo-wrapper ">
                        <div class="photo"></div>
                        <a class="overlay"><img src="http://www.anglobal.com/static/images/servicios/analitica.png" alt="Analytics"></a>
                    </div>
                    <div class="info">
                       <?php echo str_replace('"','',$textos[42]); ?>
                    </div>
                </div>
            </div>

        </div>
        <div class="clearfix"></div>
        <div class="projects">
            <div class="project-wrapper col-sm-2 col-xs-4">
                <div class="project">
                    <div class="photo-wrapper ">
                        <div class="photo"></div>
                        <a class="overlay"><img src="http://www.anglobal.com/static/images/servicios/appPerformance.png" alt="Application performance"></span></a>
                    </div>
                    <div class="info">
                        <?php echo str_replace('"','',$textos[43]); ?>
                    </div>
                </div>
            </div>

            <div class="project-wrapper col-sm-2 col-xs-4">
                <div class="project">
                    <div class="photo-wrapper ">
                        <div class="photo"></div>
                        <a class="overlay"><img src="http://www.anglobal.com/static/images/servicios/mobileDev.png" alt="Mobile development"></a>
                    </div>
                    <div class="info">
                        <?php echo str_replace('"','',$textos[44]); ?>
                    </div>
                </div>
            </div>

            <div class="project-wrapper col-sm-2 col-xs-4">
                <div class="project">
                    <div class="photo-wrapper ">
                        <div class="photo"></div>
                        <a class="overlay"><img src="http://www.anglobal.com/static/images/servicios/cloud.png" alt="Cloud services"></a>
                    </div>
                    <div class="info">
                        <?php echo str_replace('"','',$textos[45]); ?>
                    </div>
                </div>
            </div>

            <div class="project-wrapper col-sm-2 col-xs-4">
                <div class="project">
                    <div class="photo-wrapper ">
                        <div class="photo"></div>
                        <a class="overlay"><img src="http://www.anglobal.com/static/images/servicios/digmkt.png" alt="Digital Marketing"></a>
                    </div>
                    <div class="info">
                        <?php echo str_replace('"','',$textos[46]); ?>
                    </div>
                </div>
            </div>
             <div class="project-wrapper col-sm-2 col-xs-4">
                <div class="project">
                    <div class="photo-wrapper ">
                        <div class="photo"></div>
                        <a class="overlay"><img src="http://www.anglobal.com/static/images/servicios/uiux.png" alt="UX/UI"></a>
                    </div>
                    <div class="info">
                        <?php echo str_replace('"','',$textos[47]); ?>
                    </div>
                </div>
            </div>

            <div class="project-wrapper col-sm-2 col-xs-4">
                <div class="project">
                    <div class="photo-wrapper ">
                        <div class="photo"></div>
                        <a class="overlay"><img src="http://www.anglobal.com/static/images/servicios/dev.png" alt="DEV OPS"></a>
                    </div>
                    <div class="info">
                        <?php echo str_replace('"','',$textos[48]); ?>
                    </div>
                </div>
            </div>

        </div>
        <div class="clearfix"></div>
        <div class="projects">
            <div class="project-wrapper col-sm-2 col-xs-4">
                <div class="project">
                    <div class="photo-wrapper ">
                        <div class="photo"></div>
                        <a class="overlay"><img src="http://www.anglobal.com/static/images/servicios/appDev.png" alt="APP develop"></span></a>
                    </div>
                    <div class="info">
                        <?php echo str_replace('"','',$textos[49]); ?>
                    </div>
                </div>
            </div>

            <div class="project-wrapper col-sm-2 col-xs-4">
                <div class="project">
                    <div class="photo-wrapper ">
                        <div class="photo"></div>
                        <a class="overlay"><img src="http://www.anglobal.com/static/images/servicios/tiempo.png" alt="Tiempo y materiales"></a>
                    </div>
                    <div class="info">
                        <?php echo str_replace('"','',$textos[50]); ?>
                    </div>
                </div>
            </div>

            <div class="project-wrapper col-sm-2 col-xs-4">
                <div class="project">
                    <div class="photo-wrapper ">
                        <div class="photo"></div>
                        <a class="overlay"><img src="http://www.anglobal.com/static/images/servicios/businnes.png" alt="Business applications"></a>
                    </div>
                    <div class="info">
                        <?php echo str_replace('"','',$textos[51]); ?>
                    </div>
                </div>
            </div>

            <div class="project-wrapper col-sm-2 col-xs-4">
                <div class="project">
                    <div class="photo-wrapper ">
                        <div class="photo"></div>
                        <a class="overlay"><img src="http://www.anglobal.com/static/images/servicios/bap.png" alt="Business applications on premise"></a>
                    </div>
                    <div class="info">
                        <?php echo str_replace('"','',$textos[52]); ?>
                    </div>
                </div>
            </div>


        </div>

    </div>
    <!--/.container-->
</section>
<section class="images">
    <img src="http://www.anglobal.com/static/images/npower.jpg" alt="" class="img-responsive hidden-xs hidden-sm">
    <img src="http://www.anglobal.com/static/images/thepowerof.jpg" alt="" class="img-responsive hidden-md hidden-lg">
</section>
<section class="projects-4 clientesList" id="clients">

    <div class="">
    <div class="container">
         <h3 class="whiteColor"><?php echo str_replace('"','',$textos[53]); ?></h3>
        <img src="http://www.anglobal.com/static/images/clientes.png?v=1.1.1" alt="" class="img-responsive hidden-xs hidden-sm">
        <img src="http://www.anglobal.com/static/images/clientes_mob.png?v=1.1.1" alt="" class="img-responsive hidden-md hidden-lg">
    </div>

    </div>
    <!--/.container-->
</section>
<!--
<section class="projects-4 contactForm" id="contact">
  <h3 class="whiteColor">CONTACT US</h3>
  <div class="container">

    <form id="form">
        <div class="row">
          <div class="col-md-1">
            <label>Name </label>
          </div>
          <div class="col-md-3">
            <input type="text" id="name" />
          </div>
        <div class="col-md-1">
            <label>Email </label>
          </div>
          <div class="col-md-3">
            <input type="text" id="email"/>
          </div>
          <div class="col-md-1">
            <label>Phone </label>
          </div>
          <div class="col-md-3">
            <input type="phone" id="phone"/>
          </div>
        </div>
          <div class="row">
          <div class="col-md-1">
            <label>Message</label>
          </div>
        <div class="col-md-11">
            <textarea type="text" id="comment"></textarea>
          </div>
        </div>
          <div class="row">
          <div class="col-md-3 col-md-push-9">
            <button type="button" id="submit" onclick="sendForm()">Send <i class='fui-arrow-right'></i></button>
          </div>
        </div>
    </form>



  </div>

  <div id="output"></div>
</section>
-->
<footer class="footer-7 ">
    <div class="container">
        <a href="#" class="goto casita"><span class="fui-home"> </span></a>
        <nav>
            <ul class="col-xs-12 col-sm-12 col-md-12 col-lg-12 espUl">
                <li><a href="mailto:weare@anglobal.com">weare@anglobal.com</a></li>
            </ul>
            <ul class="col-xs-12 col-sm-12 col-md-12 col-lg-12 espUl">
                <li><a href="tel:5552581400">+52 (55) 5258 1400</a></li>
            </ul>
            <ul class="col-xs-12 col-sm-12 col-md-12 col-lg-12 espUl">
                <li><a href="/en/privacy_notice.php">Privacy notice</a></li>
            </ul>
            <ul class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
              <li class="flag-wrapper">
                  <a href="/es" class="img-thumbnail flag flag-icon-background flag-icon-mx"></a>
              </li>
              <li class="flag-wrapper">
                  <a href="/en" class="img-thumbnail flag flag-icon-background flag-icon-us"></a>
              </li>
              <li class="flag-wrapper">
                  <a href="/pt" class="img-thumbnail flag flag-icon-background flag-icon-br"></a>
              </li>
            </ul>
            <div class="clearfix"></div>
        </nav>
        <div class="social-btns" target="_blank">

            <a class="iab">
                <img src="http://www.anglobal.com/static/images/logo_white.png" alt="">
            </a>
            <!--<a href="https://twitter.com/AGSNASOFT" target="_blank">
                <div class="fui-twitter"></div>
                <div class="fui-twitter"></div>
            </a>
            <a href="https://www.linkedin.com/company/28186?trk=tyah&trkInfo=clickedVertical%3Acompany%2CclickedEntityId%3A28186%2Cidx%3A2-2-3%2CtarId%3A1448670213103%2Ctas%3Aags%20nasoft">
                <div class="fui-linkedin"></div>
                <div class="fui-linkedin"></div>
            </a>-->
        </div>
    </div>
</footer>            <!-- Placed at the end of the document so the pages load faster -->
            <script src="../startup/common-files/js/jquery-1.10.2.min.js"></script>

            <script src="../startup/common-files/js/jquery.scrollTo-1.4.3.1-min.js"></script>
            <script src="../startup/common-files/js/jquery.sharrre.min.js"></script>
            <script src="../startup/common-files/js/bootstrap.min.js"></script>
            <script src="../static/js/owl.carousel.js"></script>
            <script src="../startup/common-files/js/masonry.pkgd.min.js"></script>
            <script src="../startup/common-files/js/modernizr.custom.js"></script>
            <script src="../startup/common-files/js/page-transitions.js"></script>
            <script src="../startup/common-files/js/easing.min.js"></script>
            <script src="../startup/common-files/js/jquery.svg.js"></script>
            <script src="../startup/common-files/js/jquery.svganim.js"></script>
            <script src="../startup/common-files/js/jquery.backgroundvideo.min.js"></script>
            <script src="../startup/common-files/js/froogaloop.min.js"></script>
            <script src="../startup/common-files/js/startup-kit.js?v=1.2.3"></script>
             <script src="../static/js/jquery.malihu.PageScroll2id.min.js"></script>
            <script src="../static/js/gneral.js?v=1.2.13"></script>
        </div>
     <div id="myModal" class="modal fade" role="dialog">
      <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <button type="button" class="close" data-dismiss="modal">&times;</button>

          <div class="modal-body">
            <iframe id="pPlayerB" src="http://player.vimeo.com/video/80282064?title=0&amp;byline=0&amp;portrait=0&amp;api=1&amp;player_id=pPlayerB" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
          </div>
        </div>

      </div>
    </div>
    </body>
</html>
