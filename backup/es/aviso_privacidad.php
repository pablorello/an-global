<?php // INDEX ENGLISH ?>
<?php require_once('idioma.php'); ?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="apple-mobile-web-app-capable" content="yes" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="apple-mobile-web-app-status-bar-style" content="black" />
        <meta name="apple-mobile-web-app-status-bar-style" content="black" />
        <meta name="keywords" content="AN, AN Global, ANGLOBAL, The power of AN, Digital Marketing, Analytics, Cloud Services, Consultoría IT, IT, Analítica Avanzada, Cloud, Análisis de data, herramientas de medicion digital, Desarrollo de software, administracion de aplicaciones e infraestructura, desarrollo de aplicaciones, diseño de plataformas digitales">
        <link rel="shortcut icon" href="http://anglobal.com/static/images/favicon.ico?v=1">

        <link rel="stylesheet" href="http://anglobal.com/startup/flat-ui/bootstrap/css/bootstrap.css">
        <link href="https://fonts.googleapis.com/css?family=Roboto+Slab:300,400,700|Open+Sans:300,800i" rel="stylesheet">

        <link rel="stylesheet" href="http://anglobal.com/startup/flat-ui/css/flat-ui.css">
        <!-- Using only with Flat-UI (free)-->
        <link rel="stylesheet" href="http://anglobal.com/startup/common-files/css/icon-font.css">
        <!-- end -->
        <link rel="stylesheet" href="http://anglobal.com/startup/common-files/css/animations.css">
        <link rel="stylesheet" href="http://anglobal.com/static/css/owl.carousel.css">
        <link rel="stylesheet" href="http://anglobal.com/static/css/style.css?v=1.1.7">
        <link rel="stylesheet" href="http://anglobal.com/static/css/flag-icon.css">

        <title></title>
        <script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-99908869-1', 'auto');
  ga('send', 'pageview');

</script>
    </head>

    <body>
        <div class="page-wrapper pterm">
  <header class="header-23">

    <div class="container">
        <div class="row">
            <div class="navbar col-sm-12 terminosNav" role="navigation">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle"></button>
                    <a href="/es"><img src="http://www.anglobal.com/static/images/anglobal.png" alt="anglobal"></a>
                </div>
                <div class="navbar-collapse collapse">
                    <ul class="nav pull-right">
                        <li><a href="/es#about" class="goto"><?php echo str_replace('"','',$textos[0]); ?></a></li>
                        <li><a href="/es#we_are" class="goto"><?php echo str_replace('"','',$textos[1]); ?></a></li>
                        <li><a href="/es#business" class="goto"><?php echo str_replace('"','',$textos[2]); ?></a></li>
                        <li><a href="/es#services" class="goto"><?php echo str_replace('"','',$textos[3]); ?></a></li>
                        <li><a href="/es#clients" class="goto"><?php echo str_replace('"','',$textos[4]); ?></a></li>
                    </ul>

                </div>
            </div>
        </div>
    </div>
</header>
<div class="container termC">
  <h5>AVISO DE PRIVACIDAD</h5>
  <p>El presente Anexo constituye el aviso de privacidad para efectos de lo Dispuesto en la Ley Federal de Protecci&oacute;n de Datos Personales en Posesi&oacute;n de Particulares (la &ldquo;<u>LFPD</u>&rdquo;) y las disposiciones que emanan o se relacionan con ella. El presente Aviso de Privacidad tiene los siguientes t&eacute;rminos y condiciones:</p>
  <ol>
  <li><strong>T&eacute;rminos definidos.</strong> Para los efectos de este Aviso de Privacidad, los siguientes t&eacute;rminos tendr&aacute;n los siguientes significados, en el entendido que salvo que en el presente Anexo se definan de otra forma, los t&eacute;rminos que se utilicen en este Aviso de Privacidad y que no est&eacute;n definidos en el mismo tendr&aacute;n el significado que se les atribuye en la LFPD:</li>
  </ol>
  <p><span>"datos personales"</span>, tiene el significado que se le atribuye en la LFPD.</p>
  <p><span>"Derechos ARCO"</span>, significa los derechos de acceso, rectificaci&oacute;n, cancelaci&oacute;n u oposici&oacute;n que de conformidad con lo establecido por la LFPD y sujeto a las excepciones establecidas en la misma y en este Aviso de Privacidad, cada Titular tiene, en relaci&oacute;n con los datos personales recabados por el Responsable y/o sus encargados, y que se describen a continuaci&oacute;n:</p>
  <p>DERECHO DE ACCESO. Es decir, el derecho a conocer sobre los datos personales de cada Titular que obren en poder del Responsable de que se trate o de sus encargados, y a qui&eacute;n le han sido compartidos y con qu&eacute; finalidad.</p>
  <p>DERECHO A LA RECTIFICACI&Oacute;N. Cada Titular tiene derecho a que sus datos personales sean rectificados cuando &eacute;stos son inexactos o incompletos.</p>
  <p>DERECHO DE CANCELACI&Oacute;N. Cada Titular tiene derecho a solicitar en cualquier momento que se supriman sus datos personales, lo cual suceder&aacute; una vez que transcurra el periodo de bloqueo. El bloqueo implica la identificaci&oacute;n y conservaci&oacute;n de los datos personales, una vez que se cumpa la finalidad para la cual fueron recabados, y tiene como prop&oacute;sito determinar las posibles responsabilidades en relaci&oacute;n con su tratamiento, hasta el plazo de prescripci&oacute;n legal o contractual de &eacute;stas. Durante dicho periodo, sus datos personales no ser&aacute;n objeto de tratamiento y transcurrido &eacute;ste, se proceder&aacute; con su cancelaci&oacute;n en la base de datos correspondiente. Una vez cancelado el dato correspondiente, el Responsable de que se trate le dar&aacute; al Titular respectivo el aviso correspondiente. En caso de que los datos personales hubiesen sido transmitidos con anterioridad a la fecha de rectificaci&oacute;n o cancelaci&oacute;n y sigan siendo tratados por terceros, el Responsable har&aacute; de su conocimiento dicha solicitud de rectificaci&oacute;n o cancelaci&oacute;n, para que proceda a efectuarla tambi&eacute;n.</p>
  <p>DERECHO DE OPOSICI&Oacute;N. El Titular tiene en todo momento el derecho a solicitar, siempre que tenga una causa leg&iacute;tima, que el Responsable deje de tratar sus datos personales.</p>
  <p>"Responsable", significa la persona o parte que reciba datos personales del Titular.<br /> </p>
  <p>"Titular", significa la persona o parte titular de los datos personales o autorizada para entregar datos personales de un tercero conforme a las leyes aplicables, que entregue dichos datos personales al Responsable.</p>
  <ol start="2">
  <li><strong>Consentimiento del Titular.</strong> Para efectos de lo dispuesto en la LFPD y en particular en su art&iacute;culo 17 (diecisiete), cada Titular manifiesta que (i) el presente Aviso de Privacidad le ha sido dado a conocer por el Responsable correspondiente en forma previa a la celebraci&oacute;n de este Anexo, y (ii) la firma del presente Anexo por cada Titular constituye su consentimiento respecto del tratamiento de sus datos personales para efectos de la LFPD y dem&aacute;s legislaci&oacute;n aplicable. En caso de que los datos personales entregados por el Titular de que se trate al Responsable respectivo en t&eacute;rminos de o para el cumplimiento de este Anexo constituyan datos personales sensibles, la suscripci&oacute;n de este Anexo por el Titular respectivo constituye su consentimiento respecto del tratamiento por el Responsable de sus datos personales sensibles para efectos de la LFPD y dem&aacute;s legislaci&oacute;n aplicable.</li>
  </ol>
  <p>El consentimiento del Titular respectivo podr&aacute; ser revocado en cualquier momento por &eacute;ste sin que se le atribuyan efectos retroactivos, en los t&eacute;rminos y conforme a los procedimientos establecidos m&aacute;s adelante para ello conforme a este Aviso de Privacidad.</p>
  <p>No obstante cualquier disposici&oacute;n de este Aviso de Privacidad, cada Titular reconoce que no se requerir&aacute; de su consentimiento para el tratamiento de datos personales por parte del Responsable respectivo o de terceros en cualquiera de los casos se&ntilde;alados en el art&iacute;culo 10 (diez) de la LFPD.</p>
  <ol start="3">
  <li><strong>Objeto del Aviso de Privacidad; Finalidad de los datos personales.</strong> El presente Aviso de Privacidad tiene por objeto establecer los t&eacute;rminos y condiciones en virtud de las cuales cada Responsable (o el encargado que designe dicho Responsable) (i) recibir&aacute; y proteger&aacute; los datos personales del Titular respectivo, a efecto de proteger su privacidad y su derecho a la autodeterminaci&oacute;n informativa, y en cumplimiento de lo dispuesto en la LFDP; (ii) utilizar&aacute; los datos personales del Titular respectivo, y (ii) realizar&aacute; en su caso las transferencias de datos personales a terceros.</li>
  </ol>
  <p>El Responsable que corresponda recolectar&aacute; y tratar&aacute; los datos personales del Titular respectivo, es decir, aquella informaci&oacute;n que puede identificarle, como su nombre y apellidos; su fecha de nacimiento; su domicilio, sea particular, del trabajo, o fiscal; su direcci&oacute;n de correo electr&oacute;nico, personal o del trabajo; su clave de identificaci&oacute;n en redes sociales; su n&uacute;mero telef&oacute;nico, particular o del trabajo; su n&uacute;mero de tel&eacute;fono celular; su n&uacute;mero de tarjeta de cr&eacute;dito; su clave del Registro Federal de Contribuyentes (RFC); su Clave &Uacute;nica de Registro de Poblaci&oacute;n (CURP); su n&uacute;mero de afiliaci&oacute;n al Instituto Mexicano del Seguro Social; y sus preferencias de consumo y navegaci&oacute;n en p&aacute;ginas de Internet o sitios. La recolecci&oacute;n de datos personales podr&aacute; efectuarse cuando el Titular que corresponda visita los puntos de venta del Responsable o sus afiliadas o distribuidores autorizados, se comunica v&iacute;a telef&oacute;nica con el Responsable respectivo o con sus encargados (incluyendo centros de atenci&oacute;n a clientes), o bien, mediante el uso de correos electr&oacute;nicos y/o mensajes cortos de texto (MSM), o mediante la utilizaci&oacute;n del sitios Web mediante el suministro voluntario de informaci&oacute;n a trav&eacute;s de las ventanas de di&aacute;logo habilitadas en el sitio, o mediante el uso de herramientas de captura autom&aacute;tica de datos, o mediante entrega directa al Responsable. Dichas herramientas le permiten recolectar la informaci&oacute;n que env&iacute;a su navegador al dicho sitio Web, tales como el tipo de navegador que utiliza, el idioma de usuario, los tiempos de acceso, y la direcci&oacute;n IP de sitios Web que utilizo para acceder al sitio del Responsable o sus afiliadas o encargados.</p>
  <p>El Responsable aplicable tambi&eacute;n recolecta datos personales de fuentes de acceso p&uacute;blico y de otras fuentes disponibles en el mercado a las que el Titular correspondiente pudo haber dado su consentimiento para compartir su informaci&oacute;n personal o que le haya proporcionado informaci&oacute;n demogr&aacute;fica an&oacute;nima asociada a un &aacute;rea geogr&aacute;fica determinada.</p>
  <p>Los datos personales del cada Titular son recolectados y tratados por el Responsable correspondiente o sus encargados con la finalidad de permitirle al Titular que corresponda, sea de manera presencial, mediante comunicaci&oacute;n telef&oacute;nica o v&iacute;a fax, o a trav&eacute;s del llenado de formatos impresos o formatos electr&oacute;nicos de sus correspondientes Sitios Web, o mediante correo electr&oacute;nico, llevar a cabo las siguientes actividades con el Responsable respectivo:</p>
  <ul>
  <li>a) &nbsp;Solicitar, comprar, cambiar, o devolver productos;</li>
  <li>b) &nbsp;Solicitar, contratar, cambiar o cancelar servicios;</li>
  <li>c) &nbsp;Efectuar pagos en l&iacute;nea;</li>
  <li>d) &nbsp;Solicitar factura o comprobante fiscal digital;</li>
  <li>e) &nbsp;Solicitar una cotizaci&oacute;n, informaci&oacute;n o muestras gratuitas de productos y servicios;</li>
  <li>f) &nbsp;Solicitar la entrega, reparaci&oacute;n o cumplimiento de garant&iacute;a de productos;</li>
  <li>g) &nbsp;Solicitar la prestaci&oacute;n de servicios o el cumplimiento de garant&iacute;a del servicio;</li>
  <li>h) &nbsp;Contactar el Servicio de Atenci&oacute;n a Clientes;</li>
  <li>i) &nbsp;Recibir publicidad impresa o a trav&eacute;s de medios electr&oacute;nicos, incluyendo comunicaciones con fines de mercadotecnia en l&iacute;nea, o telemarketing sobre productos y servicios;</li>
  <li>j) &nbsp;Crear perfiles personales;</li>
  <li>k) &nbsp;Participar en encuestas;</li>
  <li>l) &nbsp;Utilizar los distintos servicios de sus correspondientes Sitio Web incluyendo la descarga de contenidos y formatos;</li>
  <li>m) &nbsp;Notificar al Responsable sobre problemas con su sitio;</li>
  <li>n) &nbsp;Participar en chats y/o foros de discusi&oacute;n en l&iacute;nea sobre los productos y servicios;</li>
  <li>o) &nbsp;Participar en trivias, concursos, rifas, juegos y sorteos;</li>
  <li>p) &nbsp;Compartir sus comentarios o sugerencias sobre los productos y servicios;</li>
  <li>q) &nbsp;Solicitar empleo;</li>
  <li>r) &nbsp;Procesar pagos, y</li>
  <li>s) &nbsp;Cualquier otra actividad de naturaleza an&aacute;loga a las descritas en los incisos previamente citados.</li>
</ul>
  <p>Asimismo, el Responsable, directamente o a trav&eacute;s de sus encargados, puede utilizar sus datos personales para los siguientes fines:</p>
  <ul>
  <li>a) Realizar estudios sobre los datos demogr&aacute;ficos, intereses y comportamiento de sus clientes, consumidores, empleados, proveedores, y de aquellos terceros con los que trate;</li>
  <li>b) Efectuar estudios de mercado y de consumo a efecto de ofrecer productos y servicios personalizados, as&iacute; como publicidad y contenidos m&aacute;s adecuados a las necesidades de sus clientes y consumidores;</li>
  <li>c) Elaborar estad&iacute;sticas internas que indiquen los servicios y productos m&aacute;s apreciados por los diferentes segmentos de consumidores, clientes y otros usuarios de sus respectivos Sitios Web.</li>
  <li>d) Formalizar el proceso transaccional con sus clientes, consumidores y proveedores;</li>
  <li>e) Recolectar informaci&oacute;n de tarjetas de d&eacute;bito o tarjetas de cr&eacute;dito utilizadas para el pago de productos y servicios, a efecto de suministrar la informaci&oacute;n conducente a las sociedades de informaci&oacute;n crediticia, en t&eacute;rminos de la Ley para regular las Sociedades de Informaci&oacute;n Crediticia. Dicha informaci&oacute;n servir&aacute; como prueba en la eventualidad de una disputa por alguna compra o pago de servicios, as&iacute; como para procesar solicitudes de devoluci&oacute;n de art&iacute;culos y su correspondiente reembolso o cambio, y para instrumentar medidas de detecci&oacute;n y prevenci&oacute;n de fraude y robo;</li>
  <li>f) Llevar a cabo revisiones de datos personales para verificar la informaci&oacute;n proporcionada por los empleados y/o candidatos a ser empleados por el Responsable o sus encargados o afiliadas, sea a trav&eacute;s de terceros o por conducto de su Departamento de Recursos Humanos;</li>
  <li>g) Instrumentar en las tiendas c&aacute;maras de video-vigilancia para fines de seguridad y prevenci&oacute;n de p&eacute;rdidas. Las grabaciones se guardan en un &aacute;rea segura de acceso restringido a gerentes, directivos y, en su caso, a las autoridades competentes. Las grabaciones son rutinariamente destruidas a menos de que sean necesarias para la investigaci&oacute;n de un incidente o un para alg&uacute;n procedimiento judicial;</li>
  <li>h) Mantener un registro de compras, as&iacute; como de las operaciones e informaci&oacute;n revisada en las distintas secciones de sus correspondientes Sitio Web y que se recolecta a trav&eacute;s de herramientas de captura autom&aacute;tica de datos;</li>
  <li>e) Enviarle al Titular respectivo la notificaci&oacute;n de ofertas, avisos y/o mensajes promocionales a trav&eacute;s de sus correspondientes Sitios Web, que le ser&aacute;n enviados, a menos que el Titular manifieste a trav&eacute;s de los formatos impresos o electr&oacute;nicos, o mediante fax, su voluntad de no recibir tales ofertas. Ocasionalmente, tales ofertas pueden contener informaci&oacute;n de proveedores del Responsable.</li>
</ul>
  <ol start="4">
  <li><strong>Transferencias de Datos.</strong> Cada Titular manifiesta su consentimiento expreso para que el Responsable correspondiente o cualquier encargado realicen transferencias de datos personales a terceros nacionales o extranjeros, en el entendido de que el tratamiento que dichos terceros den a los datos personales de cada Titular deber&aacute; ajustarse a lo establecido en este Aviso de Privacidad</li>
  </ol>
  <p>Para efectos de lo establecido en esta Secci&oacute;n 4, pero sujeto a lo establecido en el &uacute;ltimo p&aacute;rrafo de la misma, cada Responsable le informa a cada Titular que con el objeto de poder entregar productos, servicios y soluciones a sus clientes, consumidores, empleados, proveedores y dem&aacute;s usuarios de sus servicios, el Responsable correspondiente y/o sus encargados o afiliadas han celebrado o celebrar&aacute;n diversos acuerdos comerciales con proveedores de productos y servicios, tanto en territorio nacional como en el extranjero, para que le suministren, entre otros servicios, los de telefon&iacute;a fija, de larga distancia, Internet, televisi&oacute;n, telefon&iacute;a celular y diferentes servicios de telecomunicaciones. La autorizaci&oacute;n de cada Titular otorgada conforme a esta Secci&oacute;n 4 faculta al Responsable correspondiente y/o a sus encargados a transmitir datos personales del Titular respectivo a dichos proveedores, en el entendido de que dichos proveedores est&aacute;n obligados, por virtud del contrato correspondiente, a mantener la confidencialidad de los datos personales suministrados por el Responsable y/o sus encargados y a observar el presente Aviso de Privacidad. El Responsable correspondiente y/o sus encargados podr&aacute;n transferir los datos personales recolectados del Titular de que se trate a su sociedad matriz o controladora, as&iacute; como a sus subsidiarias o afiliadas bajo el control del Responsable respectivo, as&iacute; como a cualquier otra sociedad del mismo grupo al que pertenezca dicho Responsable y que operen con los mismos procesos y pol&iacute;ticas internas, sea que se encuentren en territorio nacional o en el extranjero. Tambi&eacute;n podr&aacute; transferir sus datos personales a otros terceros que le apoyen para cumplir con los contratos o relaciones jur&iacute;dicas que tenga con el Titular de que se trate.</p>
  <p>No obstante lo dispuesto en esta Secci&oacute;n 4 o en cualquier otro lugar de este Aviso de Privacidad, cada Titular reconoce y acepta que el Responsable respectivo no requiere de autorizaci&oacute;n ni confirmaci&oacute;n de dicho Titular para realizar transferencias de datos personales nacionales o internacionales en los casos previstos en el art&iacute;culo 37 (treinta y siete) de la LFPD o en cualquier otro caso de excepci&oacute;n previsto por la LFPD o la dem&aacute;s legislaci&oacute;n aplicable.</p>

  <ol start="5">
  <li><strong>Retenci&oacute;n y seguridad de los datos personales.</strong> Cada Responsable y/o sus encargados conservar&aacute;n los datos personales del Titular de que se trate durante el tiempo que sea necesario para procesar sus solicitudes de informaci&oacute;n, productos y/o servicios, as&iacute; como para mantener los registros contables, financieros y de auditoria en t&eacute;rminos de la LFPD y de la legislaci&oacute;n mercantil, fiscal y administrativa vigente. Los datos personales del Titular de que se trate recolectados por el Responsable correspondiente y/o sus encargados se encontrar&aacute;n protegidos por medidas de seguridad administrativas, t&eacute;cnicas y f&iacute;sicas adecuadas contra el da&ntilde;o, p&eacute;rdida, alteraci&oacute;n, destrucci&oacute;n o uso, acceso o tratamiento no autorizados, de conformidad con lo dispuesto en la LFPD y de la regulaci&oacute;n administrativa derivada de la misma.</li>
  </ol>
  <ol start="6">
  <li><strong>Departamento de datos personales/oficina de privacidad; domicilio.</strong> Cada Responsable pondr&aacute; a disposici&oacute;n de cada Titular el departamento de datos personales / oficial de privacidad o similar, quien tendr&aacute; a su cargo la recepci&oacute;n, registro y atenci&oacute;n de sus solicitudes para ejercer su derecho de acceso, rectificaci&oacute;n, cancelaci&oacute;n y oposici&oacute;n a sus datos personales, as&iacute; como para limitar el uso o divulgaci&oacute;n de sus datos, y los dem&aacute;s derechos previstos en la LFPD.</li>
  </ol>
</div>
<footer class="footer-7 ">
    <div class="container">
        <a href="#" class="goto casita"><span class="fui-home"> </span></a>
        <nav>
            <ul class="col-xs-12 col-sm-12 col-md-12 col-lg-12 espUl">
                <li><a href="mailto:weare@anglobal.com">weare@anglobal.com</a></li>
            </ul>
            <ul class="col-xs-12 col-sm-12 col-md-12 col-lg-12 espUl">
                <li><a href="tel:5552581400">+52 (55) 5258 1400</a></li>
            </ul>
            <ul class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
              <li class="flag-wrapper">
                  <a href="/es" class="img-thumbnail flag flag-icon-background flag-icon-mx"></a>
              </li>
              <li class="flag-wrapper">
                  <a href="/en" class="img-thumbnail flag flag-icon-background flag-icon-us"></a>
              </li>
              <li class="flag-wrapper">
                  <a href="/pt" class="img-thumbnail flag flag-icon-background flag-icon-br"></a>
              </li>
            </ul>
            <div class="clearfix"></div>
        </nav>
        <div class="social-btns" target="_blank">

            <a class="iab">
                <img src="http://www.anglobal.com/static/images/logo_white.png" alt="">
            </a>
        </div>
    </div>
</footer>            <!-- Placed at the end of the document so the pages load faster -->
            <script src="http://anglobal.com/startup/common-files/js/jquery-1.10.2.min.js"></script>

            <script src="http://anglobal.com/startup/common-files/js/jquery.scrollTo-1.4.3.1-min.js"></script>
            <script src="http://anglobal.com/startup/common-files/js/jquery.sharrre.min.js"></script>
            <script src="http://anglobal.com/startup/common-files/js/bootstrap.min.js"></script>
            <script src="http://anglobal.com/static/js/owl.carousel.js"></script>
            <script src="http://anglobal.com/startup/common-files/js/masonry.pkgd.min.js"></script>
            <script src="http://anglobal.com/startup/common-files/js/modernizr.custom.js"></script>
            <script src="http://anglobal.com/startup/common-files/js/page-transitions.js"></script>
            <script src="http://anglobal.com/startup/common-files/js/easing.min.js"></script>
            <script src="http://anglobal.com/startup/common-files/js/jquery.svg.js"></script>
            <script src="http://anglobal.com/startup/common-files/js/jquery.svganim.js"></script>
            <script src="http://anglobal.com/startup/common-files/js/jquery.backgroundvideo.min.js"></script>
            <script src="http://anglobal.com/startup/common-files/js/froogaloop.min.js"></script>
            <script src="http://anglobal.com/startup/common-files/js/startup-kit.js?v=1.2.3"></script>
             <script src="http://anglobal.com/static/js/jquery.malihu.PageScroll2id.min.js"></script>
            <script src="http://anglobal.com/static/js/gneral.js?v=1.2.13"></script>
        </div>
     <div id="myModal" class="modal fade" role="dialog">
      <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <button type="button" class="close" data-dismiss="modal">&times;</button>

          <div class="modal-body">
            <iframe id="pPlayerB" src="http://player.vimeo.com/video/80282064?title=0&amp;byline=0&amp;portrait=0&amp;api=1&amp;player_id=pPlayerB" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
          </div>
        </div>

      </div>
    </div>
    </body>
</html>
