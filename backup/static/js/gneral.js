$(document).ready(function(){
	altura();

	$('.owl-carousel').owlCarousel({
	     autoPlay :true,
       touchDrag : false,
      navigation : true, // Show next and prev buttons
      slideSpeed : 600,
      paginationSpeed : 400,
      singleItem:true,
       navigationText: [
      "<i class='fui-arrow-left' aria-hidden='true'></i>",
      "<i class='fui-arrow-right' aria-hidden='true'></i>",
      ],
      responsive : {
          0 : {
              autoHeight:false
          },
          // breakpoint from 480 up
          480 : {
              autoHeight:false
          },
          // breakpoint from 768 up
          768 : {
              autoHeight:true
          }
      }
      });
			/*

*/
  (function($){
        $(window).load(function(){
            $("a.goto").mPageScroll2id();
            $("a.goto").click(function(){
              $(this).parent().parent().children('li').removeClass('active');
              $(this).parent().addClass('active');
              $('.navbar-collapse').removeClass('in');
              var stateObj = { foo: "bar" };
              var hash = $(this).attr('href');
              history.pushState(stateObj, "agnglobal", hash);
            });
        });
    })(jQuery);


/*
		$(document).on("submit", "form", function(e){
			var formData = {
							'user_name': $('input[name=user_name]').val(),
							'user_email': $('input[name=user_email]').val(),,
							'user_text': $('input[name=user_text]').val(),
					};
					console.log(formData);

			$.ajax({
					url: "send.php",
					type: "POST",
					data: formData,
					success: function(data) {
							$('#output').html(data);
			},
			});
			e.preventDefault();
			 return  false;
		});

		*/

});
$(window).resize(function() {
	altura();
});
function sendForm() {
    var name = document.getElementById("name").value;
    var email = document.getElementById("email").value;
    var comment = document.getElementById("comment").value;
    var dataString = 'name1=' + name + '&email1=' + email + '&comment=' + comment;
    if (name == '' || email == '' || comment == '')
    {
        alert("Por favor llena los campos indicados");
    }
    else
    {
        $.ajax({
            type: "POST",
            url: "/ajaxjs.php",
            data: dataString,
            cache: false,
            success: function(html) {
							console.log('Sent');
            $('#form')[0].reset();
            $('.successMessage').text('Mensaje enviado exitosamente');
            }
        });
    }
		e.preventDefault();
    return false;
}


function altura(){
	var allAltura = new Array();
	$('.eachUnidad').each(function(){
		allAltura.push(Math.ceil($(this).height()));
	});
	var mayor = Math.max.apply(null, allAltura);
	mayor += 30;
	$('.eachUnidad').css('min-height',mayor+'px');
}

function videoModal(e){
	var video = $(e).attr('data-video');
	$('.popup-video iframe').attr('src',video);
	$('.popup-video').addClass('shown');
        $('.popup-video, .mask').fadeIn('slow');
        $('.mask').on('click', function() {
            jQuery('#pPlayer')[0].contentWindow.postMessage('{"event":"command","func":"pauseVideo","args":""}', '*')
            $('.popup-video, .mask').fadeOut('slow', function() {
                $('.popup-video').removeClass('shown');
            });
        });
}
function cerrarModal(){
  $('.popup-video, .mask').fadeOut('slow', function() {
                $('.popup-video').removeClass('shown');
            });
}
function more(e){
	idContenedor = $(e).attr('data-ide');
	idhtml = idContenedor+'.html';
	$('#'+idContenedor).load(idhtml);
	$(e).attr('onclick','less(this)');
}
function less(e){
	idContenedor = $(e).attr('data-ide');
	$('#'+idContenedor).empty();
	$(e).attr('onclick','more(this)');
}
