<?php // INDEX ENGLISH ?>
<?php require_once('idioma.php'); ?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="apple-mobile-web-app-capable" content="yes" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="apple-mobile-web-app-status-bar-style" content="black" />
        <meta name="apple-mobile-web-app-status-bar-style" content="black" />
        <meta name="keywords" content="AN, AN Global, ANGLOBAL, The power of AN, Digital Marketing, Analytics, Cloud Services, Consultoría IT, IT, Analítica Avanzada, Cloud, Análisis de data, herramientas de medicion digital, Desarrollo de software, administracion de aplicaciones e infraestructura, desarrollo de aplicaciones, diseño de plataformas digitales">
        <link rel="shortcut icon" href="http://anglobal.com/static/images/favicon.ico?v=1">

        <link rel="stylesheet" href="http://anglobal.com/startup/flat-ui/bootstrap/css/bootstrap.css">
        <link href="https://fonts.googleapis.com/css?family=Roboto+Slab:300,400,700|Open+Sans:300,800i" rel="stylesheet">

        <link rel="stylesheet" href="http://anglobal.com/startup/flat-ui/css/flat-ui.css">
        <!-- Using only with Flat-UI (free)-->
        <link rel="stylesheet" href="http://anglobal.com/startup/common-files/css/icon-font.css">
        <!-- end -->
        <link rel="stylesheet" href="http://anglobal.com/startup/common-files/css/animations.css">
        <link rel="stylesheet" href="http://anglobal.com/static/css/owl.carousel.css">
        <link rel="stylesheet" href="http://anglobal.com/static/css/style.css?v=1.1.6">
        <link rel="stylesheet" href="http://anglobal.com/static/css/flag-icon.css">

        <title></title>
        <script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-99908869-1', 'auto');
  ga('send', 'pageview');

</script>
    </head>

    <body>
        <div class="page-wrapper pterm">
  <header class="header-23">

    <div class="container">
        <div class="row">
            <div class="navbar col-sm-12 terminosNav" role="navigation">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle"></button>
                    <a href="/pt"><img src="http://www.anglobal.com/static/images/anglobal.png" alt="anglobal"></a>
                </div>
                <div class="navbar-collapse collapse">
                    <ul class="nav pull-right">
                        <li><a href="/pt#about" class="goto"><?php echo str_replace('"','',$textos[0]); ?></a></li>
                        <li><a href="/pt#we_are" class="goto"><?php echo str_replace('"','',$textos[1]); ?></a></li>
                        <li><a href="/pt#business" class="goto"><?php echo str_replace('"','',$textos[2]); ?></a></li>
                        <li><a href="/pt#services" class="goto"><?php echo str_replace('"','',$textos[3]); ?></a></li>
                        <li><a href="/pt#clients" class="goto"><?php echo str_replace('"','',$textos[4]); ?></a></li>
                    </ul>

                </div>
            </div>
        </div>
    </div>
</header>
<div class="container termC">
  <h5>AVISO DE PRIVACIDADE</h5>
  <p>Este Anexo constitui o aviso de privacidade para efeitos do disposto na Ley Federal de <em>Protecci&oacute;n de Datos Personales en Posesi&oacute;n de Particulares</em> (a &ldquo;<u>LFPD</u>&rdquo;), (Lei Federal de Prote&ccedil;&atilde;o da Dados Pessoais na Posse de Particulares) e as disposi&ccedil;&otilde;es que emanam ou se relacionam com ela. O presente Aviso de Privacidade tem os seguintes termos e condi&ccedil;&otilde;es:</p>

  <ol>
  <li><strong>Termo definido.</strong> Para os efeitos deste Aviso de Privacidade, os seguintes termos ter&atilde;o os seguintes significados, no entendimento que, salvo que o presente Anexo se defina de outra forma, os termos que sejam utilizados neste Aviso de Privacidade e que n&atilde;o estejam definidos no mesmo, ter&atilde;o o significado atribu&iacute;do na <em>LFPD</em>:</li>
  </ol>

  <p><span>dados pessoais</span>, tem o significado atribu&iacute;do na <em>LFPD.</em></p>

  <p><span>Direitos ARCO</span>, consiste nos direitos de acesso, retifica&ccedil;&atilde;o, cancelamento ou oposi&ccedil;&atilde;o conforme com o estabelecido na LFPD, sujeito &agrave;s exce&ccedil;&otilde;es estabelecidas na mesma e neste Aviso de Privacidade, cada Titular tem, em rela&ccedil;&atilde;o aos dados pessoais obtidos pelo Respons&aacute;vel e/ou seus encarregados e que est&atilde;o discriminados a seguir:</p>

  <p>DIREITO DE ACESSO. Isto &eacute;, o direito de ter conhecimento sobre os dados pessoais de cada titular que estejam sob a posse do Respons&aacute;vel do caso ou de seus encarregados e sobre quem mais tenha recebido a informa&ccedil;&atilde;o, al&eacute;m da finalidade do compartilhamento.</p>

  <p>DIREITO DE RETIFICA&Ccedil;&Atilde;O. Cada Titular tem direito &agrave; retifica&ccedil;&atilde;o dos seus dados pessoais sempre e quando estes estejam inexatos ou incompletos.</p>

  <p>DIREITO DE CANCELAMENTO. Cada Titular tem direito de solicitar, em qualquer momento, que se suprimam os seus dados pessoais, ato o qual acontecer&aacute; uma vez que transcorra o per&iacute;odo de bloqueio. O bloqueio implica na identifica&ccedil;&atilde;o e conserva&ccedil;&atilde;o dos dados pessoais, uma vez que se cumpra a finalidade para a qual foram obtidos, e tem como prop&oacute;sito determinar as poss&iacute;veis responsabilidades em rela&ccedil;&atilde;o ao seu tratamento, at&eacute; o prazo de prescri&ccedil;&atilde;o legal ou contratual da mesma.</p>

  <p>Durante este per&iacute;odo, os seus dados pessoais n&atilde;o ser&atilde;o objeto de tratamento e transcorrido o tempo estipulado, procede-se com o cancelamento na base de dados correspondente. Uma vez cancelado o dado correspondente, o Respons&aacute;vel em quest&atilde;o avisar&aacute; ao respectivo Titular. No caso dos dados pessoais terem sido transmitidos anteriormente &agrave; data de retifica&ccedil;&atilde;o ou cancelamento e continuem sendo tratados por terceiros, o Respons&aacute;vel lhe informar&aacute; sobre tal solicita&ccedil;&atilde;o de retifica&ccedil;&atilde;o ou cancelamento, para que seja efetuado o processo de cancelamento por sua parte tamb&eacute;m.&nbsp;</p>

  <p>DIREITO DE OPOSI&Ccedil;&Atilde;O. O Titular tem o direito de solicitar a qualquer momento, sempre e quando possua uma causa leg&iacute;tima, que o Respons&aacute;vel deixe de utilizar seus dados pessoais.</p>

  <p><span>Respons&aacute;vel</span>, significa a pessoa ou parte que receba os dados pessoais do Titular.</p>

  <p><span>Titular</span>, significa a pessoa ou parte titular dos dados pessoais ou autorizada a entregar dados pessoais de terceiros de acordo com as leis aplic&aacute;veis, que seja respons&aacute;vel pela entrega dos dados ao Respons&aacute;vel.</p>

  <ol start="2">
  <li><strong>Consentimento do Titular.</strong> Para efeitos do disposto na LFPD e em particular no artigo 17 (dezessete), cada Titular manifesta que (i) este Aviso de Privacidade foi apresentado pelo Respons&aacute;vel correspondente previamente &agrave; apresenta&ccedil;&atilde;o deste Anexo, e (ii) a assinatura deste documento por cada Titular constitui o seu consentimento na utiliza&ccedil;&atilde;o dos seus dados pessoais para efeitos da LFPD e outras legisla&ccedil;&otilde;es aplicadas. Em casos em que os dados pessoais fornecidos pelo Titular ao respectivo Respons&aacute;vel de acordo com termos de/ou para o cumprimento deste Anexo constituam dados pessoais sens&iacute;veis, a subscri&ccedil;&atilde;o do Anexo realizada pelo respectivo Titular, considera-se como consentimento por parte do Titular de maneira que estes dados ser&atilde;o utilizados pelo Respons&aacute;vel, considerando sempre o disposto na LFPD e demais legisla&ccedil;&otilde;es aplic&aacute;veis.</li>
  </ol>

  <p>O consentimento do respectivo Titular poder&aacute; ser revogado em qualquer momento pelo mesmo, sem que lhe seja atribu&iacute;do efeitos retroativos, nos termos e conforme os procedimentos estabelecidos na continua&ccedil;&atilde;o deste Aviso de Privacidade.</p>

  <p>Todavia, qualquer disposi&ccedil;&atilde;o deste Aviso de Privacidade, cada Titular reconhece que n&atilde;o se requerer&aacute; de seu consentimento para a utiliza&ccedil;&atilde;o dos seus dados pessoais por parte do respectivo Respons&aacute;vel ou de terceiros, em qualquer dos casos sinalados no artigo 10 (dez) da LFPD.</p>

  <ol start="3">
  <li><strong>Objetivo do Aviso de Privacidade; Finalidade dos dados pessoais.</strong> O presente Aviso de Privacidade tem como objetivo estabelecer os termos e condi&ccedil;&otilde;es pelas quais cada Respons&aacute;vel (ou o encarregado designado pelo Respons&aacute;vel) (i) receber&aacute; e proteger&aacute; os dados pessoais do respectivo Titular, para efeitos de prote&ccedil;&atilde;o &agrave; sua privacidade e seu direito &agrave; autodetermina&ccedil;&atilde;o informativa, al&eacute;m do cumprimento do disposto na LFDP; (ii) utilizar&aacute; os dados pessoais do respectivo Titular, e (ii) realizar&aacute;, quando necess&aacute;rio, as transfer&ecirc;ncias de dados pessoais a terceiros.</li>
  </ol>

  <p>O Respons&aacute;vel correspondente poder&aacute; solicitar e utilizar os dados pessoais do respectivo Titular, isto &eacute;, a informa&ccedil;&atilde;o necess&aacute;ria para a sua identifica&ccedil;&atilde;o, como nome e sobrenome; data de nascimento; endere&ccedil;o, seja particular, comercial ou fiscal; endere&ccedil;o eletr&ocirc;nico (e-mail), pessoal ou comercial; senha de identifica&ccedil;&atilde;o em redes sociais; n&uacute;mero telef&ocirc;nico, celular ou fixo; n&uacute;mero do cart&atilde;o de cr&eacute;dito; seu <em>Registro Federal de Contribuyentes (RFC)</em>; sua <em>Clave &Uacute;nica de Registro de Poblaci&oacute;n (CURP)</em>; seu n&uacute;mero de filia&ccedil;&atilde;o ao <em>Instituto Mexicano del Seguro Social</em>; prefer&ecirc;ncias de consumo e navega&ccedil;&atilde;o em sites de Internet. A solicita&ccedil;&atilde;o dos dados pessoais poder&aacute; ser efetuada quando o Titular correspondente visite os pontos de venda do Respons&aacute;vel ou suas filiais e distribuidores autorizados. Comunica-se por via telef&ocirc;nica com o respectivo Respons&aacute;vel ou com seus encarregados (incluindo centros de aten&ccedil;&atilde;o a clientes), mediante o uso de correios eletr&ocirc;nicos e/o mensagens curtas de texto (SMS), mediante tamb&eacute;m a utiliza&ccedil;&atilde;o de websites a partir do fornecimento volunt&aacute;rio de informa&ccedil;&atilde;o atrav&eacute;s de janelas de bate-papo habilitadas na p&aacute;gina, mediante o uso de ferramentas de captura autom&aacute;tica de dados, ou ainda, mediante a entrega direta ao Respons&aacute;vel. Tais ferramentas permitem que as informa&ccedil;&otilde;es sejam enviadas ao navegador do website, tais como o tipo de navegador que est&aacute; sendo utilizado, o idioma do usu&aacute;rio, os per&iacute;odos de acesso, o endere&ccedil;o de IP dos websites que s&atilde;o usados para o acesso da p&aacute;gina do Respons&aacute;vel ou de suas filiais e encarregados.</p>

  <p>O Respons&aacute;vel referido tamb&eacute;m levanta dados pessoais de fontes de acesso p&uacute;blico e de outras fontes dispon&iacute;veis no mercado, as quais o Titular correspondente possa haver dado o seu consentimento para o compartilhamento da informa&ccedil;&atilde;o pessoal ou que tenha sido proporcionado informa&ccedil;&atilde;o demogr&aacute;fica an&ocirc;nima associada a uma &aacute;rea geogr&aacute;fica determinada.</p>

  <p>Os dados pessoais de cada Titular s&atilde;o recebidos e tratados pelo respectivo Respons&aacute;vel ou seus encarregados com a finalidade de permitir que o Titular, seja de maneira presencial, mediante comunica&ccedil;&atilde;o telef&ocirc;nica, fax ou atrav&eacute;s do preenchimento de formatos impressos eletr&ocirc;nicos dos seus correspondentes websites, ou ainda, mediante correio eletr&ocirc;nico, levar a cabo as seguintes atividades com o respectivo Respons&aacute;vel.</p>

  <ul>
  <li>a)&nbsp;Solicitar, comprar, trocar ou devolver produtos;</li>
  <li>b) Solicitar, contratar, trocar ou cancelar servi&ccedil;os;</li>
  <li>c) Efetuar pagamentos online;</li>
  <li>d) Solicitar recibo ou nota fiscal digital;</li>
  <li>e) Solicitar um or&ccedil;amento, informa&ccedil;&atilde;o ou amostras gr&aacute;tis de produtos e servi&ccedil;os;</li>
  <li>f) &nbsp;Solicitar a entrega, retifica&ccedil;&atilde;o ou cumprimento da garantia de produtos;</li>
  <li>g) &nbsp;Solicitar a presta&ccedil;&atilde;o de servi&ccedil;os ou o cumprimento da garantia do servi&ccedil;o;</li>
  <li>h) &nbsp;Contatar o Servi&ccedil;o de Aten&ccedil;&atilde;o ao Cliente;</li>
  <li>i) &nbsp;Receber publicidade impressa ou atrav&eacute;s de meios eletr&ocirc;nicos, incluindo comunica&ccedil;&otilde;es com fins de propaganda online, telemarketing sobre produtos e servi&ccedil;os;</li>
  <li>j) &nbsp;Criar perfis pessoais;</li>
  <li>k) &nbsp;Participar de pesquisas;</li>
  <li>l) &nbsp;Utilizar os distintos servi&ccedil;os dos seus correspondentes website incluindo o download de conte&uacute;dos e formatos;</li>
  <li>m) &nbsp;Notificar o Respons&aacute;vel sobre problemas com o site;</li>
  <li>n) &nbsp;Participar de chats e/ou foros de discuss&atilde;o virtuais sobre os produtos e servi&ccedil;os;</li>
  <li>o) &nbsp;Participar de testes, concursos, rifas, jogos e sorteios;</li>
  <li>p) &nbsp;Compartilhar seus coment&aacute;rios ou sugest&otilde;es sobre os produtos e servi&ccedil;os;</li>
  <li>q) &nbsp;Solicitar emprego;</li>
  <li>r) &nbsp;Realizar pagamentos, e</li>
  <li>s) &nbsp;Qualquer outra atividade de natureza an&aacute;loga &agrave;s descritas nos incisos previamente citados.</li>
  </ul>

  <p>Desse modo, o Respons&aacute;vel, diretamente ou atrav&eacute;s de encarregados, pode utilizar seus dados pessoais para os seguintes fins:</p>

  <ul>
  <li>a) Realizar estudos sobre os dados demogr&aacute;ficos, interesses e comportamento de seus clientes, consumidores, funcion&aacute;rios, fornecedores e daqueles terceiros com quem esteja tratando;</li>
  <li>b) Realizar estudos de mercado e de consumo para efeitos de oferecer produtos e servi&ccedil;os personalizados, assim como publicidade e conte&uacute;dos mais adequados &agrave;s necessidades de seus clientes e consumidores;</li>
  <li>c) Elaborar estat&iacute;sticas internas que indiquem os servi&ccedil;os e produtos mais apreciados pelos diferentes segmentos de consumidores, clientes e outros usu&aacute;rios de seus respectivos websites.</li>
  <li>d) Formalizar o processo transacional com seus clientes, consumidores e fornecedores;</li>
  <li>e) Solicitar informa&ccedil;&otilde;es de cart&atilde;o de cr&eacute;dito utilizadas para o pagamento de produtos e servi&ccedil;os, para o fornecimento da informa&ccedil;&atilde;o necess&aacute;ria para os &oacute;rg&atilde;os de informa&ccedil;&otilde;es de cr&eacute;ditos, nos termos da Lei para regulamentar as Sociedades de Informa&ccedil;&atilde;o de Cr&eacute;dito. Tal informa&ccedil;&atilde;o servir&aacute; como prova em uma eventual disputa por alguma compra ou pagamento de servi&ccedil;os, assim como para realizar solicita&ccedil;&otilde;es de devolu&ccedil;&atilde;o de artigos e seu devido reembolso ou troca, e para instrumentar medidas de deten&ccedil;&atilde;o e preven&ccedil;&atilde;o de fraude e roubo;</li>
  <li>f) Realizar revis&otilde;es de dados pessoais para verificar a informa&ccedil;&atilde;o proporcionada pelos funcion&aacute;rios e/ou candidatos a serem empregados pelo Respons&aacute;vel ou encarregados, filiados, ou seja, atrav&eacute;s de terceiros ou por interm&eacute;dio do Departamento de Recursos Humanos;</li>
  <li>g) Empregar c&acirc;meras de vigil&acirc;ncia nas lojas com fins de seguran&ccedil;a e prote&ccedil;&atilde;o de perdas e danos. As grava&ccedil;&otilde;es s&atilde;o armazenadas em uma &aacute;rea segura de acesso restrito aos gerentes diretivos e, em caso de necessidade, &agrave;s autoridades competentes. As grava&ccedil;&otilde;es s&atilde;o cotidianamente destru&iacute;das, salvo casos que sejam necess&aacute;rias para a investiga&ccedil;&atilde;o de um acidente ou para algum procedimento judicial;</li>
  <li>h) Manter um registro de compras, assim como das opera&ccedil;&otilde;es e informa&ccedil;&atilde;o revisada nas diferentes se&ccedil;&otilde;es dos correspondentes websites e que ser&atilde;o solicitados atrav&eacute;s de ferramentas de captura autom&aacute;tica de dados;</li>
  <li>e) Enviar ao respectivo Titular a notifica&ccedil;&atilde;o de ofertas, avisos e/ou mensagens promocionais atrav&eacute;s de suas p&aacute;ginas Web, que lhe ser&atilde;o enviadas, a menos que o Titular manifeste atrav&eacute;s dos formatos impressos ou virtuais, ou ainda, mediante fax, sua vontade de n&atilde;o receber tais ofertas. Ocasionalmente, tais ofertas podem conter informa&ccedil;&atilde;o de fornecedores do Respons&aacute;vel.</li>
  </ul>

  <ol start="4">
  <li><strong>Transfer&ecirc;ncias de Dados.</strong> Cada Titular manifesta consentimento expresso para que o Respons&aacute;vel, ou qualquer encarregado, realize transfer&ecirc;ncias de dados pessoais a terceiros nacionais ou estrangeiros, entendendo-se que o trato dado aos dados de cada Titular dever&aacute; estar ajustado ao que est&aacute; estabelecido neste Aviso de Privacidade.</li>
  </ol>

  <p>Para efeitos do estabelecido nesta Se&ccedil;&atilde;o 4, mas sujeito ao estabelecido no &uacute;ltimo par&aacute;grafo da mesma, cada Respons&aacute;vel informa a cada Titular que com o intuito de poder entregar produtos, servi&ccedil;os e solu&ccedil;&otilde;es aos seus clientes, consumidores, funcion&aacute;rios e demais usu&aacute;rios dos servi&ccedil;os, o Respons&aacute;vel, encarregados e/ou filiados celebraram ou celebrar&atilde;o diversos acordos comerciais com fornecedores de produtos e servi&ccedil;os, tanto em territ&oacute;rio nacional como no exterior, para que sejam fornecidos, entre outros servi&ccedil;os, os de telefonia fixa, de longa dist&acirc;ncia, internet, televis&atilde;o, telefonia celular e diferentes servi&ccedil;os de telecomunica&ccedil;&otilde;es. A autoriza&ccedil;&atilde;o de cada Titular outorgada conforme esta Se&ccedil;&atilde;o 4 autoriza o determinado Respons&aacute;vel e/ou encarregados a transmitir dados pessoais do Titular a determinados fornecedores, no entendimento de que tais fornecedores est&atilde;o obrigados, em virtude do contrato correspondente, a manter a confidencialidade dos dados pessoais fornecidos pelo Respons&aacute;vel e/ou encarregados, al&eacute;m de considerar este Aviso de Privacidade. O Respons&aacute;vel e/ou encarregados poder&atilde;o transferir os dados pessoais fornecidos pelo Titular ao que diz respeito &agrave; sociedade matriz ou controladora, assim como a suas subsidi&aacute;rias ou afiliadas baixo subordinadas ao Respons&aacute;vel, bem como a qualquer outra sociedade da qual o Respons&aacute;vel seja parte e que operem com os mesmos processos e pol&iacute;ticas internas, no territ&oacute;rio nacional ou no exterior. Tamb&eacute;m estar&aacute; autorizada a transfer&ecirc;ncia dos seus dados pessoais a terceiros que apoiem no cumprimento dos contratos ou rela&ccedil;&otilde;es jur&iacute;dicas que tenham com o Titular que esteja em quest&atilde;o.</p>
  <p>Contudo, o disposto na Se&ccedil;&atilde;o 4 ou em qualquer outro lugar deste Aviso de Privacidade, cada Titular reconhece e aceita que o Respons&aacute;vel n&atilde;o requer de autoriza&ccedil;&atilde;o nem confirma&ccedil;&atilde;o de tal Titular para realizar transfer&ecirc;ncias de dados pessoais nacionais ou internacionais nos casos previstos no artigo 37 (trinta e sete) na LFPD ou em qualquer outro caso de exce&ccedil;&atilde;o prevista na LFPD ou demais legisla&ccedil;&atilde;o aplic&aacute;vel.</p>

  <ol start="5">
  <li><strong>Reten&ccedil;&atilde;o e seguran&ccedil;a de dados pessoais.</strong> Cada Respons&aacute;vel e/ou seus encarregados conservar&atilde;o os dados pessoais do Titular que esteja em quest&atilde;o durante o tempo que seja necess&aacute;rio para proceder com a solicita&ccedil;&atilde;o de informa&ccedil;&atilde;o, produtos e/ou servi&ccedil;os, bem como para manter os registros cont&aacute;beis, financeiros e de auditoria em termos da LFPD e da legisla&ccedil;&atilde;o mercantil, fiscal e administrativa vigente. Os dados pessoais do Titular, obtidos pelo Respons&aacute;vel e/ou seus encarregados se encontrar&atilde;o protegidos por medidas de seguran&ccedil;a administrativas, t&eacute;cnicas e f&iacute;sicas adequadas, contra o dano, perda, altera&ccedil;&atilde;o, destrui&ccedil;&atilde;o ou uso, acesso ou manipula&ccedil;&atilde;o n&atilde;o autorizados, de conformidade com o disposto na LFPD e da regulamenta&ccedil;&atilde;o administrativa derivada da mesma.</li>
  </ol>

  <ol start="6">
  <li><strong>Departamento de dados pessoais/centro de privacidade; domic&iacute;lio.</strong> Cada Respons&aacute;vel colocar&aacute; a disposi&ccedil;&atilde;o de cada Titular, o departamento de dados pessoais/centro de privacidade ou similar, o qual ter&aacute; &agrave; sua disposi&ccedil;&atilde;o a recep&ccedil;&atilde;o, registro e aten&ccedil;&atilde;o &agrave;s solicita&ccedil;&otilde;es para exercer o direito de acesso, retifica&ccedil;&atilde;o, cancelamentos e oposi&ccedil;&atilde;o a dados pessoais, tal como para limitar o uso ou divulga&ccedil;&atilde;o dos dados, al&eacute;m dos demais direitos previstos na LFPD.</li>
  </ol>
</div>
<footer class="footer-7 ">
    <div class="container">
        <a href="#" class="goto casita"><span class="fui-home"> </span></a>
        <nav>
            <ul class="col-xs-12 col-sm-12 col-md-12 col-lg-12 espUl">
                <li><a href="mailto:weare@anglobal.com">weare@anglobal.com</a></li>
            </ul>
            <ul class="col-xs-12 col-sm-12 col-md-12 col-lg-12 espUl">
                <li><a href="tel:5552581400">+52 (55) 5258 1400</a></li>
            </ul>
            <ul class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
              <li class="flag-wrapper">
                  <a href="/es" class="img-thumbnail flag flag-icon-background flag-icon-mx"></a>
              </li>
              <li class="flag-wrapper">
                  <a href="/en" class="img-thumbnail flag flag-icon-background flag-icon-us"></a>
              </li>
              <li class="flag-wrapper">
                  <a href="/pt" class="img-thumbnail flag flag-icon-background flag-icon-br"></a>
              </li>
            </ul>
            <div class="clearfix"></div>
        </nav>
        <div class="social-btns" target="_blank">

            <a class="iab">
                <img src="http://www.anglobal.com/static/images/logo_white.png" alt="">
            </a>
        </div>
    </div>
</footer>            <!-- Placed at the end of the document so the pages load faster -->
            <script src="http://anglobal.com/startup/common-files/js/jquery-1.10.2.min.js"></script>

            <script src="http://anglobal.com/startup/common-files/js/jquery.scrollTo-1.4.3.1-min.js"></script>
            <script src="http://anglobal.com/startup/common-files/js/jquery.sharrre.min.js"></script>
            <script src="http://anglobal.com/startup/common-files/js/bootstrap.min.js"></script>
            <script src="http://anglobal.com/static/js/owl.carousel.js"></script>
            <script src="http://anglobal.com/startup/common-files/js/masonry.pkgd.min.js"></script>
            <script src="http://anglobal.com/startup/common-files/js/modernizr.custom.js"></script>
            <script src="http://anglobal.com/startup/common-files/js/page-transitions.js"></script>
            <script src="http://anglobal.com/startup/common-files/js/easing.min.js"></script>
            <script src="http://anglobal.com/startup/common-files/js/jquery.svg.js"></script>
            <script src="http://anglobal.com/startup/common-files/js/jquery.svganim.js"></script>
            <script src="http://anglobal.com/startup/common-files/js/jquery.backgroundvideo.min.js"></script>
            <script src="http://anglobal.com/startup/common-files/js/froogaloop.min.js"></script>
            <script src="http://anglobal.com/startup/common-files/js/startup-kit.js?v=1.2.3"></script>
             <script src="http://anglobal.com/static/js/jquery.malihu.PageScroll2id.min.js"></script>
            <script src="http://anglobal.com/static/js/gneral.js?v=1.2.13"></script>
        </div>
     <div id="myModal" class="modal fade" role="dialog">
      <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <button type="button" class="close" data-dismiss="modal">&times;</button>

          <div class="modal-body">
            <iframe id="pPlayerB" src="http://player.vimeo.com/video/80282064?title=0&amp;byline=0&amp;portrait=0&amp;api=1&amp;player_id=pPlayerB" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
          </div>
        </div>

      </div>
    </div>
    </body>
</html>
