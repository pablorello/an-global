<nav class="navbar navbar-expand-lg navbar-light" id="shadowOver">
    <a class="navbar-brand" href="<?php echo get_home_url(); ?>"> <img src="<?php bloginfo('template_url');?>/images/logo_an.svg" alt=""></a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTextA" aria-controls="navbarTextA" aria-expanded="false" aria-label="Toggle navigation">
      <span class="a"></span>
      <span class="b"></span>
      <span class="c"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarTextA">

      <div id="navbarText">
        <ul class="navbar-nav mr-auto bu" data-url="<?php bloginfo('template_url');?>/submenu.php">
          <?php
          $argsm = array(
                  'post_type' => 'business_units'
                  );
                  $popularesm = query_posts($argsm);
                  $conteoProyectosm= count($popularesm);
                  for ($m=0; $m < $conteoProyectosm; $m++) { ?>
                    <li class="nav-item" onclick="showMenu(this)">
                      <a class="thisA nav-link <?php echo ($ideActaul == $popularesm[$m]->ID)?'activeMnu':''; ?>" href="<?php echo get_the_permalink($popularesm[$m]->ID) ?>" data-url="<?php echo get_the_permalink($popularesm[$m]->ID) ?>" data-ide="<?php echo $popularesm[$m]->ID; ?>"><?php echo str_replace('AN_','',get_the_title($popularesm[$m]->ID)); ?></a>
                      <div class="submenu<?php echo strtolower(str_replace('AN_',' ',get_the_title($popularesm[$m]->ID))); ?>">
                          <div class="container">
                            <div class="row">
                              <div class="col-12 col-sm-12 col-md-7 col-lg-7 col-xl-7">
                                <div class="claimMenu">
                                  <?php
                                    $cliamE = get_field('logotipo_claim',$popularesm[$m]->ID);
                                  ?>
                                  <div class="eachClaim">
                                    <img src="<?php echo $cliamE['url'] ?>" alt="<?php echo $cliamE['alt'] ?>">
                                  </div>
                                  <div class="descMenu  d-none d-md-block">
                                    <div class="containerDescM">
                                      <?php echo get_field('descripcion_menu',$popularesm[$m]->ID); ?>
                                    </div>
                                  </div>
                                  <div class="conoceMenu">
                                    <p><?php _e("[:es]CONOCE MÁS SOBRE[:en]GO TO"); ?> <a class="regular" href="<?php echo get_the_permalink($popularesm[$m]->ID) ?>"><?php echo get_the_title($popularesm[$m]->ID); ?></a><i class="icon dripicons-arrow-thin-right"></i></p>
                                  </div>
                                </div>
                              </div>
                              <div class="col-12 col-sm-12 col-md-1 col-lg-1 col-xl-1"></div>
                              <div class="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
                                <ul class="row submenuHere">
                                <?php
                                  $submenu = get_field('soluciones_unidades',$popularesm[$m]->ID);
                                  $conteo = count($submenu);
                                  for ($i=0; $i < 5; $i++) {
                                    ?>
                                        <li class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                                          <a href="<?php echo get_the_permalink($submenu[$i]->ID); ?>" ><?php echo get_the_title($submenu[$i]->ID); ?></a>
                                        </li>
                                    <?php
                                  }
                                ?>
                                <li class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12"><a class="seeMore" href="<?php echo get_the_permalink($popularesm[$m]->ID) ?>"><?php _e("[:es]VER MÁS SOLUCIONES...[:en]MORE SOLUTIONS..."); ?></a><i class="icon dripicons-arrow-thin-right"></i></li>
                                </ul>
                              </div>
                            </div>
                          </div>
                        </div>
                    </li>

                <?php
                  }
          ?>

        </ul>
      </div>
      <div class="secondMenu">
        <ul class="navbar-nav mr-auto bu">
          <li class="nav-item" onclick="showMenu(this)">
            <a class="thisA <?php echo ($ideActaul == 136)?'activeMnu':''; ?>" data-url="<?php echo get_the_permalink($popularesm[$m]->ID) ?>" href="<?php echo get_the_permalink(136); ?>"><?php echo get_the_title(136); ?></a>


            <div class="submenu second">
                                      <div class="container">
                                        <div class="row">
                                          <div class="col-12 col-sm-12 col-md-7 col-lg-7 col-xl-7">
                                            <div class="claimMenu">
                                              <div class="descMenu  d-none d-md-block">
                                                <div class="containerDescM">
                                                  <p>  <?php echo get_field('description_menu',136); ?></p>
                                                </div>
                                              </div>
                                              <div class="conoceMenu">
                                                <p>GO TO <a class="regular" href="<?php echo get_the_permalink(136); ?>"><?php echo get_the_title(136); ?></a><i class="icon dripicons-arrow-thin-right"></i></p>
                                              </div>
                                            </div>
                                          </div>
                                          <div class="col-12 col-sm-12 col-md-1 col-lg-1 col-xl-1"></div>
                                          <div class="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
                                            <ul class="row submenuHere">
                                              <li class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                                                  <a class="<?php echo ($ideActaul == 1465)?'activeMnu':''; ?>" href="<?php echo get_the_permalink(1465); ?>"><?php echo get_the_title(1465); ?></a>
                                              </li>
                                              <li class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                                                  <a class="<?php echo ($ideActaul == 1419)?'activeMnu':''; ?>" href="<?php echo get_the_permalink(1419); ?>"><?php echo get_the_title(1419); ?></a>
                                                </li>
                                                <li class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                                                  <a class="<?php echo ($ideActaul == 1419)?'activeMnu':''; ?>" target="_blank" href="<?php bloginfo('url') ?>/wp-content/uploads/2018/12/AN_CO_Ethics_Code.pdf">AN Ethics Code (PDF)</a>
                                                </li>
                                            </ul>
                                          </div>
                                        </div>
                                      </div>
                                    </div>



<?php /*
            <div class="submenu">
                <div class="container">
                  <div class="row">
                    <div class="col-12 col-sm-12 col-md-7 col-lg-7 col-xl-7">
                      <div class="claimMenu">
                        <div class="descMenu  d-none d-md-block">
                          <div class="containerDescM">
                            <?php echo get_field('descripcion_menu',$popularesm[$m]->ID); ?>
                          </div>
                        </div>
                        <div class="conoceMenu">
                          <p><?php _e("[:es]CONOCE MÁS SOBRE[:en]GO TO"); ?> <a class="regular" href="<?php echo get_the_permalink(136) ?>"><?php echo get_the_title(136); ?></a><i class="icon dripicons-arrow-thin-right"></i></p>
                        </div>
                      </div>
                    </div>
                    <div class="col-12 col-sm-12 col-md-1 col-lg-1 col-xl-1"></div>
                    <div class="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
                      <ul class="row submenuHere">
                              <li class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                                <a class="<?php echo ($ideActaul == 1465)?'activeMnu':''; ?>" href="<?php echo get_the_permalink(1465); ?>"><?php echo get_the_title(1465); ?></a>
                                <a class="<?php echo ($ideActaul == 1419)?'activeMnu':''; ?>" href="<?php echo get_the_permalink(1419); ?>"><?php echo get_the_title(1465); ?></a>
                              </li>
                      </ul>
                    </div>
                  </div>
                </div>
              </div>
*/
?>
          </li>
          <li class="nav-item">
            <a class="<?php echo ($ideActaul == 160)?'activeMnu':''; ?>" href="<?php echo get_the_permalink(160); ?>"><?php echo get_the_title(160); ?></a>
          </li>
          <li class="nav-item">
            <a class="<?php echo ($ideActaul == 187)?'activeMnu':''; ?>" href="<?php echo get_the_permalink(187); ?>"><?php echo get_the_title(187); ?></a>
          </li>
          <li class="nav-item idioma">
            <a href="<?php echo qts_get_url($another[$existe]);?>"><?php echo $txtIdioma[$existe]; ?><span class="icon"></span></a>
          </li>
        </ul>
      </div>
    </div>

</nav>
