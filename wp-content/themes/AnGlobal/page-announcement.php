<?php
/* Template Name: Announcement */
?>
<?php 
$este = get_the_ID();
$imgBG = get_field('image_bg_announcement', $este);
$imgCntr = get_field('image_announcement', $este);
$place = get_field('place_announcement', $este);
?>
<?php get_header(); ?>
<div class="page-announcement">
<div class="container-fluid">
    <div class="row">
    <div class="imageBG" style="background-image: url(<?php echo $imgBG['url']; ?>)">
        <img src="<?php echo $imgCntr['url']; ?>" />
    </div>
    </div><!-- row -->
</div>
<div class="container">
    <div class="row justify-content-center">
        <div class="col-12 col-sm-12 col-md-10 col-lg-8 col-xl-8">
        <div class="place">
            <p><?php echo $place; ?>, <?php echo get_the_date(); ?></p>
            </div>
            <h1>
    AN Global Announces Acquisition of 4th Source: Bolsters Digital Services for US Customers
    </h1>
    <div class="content">
    <?php 
$post = get_post($este); 
$content = apply_filters('the_content', $post->post_content); 
echo $content;  
?>
    </div>
        </div><!-- col-12-10-8 -->
    </div><!-- row -->
    
    
   
</div>
</div>
<?php get_footer(); ?>