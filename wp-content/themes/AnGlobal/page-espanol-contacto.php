<?php get_header();
$idHome = 200;
    $img_desk= get_field('banner_desktop_contacto');
  ?>
        <div class="bannerNuevaInterna" style="background-image: url('<?php echo $img_desk['url'];?>')">

          <div class="container">

              <div class="row">
                  <div class="col-12 col-sm-12 col-md-8 col-lg-7 col-xl-7">
                    <div class="tsec LadoA">
                        <h1 class="stnd"><span><?php echo get_the_title();?><hr></span></h1>
                        <div class="clearfix"></div>
                    </div>
                    <div class="texto"><?php echo get_field('texto_contacto');?></div>
                  </div>
                  <div class="col-12 col-sm-12 col-md-4 col-lg-5 col-xl-5"></div>

                </div>
          </div>

</div>
        <div class="especialista">
            <div class="container">
                <div class="row">
                    <div class="col-12 col-sm-12 col-md-12 col-lg-5 col-xl-5">
                        <h2><?php echo get_field('titulo_especialista'); ?></h2>
                        <div class="texto">
                            <?php echo get_field('texto_especialista'); ?>
                        </div>
                    </div>
                    <div class="col-sm-0 col-md-1 col-lg-1 col-xl-1"></div>
                    <div class="col-12 col-sm-12 col-md-12 col-lg-6 col-xl-6">
                        <div class="row">
                            <?php
        $unidades = get_field('unidades_contacto');
        for ($i=0; $i < count($unidades); $i++) {
          ?>
                                <div class="col-12 colsm-12 col-md-6 col-lg-6 col-xl-6">
                                    <div id="<?php echo $unidades[$i]->post_name;?>" class="eachUnidad <?php echo ($i == (count($unidades)-1))?'lastE':''; ?>">
                                        <?php
                  $imgUnidad = get_field('logotipo_claim',$unidades[$i]->ID);
                  if(!empty($imgUnidad)){
                ?> <img src="<?php echo $imgUnidad['url']?>" alt="<?php echo $imgUnidad['alt']?>">
                                            <?php } ?>
                                                <hr>
                                                <ul>
                                                    <li>
                                                        <div class="iconos"> <span class="icon dripicons-mail"></span> </div>
                                                        <div class="info">
                                                            <a href="mailto:<?php echo get_field('correo_unidad',$unidades[$i]->ID); ?>" target="_blank"><?php echo get_field('correo_unidad',$unidades[$i]->ID); ?></a>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="iconos"> <span class="icon dripicons-phone"></span> </div>
                                                        <div class="info">
                                                            <?php echo get_field('telefono_unidad',$unidades[$i]->ID); ?>
                                                        </div>
                                                    </li>
                                                </ul>
                                    </div>
                                </div>
                                <?php } ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="presencia">
            <div class="triangleCornerTop ">
                <svg xmlns="http://www.w3.org/2000/svg " version="1.1 " viewBox="0 0 10 10 " preserveAspectRatio="none " class="triangleCornerTop ">
                    <polygon class="fillTriangle " points="0,0 8,0 0,8 "></polygon>
                </svg>
            </div>
            <div class="container">
                <div class="row">
                    <div class="col-12 colsm-12 col-md-2 col-lg-2 col-xl-2"></div>
                    <div class="col-12 colsm-12 col-md-8 col-lg-8 col-xl-8">
                          <h2><?php echo get_field('titulo_tenemos_presencia'); ?></h2>
                        <div class="contentUl">
                        <ul data-url="<?php bloginfo('template_url');?>/oficinas.php" class="selectContact">
                          <div class="arrowListado"><div class="icon dripicons-chevron-down"></div></div>
                            <?php
          $argsc = array(
                  'post_type' => 'oficinas',
                  'order' => 'ASC'
                  );
                  $popularesc = query_posts($argsc);
                  $conteoProyectosc= count($popularesc);?>
                  <li id="actual" onclick="Oficinas(this)" class="">
                      <?php echo get_the_title($popularesc[0]->ID); ?>
                  </li>
                  <?php
                  for ($a=0; $a < $conteoProyectosc ; $a++) { ?>
                                <li id="<?php echo $popularesc[$a]->ID; ?>" onclick="Oficinas(this)" data-title="<?php echo get_the_title($popularesc[$a]->ID); ?>">
                                    <?php echo get_the_title($popularesc[$a]->ID); ?>
                                </li>
                                <?php }
                  //var_dump($popularesc[0]->ID);
          ?>
                        </ul>
                        </div>
                        <div class="informacionOf">
                          <?php
                            $direccion = get_field('direccion_oficinas',$popularesc[0]->ID);
                            $telefono = get_field('telefono_oficina',$popularesc[0]->ID);
                            $correo = get_field('correo_oficinas',$popularesc[0]->ID);
                            $location = get_field('google_maps',$popularesc[0]->ID);
                          ?>
                            <ul>
                              <?php if (!empty($direccion)) {  ?>
                                <li class="eachLine">
                                    <div class="iconos"> <span class="icon dripicons-location"></span> </div>
                                    <div class="info direccion">
                                        <?php echo $direccion; ?>
                                    </div>
                                    <div class="textA">
                                      <a href="<?php echo "http://maps.google.com/?q=".$location['lat'].",".$location['lng']; ?>" class="verGM" target="_blank">VER EN GOOGLE MAPS</a>
                                    </div>
                                    <div class="clearfix"></div>
                                </li>
                              <?php }
                                if (!empty($correo)) {
                               ?>
                                <li class="eachLine">
                                    <div class="iconos"> <span class="icon dripicons-mail"></span> </div>
                                    <div class="info correo">
                                        <?php echo $correo; ?>
                                    </div>
                                </li>
                              <?php }
                                if (!empty($telefono)) {
                               ?>
                                <li>
                                    <div class="iconos"> <span class="icon dripicons-phone"></span> </div>
                                    <div class="info tel">
                                        <?php echo $telefono; ?>
                                    </div>
                                </li>
                              <?php } ?>
                            </ul>
                        </div>
                    </div>
                    <div class="col-12 colsm-12 col-md-2 col-lg-2 col-xl-2"></div>
                </div>
            </div>
        </div>

        <div class="contacta">
            <div class="container">
                <div class="row">
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                        <div class="txtContacta">
                            <h3><?php echo get_field('titulo_work',1165); ?></h3></div>
                    </div>
                    <div class="col-12 col-sm-12 col-md-6 col-lg-4">
                       <div class="txtContacta">
                        <p><?php echo get_field('texto_werk',1165); ?></p> <a href="mailto:<?php echo get_field('mail_work',1165); ?>"><?php echo get_field('mail_work',1165); ?></a>
                    </div>
                    </div>
                    <div class="col-12 col-sm-12 col-md-12 col-lg-2"></div>
                </div>
            </div>
            <div class="triangleCornerBottom dark">
                    <svg xmlns="http://www.w3.org/2000/svg" version="1.1" viewBox="0 0 10 10" preserveAspectRatio="none" class="triangleCornerBottom">
                        <polygon class="fillTriangle" points="1,10 10,1 10,10"></polygon>
                    </svg>
                </div>
        </div>

        <?php get_footer(); ?>
