
<?php get_header();
  $img_desk= get_field('imagen_nosotros_desk');
  ?>

  <div class="bannerNuevaInterna" style="background-image: url('<?php echo $img_desk['url'];?>')">

  <div class="container">

  <div class="row">
      <div class="col-12 col-sm-12 col-md-8 col-lg-7 col-xl-7">
        <div class="tsec LadoA">
            <h1 class="stnd"><span><?php echo get_the_title();?><hr></span></h1>
            <div class="clearfix"></div>
        </div>
        <div class="texto"><?php echo get_field('texto_banner_investor');?></div>
      </div>
      <div class="col-12 col-sm-12 col-md-4 col-lg-5 col-xl-5"></div>

    </div>
</div>

</div>


</div>
  <div class="teamGlobal"  data-url="<?php echo get_home_url(); ?>/team" >


        <?php
        $argsa = array(
                'post_type' => 'jerarquia'
                );
                $popularesa = query_posts($argsa);
                $conteoProyectosa= count($popularesa);
                $idesTeam = array();
                for ($i=0; $i < $conteoProyectosa; $i++) {
                  $argsb = array(
                    'post_type' => 'team_members',
                    'meta_query' => array(
                      array(
                        'key' => 'jerarquia',
                        'value' => 'a:1:{i:0;s:4:"'.$popularesa[$i]->ID.'";}'
                        )
                      )
                  );
                  $popularesb = query_posts($argsb);
                  $conteoPPP = count($popularesb);
                  $numero = 0;

                  if ($popularesa[$i]->ID == 1495) {
                    ?>
                    <section class="firstT">
                      <div class="triangleCornerTop">
                          <svg xmlns="http://www.w3.org/2000/svg" version="1.1" viewBox="0 0 10 10" preserveAspectRatio="none" class="triangleCornerTop">
                              <polygon class="fillTriangle" points="0,0 8,0 0,8"></polygon>
                          </svg>
                      </div>
                    <div class="container">
                    <div class="row">
                    <?php
                    for ($a=0; $a < $conteoPPP; $a++) {
                      $imgBio = get_field('fotografia',$popularesb[$a]->ID);
                      $practica = get_field('selecciona_practica_bio',$popularesb[$a]->ID);
                      ?>
                      <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="eachPersona" onclick="showSTeam(this)" data-solution="<?php echo $popularesb[$a]->ID; ?>" data-practica="<?php echo $practica[0]->ID; ?>">
                          <img src="<?php echo $imgBio['url']?>" alt="<?php echo $imgBio['alt']?>">
                          <div class="skewP">
                            <div class="inverseSkew">
                              <span class="nombreP"><?php echo get_the_title($popularesb[$a]->ID); ?></span>
                              <p class="puesto"><?php echo get_field('puesto',$popularesb[$a]->ID); ?></p>
                            </div>
                          </div>
                          <a> <?php echo get_field('boton_verbio',1414); ?></a>
                        </div>
                      </div>
                      <?php
                    }?>
                  </div>
                </div>
                <div class="triangleCornerBottom">
                    <svg xmlns="http://www.w3.org/2000/svg" version="1.1" viewBox="0 0 10 10" preserveAspectRatio="none" class="triangleCornerBottom">
                        <polygon class="fillTriangle" points="1,10 10,1 10,10"></polygon>
                    </svg>
                </div>
                </section>
                    <?php
                  }else{

                    ?>
                    <div class="container">
                    <div class="row">
                      <?php
                      $primo =$i%2;
                      if ($primo == 1) {

                        $idesTotal = array();
                        $idesTotal[0][0]["ID"]='especial';
                        $idesTotal =  array_merge_recursive($idesTotal,$popularesb);

                      }else{

                        $chunkResult = array_chunk($popularesb, 2);
                        $idesTotal = array();
                        $chunkResult[0][2]["ID"]='especial';

                          for ($d=0; $d < count($chunkResult); $d++) {
                           $idesTotal =  array_merge_recursive($idesTotal,$chunkResult[$d]);
                          }
                        }
                        $nn = 1;
                        for ($c=0; $c < count($idesTotal); $c++) {
                            $imgBio = get_field('fotografia',$idesTotal[$c]->ID);
                            $practica = get_field('selecciona_practica_bio',$idesTotal[$c]->ID);
                            $nuevoN = $nn++;
                          ?>
                          <div class="col-sm-12 col-md-6 col-lg-4 <?php echo ($idesTotal[$c]->ID == null)?'order-0 order-sm-0 order-md-0 order-lg-'.$nuevoN : 'order-'.$nuevoN.' order-sm-'.$nuevoN.' order-md-'.$nuevoN.' order-lg-'.$nuevoN; ?>">
                            <div class="<?php echo ($idesTotal[$c]->ID == null)?'':'eachPersona'; ?>"  onclick="showSTeam(this)" data-solution="<?php echo $idesTotal[$c]->ID ?>" data-practica="<?php echo $practica[0]->ID; ?>">
                              <?php
                              if ($idesTotal[$c]->ID == null) {
                                ?>
                                  <h3><?php echo get_the_title($popularesa[$i]->ID); ?></h3>
                                <?php
                              }else{
                                ?>
                                  <img src="<?php echo $imgBio['url']?>" alt="<?php echo $imgBio['alt']?>">
                                  <div class="skewP <?php echo (get_field('color_de_unidad',$practica[0]->ID) != '')?'withU':''; ?>" style="background:<?php echo get_field('color_de_unidad',$practica[0]->ID) ?>;">
                                    <div class="inverseSkew" >
                                      <span class="nombreP"><?php echo get_the_title($idesTotal[$c]->ID); ?></span>
                                      <p class="puesto"><?php echo get_field('puesto',$idesTotal[$c]->ID); ?></p>
                                    </div>
                                  </div>
                                  <a><?php echo get_field('boton_verbio',1414); ?></a>
                                <?php
                              }
                                  ?>
                            </div>
                          </div>
                          <?php
                        }
                      ?>
                    </div>
                  </div>
                  <?php
                }
              }
         ?>

  </div>
            <div class="contacta">
                <div class="container">
                    <div class="row">
                        <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                            <div class="txtContacta">
                                <h3><?php echo get_field('titulo_contact',1165); ?></h3></div>
                        </div>
                        <div class="col-12 col-sm-12 col-md-6 col-lg-3">
                            <div class="txtContacta">
                                <p><?php echo get_field('texto_contact',1165); ?></p>
                            </div>
                        </div>
                        <div class="col-12"> <a href="<?php echo get_the_permalink(187); ?>"><?php echo get_field('boton_contact',1165); ?></a> </div>
                    </div>
                </div>
                <div class="triangleCornerBottom dark">
                    <svg xmlns="http://www.w3.org/2000/svg" version="1.1" viewBox="0 0 10 10" preserveAspectRatio="none" class="triangleCornerBottom">
                        <polygon class="fillTriangle" points="1,10 10,1 10,10"></polygon>
                    </svg>
                </div>
            </div>

            <?php get_footer(); ?>
