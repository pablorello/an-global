<?php
include(locate_template('laterales.php'));
  $ide = filter_var($_GET['ide'], FILTER_SANITIZE_STRING);
  $practica = filter_var($_GET['unidad'], FILTER_SANITIZE_STRING);
  $color = get_field('color_de_unidad',$practica);
  $fotografia = get_field('fotografia',$ide);
  $arrows = ides('jerarquia',$practica,$ide);
  $practica = (!empty($practica))? get_field('logotipo_unidad_color',$practica) : get_field('logotipo_empresa',1457);
  $descripcion_team = get_field('descripcion_team',$ide);
  $puesto = get_field('puesto',$ide);
  $permalink = get_the_permalink($ide);

    $arr[] = array(
      'titulo' => get_the_title($ide),
      'fotografia' => $fotografia['url'],
      'descripcion' => $descripcion_team,
      'puesto' => $puesto,
      'practica' => $practica['url'],
      'color' => $color,
      'link' => $permalink,
      'prev' => $arrows['prev'],
      'next' => $arrows['next']
    );
header('Content-type: application/json; charset=utf-8');
echo json_encode($arr);
exit();
?>
