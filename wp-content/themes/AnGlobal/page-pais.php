<?php
  $ide = filter_var($_GET['ide'], FILTER_SANITIZE_STRING);
  $mapa = get_field('mapa',$ide);
  $mapaMon = get_field('mapa_mobile',$ide);
  $oficina = get_field('oficina',$ide);
  $location = get_field('google_maps',$oficina[0]->ID);
    $arr[] = array(
      'mapa' => $mapa['url'],
      'mapaMob' => $mapaMon['url'],
      'titulo' => get_the_title($oficina[0]->ID),
      'direccion' => get_field('direccion_oficinas',$oficina[0]->ID),
      'mail' => get_field('correo_oficinas',$oficina[0]->ID),
      'telefono' => get_field('telefono_oficina',$oficina[0]->ID),
      'maps' => "http://maps.google.com/?q=".$location['lat'].",".$location['lng']
    );
header('Content-type: application/json; charset=utf-8');
echo json_encode($arr);
exit();
?>
