<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta http-equiv="Content-Language" content="es"/>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="apple-mobile-web-app-capable" content="yes" />
<meta names="apple-mobile-web-app-status-bar-style" content="black-translucent" />


  <title><?php echo get_the_title(); ?></title>
  <link rel="icon" type="image/png" href="<?php bloginfo('template_url');?>/images/favicon.png" />
  <link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url');?>/css/bootstrap.min.css">
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Roboto+Slab:100,300,400,700" rel="stylesheet">
<link rel="stylesheet" href="<?php bloginfo('template_url');?>/css/webfont.css">
<link rel="stylesheet" href="<?php bloginfo('template_url');?>/css/owl.carousel.min.css">
<link rel="stylesheet" href="<?php bloginfo('template_url');?>/css/owl.theme.default.min.css">
  <link rel="stylesheet" href="<?php bloginfo('template_url');?>/css/jquery.smartmenus.bootstrap-4.css">
  <link rel="stylesheet" href="<?php bloginfo('template_url');?>/css/estilos.css?v=1.2.2">
  <link rel="stylesheet" href="<?php bloginfo('template_url');?>/css/estilosb.css?v=1.1.16">
  <link rel="stylesheet" href="<?php bloginfo('template_url');?>/css/menu.css?v=1.3.11">
  <link rel="stylesheet" href="<?php bloginfo('template_url');?>/css/home.css?v=1.1.16">
  <link rel="stylesheet" href="<?php bloginfo('template_url');?>/css/estilosc.css?v=1.2.4">
  <!--prueba-->
  <?php
  wp_head();
  global $currentLang;
  $currentLang = qtrans_getLanguage();
  $idiomas = array('es','en');
  $another = array('en','es');
  $existe = array_search($currentLang, $idiomas);
$ideActaul = get_the_ID();
  $enabled_languages = get_option('qtranslate_enabled_languages');
$language_names = get_option('qtranslate_language_names');
$txtIdioma = array('ENGLISH','ESPAÑOL');

  ?>
</head>

<body>
  <header class="header topMenu">
    <div class="container no-padding">

<?php get_template_part( 'menu' ); ?>
    </div>

  </header>
  <div class="gralContainer">
