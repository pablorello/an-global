<?php
  $practica = filter_var($_GET['unidad'], FILTER_SANITIZE_STRING);
  $total = filter_var($_GET['total'], FILTER_SANITIZE_STRING);
  $solucionesUnidad = get_field('soluciones_unidades',$practica);
  $nuevoTotal = $total + 10;
  $totalFinal = count($solucionesUnidad) - $total;
  $nuevoTotal = ($nuevoTotal < count($solucionesUnidad))? $nuevoTotal : $total+$totalFinal;
  $nuevoTotalB = ($nuevoTotal < count($solucionesUnidad))? $nuevoTotal : null;
  for ($i= $total; $i < $nuevoTotal; $i++) {
    $iconoSol= get_field('iconos_soluciones',$solucionesUnidad[$i]->ID);
    $arr[] = array(
      'titulo' => get_the_title($solucionesUnidad[$i]->ID),
      'desc' => get_field('descripcion_soluciones',$solucionesUnidad[$i]->ID),
      'icono' => $iconoSol['url'],
      'id' => $solucionesUnidad[$i]->ID,
      'totalF' => $nuevoTotalB,
      'boton'=> get_field('boton_versoluciones',$ideGralTexto)
    );
  }
  header('Content-type: application/json; charset=utf-8');
  echo json_encode($arr);
  exit();
?>
