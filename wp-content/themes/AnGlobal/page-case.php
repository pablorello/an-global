<?php
include(locate_template('laterales.php'));
    $ide = filter_var($_GET['ide'], FILTER_SANITIZE_STRING);
    $practica = filter_var($_GET['unidad'], FILTER_SANITIZE_STRING);
    $nombre = get_the_title($ide);
    $video_webm = get_field('video_webm',$ide);
    $video_ogg = get_field('video_ogg',$ide);
    $video_mp4 = get_field('video_mp4',$ide);
    $image = get_field('imagen_caso',$ide);
    $proyecto = get_field('proyecto_caso',$ide);
    $objetivo = get_field('objetivo_caso',$ide);
    $soluciones = get_field('soluciones_caso',$ide);
    $cliente = get_field('cliente_caso',$ide);
    $imgCliente = get_field('logotipo_clientes',$cliente[0]->ID);
    $arrows = ides('casos_exito_unidades',$practica,$ide);
    for ($i=0; $i < count($soluciones); $i++) {
      $icono = get_field('iconos_soluciones',$soluciones[$i]->ID);
      $sol[]= array(
        'tituloS' => get_the_title($soluciones[$i]->ID),
        'iconoS' => $icono['url'],
        'link' => get_the_permalink($soluciones[$i]->ID)
      );
    }
    $arr[] = array(
      'titulo' => $nombre,
      'video_webm' => $video_webm,
      'video_ogg' => $video_ogg,
      'video_mp4' => $video_mp4,
      'image' => $image['url'],
      'titProy' => get_field('titulo_proyecto',$ideGralTexto),
      'proyecto' => $proyecto,
      'titObj' => get_field('titulo_objetivo',$ideGralTexto),
      'objetivo' => $objetivo,
      'cliente' => $imgCliente['url'],
      'titSol' => get_field('titulo_soluciones',$ideGralTexto),
      'soluciones'=> $sol,
      'boton' => get_field('boton_versoluciones',$ideGralTexto),
      'prev' => $arrows['prev'],
      'next' => $arrows['next']
    );
header('Content-type: application/json; charset=utf-8');
echo json_encode($arr);
exit();
?>
