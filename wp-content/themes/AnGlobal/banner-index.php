<?php
$idHome = 200;
$fondoMp4 = get_field('fondo_mp4',$idHome);
$fondoOgg = get_field('fondo_ogg',$idHome);
$fondoWebm = get_field('fondo_webm',$idHome);
$img_desk= get_field('banner_desktop_home',$idHome);
$bannerD = get_field('imagen_desktop_hm',$idHome);
$bannerM = get_field('imagen_mobile_hm',$idHome);
$InitialWord = get_field('initial_word',$idHome);
$bannerTitle = get_field('titulo_banner_home',$idHome);
$bannerSubtitle = get_field('subtitulo_banner_home',$idHome);
 ?>
 <style>
 </style>
<div id="bannerHome" style="background-image:url('<?php echo $img_desk['url']?>');">
  <video autoplay="autoplay" loop id="video_home" preload="metadata" volume="0"/>
    <source src="<?php echo $fondoMp4['url']; ?>" type="video/mp4" />
    <source src="<?php echo $fondoOgg['url']; ?>" type="video/ogg" />
    <source src="<?php echo $fondoWebm['url']; ?>" type="video/webm" />
  </video/>
  <div id="innerBanner">
      <div class="container">
        <div class="row">
          <div class="col-3 col-lg-4 col-xl-4">
            <h1 class="white">
              <?php echo $InitialWord; ?>
            </h1>
          </div>
          <div class="bannerBloque col-9 col-lg-8 col-xl-8">
            <div class="innerBloque">
                <div class="wrap_1">
                <h2>
                  <span class="space space2"></span>
                  <span class="space space1"></span>
                  <?php echo $bannerTitle; ?></h2>

                </div>
                  <div class="wrap_2">
                <h7>
                    <span class="space space5"></span>
                    <span class="space space4"></span>
                  <span class="space space3"></span>
                  <span class="space space2"></span>
                  <span class="space space1"></span>
            <?php echo $bannerSubtitle; ?>
          </h7>
        </div>
            </div>
          <div>
        </div>


          </div>

          </div>
      </div>
  </div>
</div>
