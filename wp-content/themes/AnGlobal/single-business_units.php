<?php get_header();
$img_desk= get_field('imagen_fondo_unidades');
$img_mob= get_field('banner_mobile_sol');
$ideUni = get_the_ID();
?>
    <div class="bannerInterna internaB practicas" style="background-image: url('<?php echo $img_desk['url']; ?>');">

        <!-- banner de practica-->
        <div class="entry">
            <div class="entryItem">
                <div class="container">
                    <div class="row">
                        <div class="col-12 col-sm-12 col-md-12 col-lg-8 col-xl-8">
                          <img class="show-only-mobile" src="<?php echo $img_desk['url']; ?>" />
                            <div class="textoEntry" style="background-image: linear-gradient(67deg, <?php echo get_field('color_a'); ?>, <?php echo get_field('color_b'); ?>);">
                                <div class="parrafo">
                                    <?php $imgLogo = get_field('logotipo_experience'); ?> <img src="<?php echo $imgLogo['url'] ?>" alt="<?php echo $imgLogo['alt'] ?>" class="svgImage">
                                        <?php echo get_field('descripcion_unidades'); ?>
                                </div>
                            </div>

                        </div>
                        <div class="col-12 col-sm-12 col-md-3 col-lg-4 col-xl-4"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- banner logo de practica-->
    <div class="jumbotron jumbotron-fluid unity">
        <?php
    $imgClaim= get_field('claim_unidades');
  ?>
            <div class="container text-center"> <img src="<?php echo $imgClaim['url'] ?>" alt="<?php echo $imgClaim['alt'] ?>" class="svgImage"> </div>
    </div>
    <?php
    $casoExito = get_field('casos_exito_unidades');
$imgCaso = get_field('imagen_caso',$casoExito[0]->ID);
if (!empty($casoExito)) {?>
  <div class="casosEx interna" data-url="<?php echo get_home_url(); ?>/case" data-unidad="<?php echo $ideUni; ?>">
    <div class="triangleCornerTop">
        <svg xmlns="http://www.w3.org/2000/svg" version="1.1" viewBox="0 0 10 10" preserveAspectRatio="none" class="triangleCornerTop">
            <polygon class="fillTriangle" points="0,0 8,0 0,8"></polygon>
        </svg>
    </div>
    <div class="container">
      <div class="row">
        <div class="col-12 col-sm-12 col-md-7 col-lg-7"></div>
        <div class="col-12 col-sm-12 col-md-5 col-lg-5">
            <h2><span><?php echo get_field('titulo_casos_exito',$ideGralTexto); ?></span></h2>
        </div>
      </div>
      <div class="slide">
        <div class="<?php echo (count($casoExito) > 1)?'owl-carousel owl-theme casos ':''; ?> Allcasos">
          <?php
            for ($l=0; $l < count($casoExito); $l++) {
              $imgCe = get_field('imagen_caso',$casoExito[$l]->ID);
              $cliente = get_field('cliente_caso',$casoExito[$l]->ID);
              $imgCliente = get_field('logotipo_clientes',$cliente[0]->ID);
            ?>
            <div class="item itemA" onclick="showCase(this)" data-solution="<?php echo $casoExito[$l]->ID; ?>">
              <div class="row">
                  <div class="col-12 col-sm-12 col-md-6 col-lg-6">

                        <div class="imageCE">
                          <img src="<?php echo $imgCe['url']; ?>" alt="<?php echo $imgCe['alt']; ?>">
                        </div>
                  </div>
                  <div class="col-12 col-sm-12 col-md-6 col-lg-6 eachIp">
                      <div class="eachInfOfP">
                          <span class="nameOfP">
                            <?php echo get_the_title($casoExito[$l]->ID); ?>
                          </span>
                          <div class="descofP">
                            <?php echo get_field('descripcion_corta_casos',$casoExito[$l]->ID); ?>
                          </div>
                          <div class="btnOfP">
                            <a><?php echo get_field('boton_vercaso',$ideGralTexto); ?></a>
                            <div class="clienteOfP">
                              <img src="<?php echo $imgCliente['url']; ?>" alt="<?php echo $imgCliente['alt']; ?>">
                            </div>
                            <div class="clearfix"></div>
                          </div>
                      </div>
                  </div>
              </div>
            </div>
          <?php } ?>
        </div>
      </div>
    </div>
    <div class="triangleCornerBottom">
        <svg xmlns="http://www.w3.org/2000/svg" version="1.1" viewBox="0 0 10 10" preserveAspectRatio="none" class="triangleCornerBottom">
            <polygon class="fillTriangle" points="1,10 10,1 10,10"></polygon>
        </svg>
    </div>
  </div>
    <?php }
    $solucionesUnidad = get_field('soluciones_unidades');
    if (!empty($solucionesUnidad)) {
      $conteoSol = (count($solucionesUnidad) > 10)? 10 : count($solucionesUnidad);
     ?>

    <!-- soluciones -->
    <div class="container solutions solutionsDatos solucC" data-url="<?php echo get_home_url(); ?>/soluciones" data-practica="<?php echo get_the_ID(); ?>" data-total="10">
        <div class="row inicial">
            <div class="col-12 col-sm-12 col-md-6 col-lg-4">
                <div class="cardTitle tittleSeccion LadoA">
                    <h2 class="stnd"><span><?php _e("[:es]SOLUCIONES[:en]SOLUTIONS"); ?><hr></span></h2>
                    <div class="clearfix"></div>
                </div>
            </div>
            <?php

          for ($i=0; $i < $conteoSol ; $i++) {
        ?>
                <!-- tarjetas soluciones -->
                <div class="col-12 col-sm-12 col-md-6 col-lg-4">
                    <div class="container card" onclick="showSolutions(this)" data-solution="<?php echo $solucionesUnidad[$i]->ID; ?>">
                        <div class="iconSolU">
                            <?php
                    $iconoSol= get_field('iconos_soluciones',$solucionesUnidad[$i]->ID);
                  ?> <img src="<?php echo $iconoSol['url'] ?>" alt="<?php echo $iconoSol['alt'] ?>"> </div>
                        <div><div class="titleCont">
                            <h3><?php echo get_the_title($solucionesUnidad[$i]->ID); ?></h3>
                          </div>
                            <div class="txtSolUn">
                                <?php echo get_field('descripcion_soluciones',$solucionesUnidad[$i]->ID); ?>
                            </div>
                        </div>
                        <a class="seeMore"><?php echo get_field('boton_versoluciones',$ideGralTexto); ?></a>
                    </div>
                </div>
                <?php } ?>
        </div>
        <?php
            if (count($solucionesUnidad) > 10) {
            ?>
            <a id="moreSl" onclick="cargarMasSol(this)" data-url="<?php echo get_home_url(); ?>/more">
              <span class="a"><?php echo get_field('boton_versoluciones',$ideGralTexto); ?></span>
              <span class="icon dripicons-chevron-down"></span>
            </a>

            <?php
            }
        ?>
    </div>
    <?php
      }
      $clientesUnidad = get_field('clientes_unidades');
      if (!empty($clientesUnidad)) {

     ?>
    <!-- clientes -->
    <div class="container-fluid clientes">
        <div class="triangleCornerTop">
            <svg xmlns="http://www.w3.org/2000/svg" version="1.1" viewBox="0 0 10 10" preserveAspectRatio="none" class="triangleCornerTop">
                <polygon class="fillTriangle" points="0,0 8,0 0,8"></polygon>
            </svg>
        </div>
        <div class="container clientesLogos">
            <div class="row">
                <div class="col-12 col-sm-12 col-md-12 col-lg-8"> </div>
                <div class="col-12 col-sm-12 col-md-12 col-lg-4">
                    <div class="tsec LadoB">
                        <h2 class="stnd"><span><?php _e("[:es]Clientes[:en]Clients"); ?><hr></span></h2>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
            <div class="row">
                <?php
        $clientesUnidad = get_field('clientes_unidades');
        $num = 0;
        for ($a=0; $a < count($clientesUnidad); $a++) {
          $num++;
          $iconoCli= get_field('logotipo_clientes',$clientesUnidad[$a]->ID);
          $middle = ($num == 2) ? 'middleItem' : '';
      ?>
                    <div class="col-6 col-sm-6 col-md-4 col-lg-4">
                        <div class="<?php echo $middle; ?>"> <img src="<?php echo $iconoCli['url'] ?>" alt="<?php echo $iconoCli['alt'] ?>"> </div>
                    </div>
                    <?php
        $num = ($num == 3)?0:$num;
     }
      ?>
            </div>
        </div>
        <div class="triangleCornerBottom">
            <svg xmlns="http://www.w3.org/2000/svg" version="1.1" viewBox="0 0 10 10" preserveAspectRatio="none" class="triangleCornerBottom">
                <polygon class="fillTriangle" points="1,10 10,1 10,10"></polygon>
            </svg>
        </div>
    </div>
    <?php }
      $certificacionUnidad = get_field('certificaciones_unidades');
      if (!empty($certificacionUnidad)) {
        // code...

     ?>
    <!-- Certificaciones -->
    <div class="container-fluid seccionLogos">
        <div class="container certificacionesLogos">
            <div class="row">
                <div class="col-12 col-sm-12 col-md-12 col-lg-8">
                    <div class="tsec LadoA">
                        <h2 class="stnd"><span><?php _e("[:es]Certificaciones[:en]Certifications"); ?><hr></span></h2>
                        <div class="clearfix"></div>
                    </div>
                </div>
                <div class="col-12 col-sm-12 col-md-12 col-lg-4"></div>
            </div>
            <div class="row">
                <?php

        for ($a=0; $a < count($certificacionUnidad); $a++) {
          $iconoCli= get_field('logotipo_ct',$certificacionUnidad[$a]->ID);
      ?>
                    <div class="col-6 col-sm-6 col-md-3 col-lg-3">
                        <div> <img src="<?php echo $iconoCli['url'] ?>" alt="<?php echo $iconoCli['alt'] ?>"> </div>
                    </div>
                    <?php
     }
      ?>
            </div>
        </div>
    </div>
    <?php }
    $tecUnidad = get_field('tecnologias_unidades');
    if(!empty($tecUnidad)){
     ?>
    <!-- tecnologias -->
    <div class="container-fluid seccionLogos tecNL">
        <div class="triangleCornerTop">
            <svg xmlns="http://www.w3.org/2000/svg" version="1.1" viewBox="0 0 10 10" preserveAspectRatio="none" class="triangleCornerTop">
                <polygon class="fillTriangle" points="0,0 8,0 0,8"></polygon>
            </svg>
        </div>
        <div class="container tecnologiasLogos">
            <div class="row">
                <div class="col-12 col-sm-12 col-md-12 col-lg-6"></div>
                <div class="col-12 col-sm-12 col-md-12 col-lg-6">
                    <div class="tsec LadoB">
                        <h2 class="stnd"><span><?php _e("[:es]Tecnologías[:en]Technologies"); ?><hr></span></h2>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
            <div class="row">
                <?php

        for ($b=0; $b < count($tecUnidad); $b++) {
          $iconoCli= get_field('logotipo_ct',$tecUnidad[$b]->ID);
      ?>
                    <div class="col-6 col-sm-6 col-md-3 col-lg-3">
                        <div> <img src="<?php echo $iconoCli['url'] ?>" alt="<?php echo $iconoCli['alt'] ?>"> </div>
                    </div>
                    <?php
     }
      ?>
            </div>
        </div>
        <div class="triangleCornerBottom">
            <svg xmlns="http://www.w3.org/2000/svg" version="1.1" viewBox="0 0 10 10" preserveAspectRatio="none" class="triangleCornerBottom">
                <polygon class="fillTriangle" points="1,10 10,1 10,10"></polygon>
            </svg>
        </div>
    </div>
    <!-- Certificaciones -->
    <?php
  }
    $metoUnidad = get_field('metodologias_unidades');
    if (!empty($metoUnidad)) {

     ?>
    <div class="container-fluid seccionLogos">
        <div class="container metoLogos">
            <div class="row">
                <div class="col-12 col-sm-12 col-md-12 col-lg-8">
                    <div class="tsec LadoA">
                        <h2 class="stnd"><span><?php _e("[:es]METODOLOGÍAS[:en]METHODOLOGIES"); ?><hr></span></h2>
                        <div class="clearfix"></div>
                    </div>
                </div>
                <div class="col-12 col-sm-12 col-md-12 col-lg-4"></div>
            </div>
            <div class="row">
                <?php

        for ($c=0; $c < count($metoUnidad); $c++) {
          $iconoCli= get_field('logotipo_ct',$metoUnidad[$c]->ID);
      ?>
                    <div class="col-6 col-sm-6 col-md-4 col-lg-4">
                        <div> <img src="<?php echo $iconoCli['url'] ?>" alt="<?php echo $iconoCli['title'] ?>"> </div>
                      <p><?php echo get_the_title($metoUnidad[$c]->ID); ?></p>
                    </div>
                    <?php
     }
      ?>
            </div>
        </div>
    </div>
    <!-- contacta-->
    <?php
    }
  $imContacto = get_field('imagen_contacto');
?>
        <div class="contacta" style="background-image: url('<?php echo $imContacto['url']; ?>');">
            <div class="triangleCornerTop">
                <svg xmlns="http://www.w3.org/2000/svg" version="1.1" viewBox="0 0 10 10" preserveAspectRatio="none" class="triangleCornerTop">
                    <polygon class="fillTriangle" points="0,0 8,0 0,8"></polygon>
                </svg>
            </div>
            <div class="container">
                <div class="row">
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                        <h3><?php echo get_field('titulo_contact',1165); ?></h3> </div>
                    <div class="col-12 col-sm-12 col-md-6 col-lg-3">
                        <div class="txtContacta">
                            <p><?php echo get_field('texto_contact',1165); ?></p>
                        </div>
                    </div>
                    <div class="col-12">
                        <div> <a href="<?php echo get_the_permalink(187); ?>"><?php echo get_field('boton_contact',1165); ?></a> </div>
                    </div>
                </div>
            </div>
            <div class="triangleCornerBottom dark">
                <svg xmlns="http://www.w3.org/2000/svg" version="1.1" viewBox="0 0 10 10" preserveAspectRatio="none" class="triangleCornerBottom">
                    <polygon class="fillTriangle" points="1,10 10,1 10,10"></polygon>
                </svg>
            </div>
        </div>


            <?php get_footer(); ?>
