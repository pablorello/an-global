<div class="announcement">
      <div class="container">
          <?php
            $idHome = 200;
              $A_image = get_field('announcement_image',$idHome);
          ?>
        <div class="row">
            <div class="col-12 col-sm-12 col-md-6 col-lg-6 no-paddingR order-0 order-sm-0 order-md-1 order-lg-1">
                <div class="imageCE">
                    <img class="img-fluid" src="<?php echo $A_image['url']; ?>" alt="An Global Announcement">
                </div>
            </div>
            <div class="row">
                
            </div>
            <div class="whiteBG col-12 col-sm-12 col-md-6 col-lg-6 order-1 order-sm-1 order-md-1 order-lg-1">
            <div class="eachInfOfP">
                <div class="descofP">
                    <p><?php echo get_field('announcement_text', $idHome); ?></p>
                </div>
                <div class="btnMore">
                    <a href="<?php echo get_permalink('2690'); ?>" data-solution="704">More</a>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>                            
        </div><!-- row -->                  
      </div><!-- container -->
    </div><!-- announcement -->