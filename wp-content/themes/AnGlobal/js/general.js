$(function(){
  $('.stnd').each(function(){
		var ancho = $(this).find('span').width();
		ancho += 150;
		$(this).find('hr').width(Math.round(ancho));
	});
});
$(document).ready(function(){

$('[data-toggle="tooltip"]').tooltip();
$('.svgImage').svgInject();
$(".gallery_carousel").each(function(){
    $(this).owlCarousel({
      margin:10,
      navText: ['<span class="icon dripicons-chevron-left"></span>','<span class="icon dripicons-chevron-right"></span>'],
      responsiveClass:true,
      responsive:{
          0:{
              items:2,
              nav:false,
              dots: true
          },
          767:{
              items:3,
              nav:true
          },
          1025:{
              items:4,
              nav:true
          }
      },
      loop:false
    });
  });
  $(".casos").each(function(){
      $(this).owlCarousel({
        margin:10,
        navText: ['<span class="icon dripicons-chevron-left"></span>','<span class="icon dripicons-chevron-right"></span>'],
        responsiveClass:true,
        responsive:{
            0:{
                items:1,
                nav:false,
                dots: true
            },
            767:{
                items:1,
                nav:true
            },
            1025:{
                items:1,
                nav:true
            }
        },
        loop:true
      });
    });
  /*$(".owl-carousel").owlCarousel({
    margin:10,
    navText: ['<span class="icon dripicons-chevron-left"></span>','<span class="icon dripicons-chevron-right"></span>'],
    responsiveClass:true,
    responsive:{
        0:{
            items:1,
            nav:true
        },
        768:{
            items:2,
            nav:true
        },
        1025:{
            items:3,
            nav:true
        }
    },
    loop:true
  });*/
//tittleSeccion();
if($(window).width() < 1025){
  $('.bu li .thisA').each(function(){
    $(this).removeAttr('href');
  });
}
var stickyTop = $('.header').height();


});
var vid = document.getElementById("myVideo");

function playVid(e) {
    vid.play();
    $(e).toggle();
}
function pauseVid() {
    vid.pause();
    $('.videoPlay').toggle();
}
$( window ).resize(function() {
  //$('.submenu').removeClass('activoSm');
  //tittleSeccion();
  if($(window).width() < 1025){
    $('.bu li .thisA').each(function(){
      $(this).removeAttr('href');
    });
  }else{
    $('.bu li .thisA').each(function(){
      var enlace = $(this).attr('data-url');
      $(this).attr('href',enlace);
    });
  }
});
function tittleSeccion(){
  $('.tittleSeccion').each(function(){
    var ancho = $(this).find('h2').width();
    $(this).width(ancho);
  });
}

function showMenu(e){
  if($(window).width() < 1025){
    var hijo = $(e).children('.submenu');
    $(e).siblings('li').each(function(){
      $(this).find('.submenu').removeClass('activoSm');
    });
    hijo.toggleClass('activoSm');
    $(e).toggleClass('downArrow');
  }
}

function submenuOut(){
/*  $('.submenu').stop(true, true).delay(200).fadeOut(500);*/
}

function Oficinas(e){
  var titulo = $(e).attr('data-title');
  $('#actual').toggle().html(titulo);
  $('.selectContact').toggleClass('activar');
  var ide = $(e).attr('id');
  var url = $(e).parents('ul').attr('data-url');
  var jqxhr = $.ajax(url+'?ide='+ide, 'jsonp')
  jqxhr.done(function(jsondata) {
    if(jsondata != null){
      $('.direccion').html(jsondata[0].direccion);
      $('.verGM').attr('href',jsondata[0].maps);
      $('.correo').html(jsondata[0].correo);
      $('.tel').html(jsondata[0].telefono);
    }
  });
}

function showSolutions(e){
  var ide = $(e).attr('data-solution');
  var url = $(e).parents('.solutionsDatos').attr('data-url');
  var practica = $(e).parents('.solutionsDatos').attr('data-practica');
  var jqxhr = $.ajax(url+'?ide='+ide+'&unidad='+practica, 'jsonp')
  jqxhr.done(function(jsondata) {

    if(jsondata != null){
      var listBen = '';
      var flechas = '';
      var pp = jsondata[0].prev;
      var nn = jsondata[0].next;
      var previ = (jsondata[0].prev != null)? '<span class="icon dripicons-chevron-left" onclick="trigg('+$.trim(pp)+')"></span>':'';
      var nexti = (jsondata[0].next != null)?'<span class="icon dripicons-chevron-right" onclick="trigg('+$.trim(nn)+')"></span>':'';

      for (a in jsondata[0].servicios) {
        listBen += '<li><span>'+jsondata[0].servicios[a].nombre+'</span></li>';
      }
      var listSol = '';
        for (a in jsondata[0].beneficios) {
          listSol += '<li><div class="ttListB"><i class="icon dripicons-'+jsondata[0].beneficios[a].icBen+'"></i><span>'+jsondata[0].beneficios[a].nombre+'</span></div></li>';
        }
      var titulo = '<span class="cerrar icon dripicons-cross" onclick="cerrarPop(this)"></span><div class="row gris">'+
                      '<div class="col-3 col-sm-3 col-md-2 col-lg-2 col-xl-2"><img src="'+jsondata[0].icono+'"></div>'+
                      '<div class="col-9 col-sm-9 col-md-10 col-lg-10 col-xl-10"><h2>'+jsondata[0].titulo+'</h2></div>'+
                    '</div>';
      var descripcion = '<div class="row">'+
                          '<div class="col-12 col-sm-12 col-md-1 col-lg-1 col-xl-1"></div>'+
                          '<div class="col-12 col-sm-12 col-md-11 col-lg-11 col-xl-11">'+
                            '<div class="overflowHidden"><div class="copete" style="background-image: linear-gradient(104deg,'+jsondata[0].colora+','+jsondata[0].colorb+');"></div><div class="descPop" style="background-image: linear-gradient(104deg,'+jsondata[0].colora+','+jsondata[0].colorb+');">'+
                              '<div class="row">'+
                                '<div class="col-1"></div>'+
                                '<div class="col-11 col-sm-11 col-md-10 col-lg-10">'+
                                  '<div class="txtPop">'+jsondata[0].descripcion+'</div>'+
                                '</div>'+
                                '<div class="col-12 col-sm-12 col-md-1 col-lg-1"></div>'+
                              '</div>'+
                            '</div>'+
                            '</div>'+
                          '</div>'+
                        '</div>';
      var benefServ = '<div class="row">'+
                          '<div class="col-12 col-sm-12 col-md-1 col-lg-1 col-xl-1"></div>'+
                          '<div class="beneficios col-12 col-sm-12 col-md-5 col-lg-5 col-xl-5">'+
                              '<h3>'+jsondata[0].benTit+'</h3>'+
                              '<ul>'+listSol+'</ul>'+
                          '</div>'+
                          '<div class="servicios col-12 col-sm-12 col-md-5 col-lg-5 col-xl-5">'+
                              '<h3>'+jsondata[0].servTit+'</h3>'+
                              '<ul>'+listBen+'</ul>'+
                          '</div>'+
                          '<div class="col-12 col-sm-12 col-md-1 col-lg-1 col-xl-1"></div>'+
                        '</div>';
          $('#pop #modalBox').html(titulo+descripcion+benefServ+'<div class="flechasOv">'+previ+nexti+'</div>').attr('style',"background-image: url('"+jsondata[0].fondo+"')");
          //window.history.pushState('', jsondata[0].titulo , jsondata[0].link);
          $('#pop').modal();
    }
  });
}
function showSTeam(e){
  var ide = $(e).attr('data-solution');
  var url = $(e).parents('.teamGlobal').attr('data-url');
  var practica = $(e).attr('data-practica');
  var jqxhr = $.ajax(url+'?ide='+ide+'&unidad='+practica, 'jsonp')
  jqxhr.done(function(jsondata) {

    if(jsondata != null){
      var flechas = '';
      var pp = jsondata[0].prev;
      var nn = jsondata[0].next;
      var previ = (jsondata[0].prev != null)? '<span class="icon dripicons-chevron-left" onclick="trigg('+$.trim(pp)+')"></span>':'';
      var nexti = (jsondata[0].next != null)?'<span class="icon dripicons-chevron-right" onclick="trigg('+$.trim(nn)+')"></span>':'';
      var titulo = '<span class="cerrar icon dripicons-cross" onclick="cerrarPop(this)"></span>' +
                  '<div class="row gris_team">'+
                      '<div class="col-12 col-sm-12 col-md-1 col-lg-1 col-xl-1"></div>'+
                      '<div class="col-11 col-sm-11 col-md-11 col-lg-11 col-xl-11"><h2>'+jsondata[0].titulo+'</h2><h4>'+jsondata[0].puesto+'</h4></div>'+

                    '</div>';
      var descripcion = '<div class="row">'+
                          '<div class="col-12 col-sm-12 col-md-1 col-lg-1 col-xl-1"></div>'+
                          '<div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6 order-2 order-sm-2 order-md-0">'+
                            '<div class="descPersona">'+jsondata[0].descripcion+'</div>'+
                          '</div>'+
                          '<div class="col-12 mobile-only order-1 order-sm-1 order-md-3"><div class="col-12 align-self-end logotipoTeam"><img src="'+jsondata[0].practica+'"/></div></div>'+
                          '<div id="teamPhoto" class="col-12 col-sm-12 col-md-5 col-lg-5 col-xl-5 order-0 order-sm-0 order-md-0">'+
                            '<img src="'+jsondata[0].fotografia+'"/>'+
                          '</div>'+
                        '</div>'+
                        '<div class="row desktop-only"><div class="col-5 col-sm-5 col-md-5 col-lg-9 col-xl-9"></div><div class="col-2 align-self-end logotipoTeam"><img src="'+jsondata[0].practica+'"/></div></div>';
          $('#pop #modalBox').html(titulo+descripcion+'<div class="flechasOv">'+previ+nexti+'</div>');
          //window.history.pushState('', jsondata[0].titulo , jsondata[0].link);
          $('#pop').modal();
    }
  });
}


function cerrarPop(e){
  $(e).siblings('.container').html('');
  $(e).parents('#pop').modal('toggle');
}

function mapa(e){
  var pais = $(e).attr('id');
  var url = $(e).parents('ul').attr('data-url');
  $(e).parents('ul').find('li').each(function(){
    $(this).removeClass('activoPais');
  });
  $('.cambioMapa').append('<div class="cargador"><div class="loader"></div></div>');
  var jqxhr = $.ajax(url+'?ide='+pais, 'jsonp')
  jqxhr.done(function(jsondata) {
    $(e).addClass('activoPais');
      if(jsondata != null){
        var mapaDispositivo = ($(window).width() >= 768)? jsondata[0].mapa : jsondata[0].mapaMob;
        $('.cambioMapa img').attr('src',mapaDispositivo);
        $('.infoOficina h6').html(jsondata[0].titulo);
        $('.dirM .textM').html(jsondata[0].direccion);
        $('.verGM').attr('href',jsondata[0].maps);
        $('.mailM .textM').html(jsondata[0].mail);
        $('.telM .textM').html(jsondata[0].telefono);
      }
      $('.infoOficina').addClass('activeOficina');
  })
  .done(function(){
    $('.cargador').remove();

  });
}

function cerrarMapa(e){
    $('.infoOficina').removeClass('activeOficina');
}

  var video1 = document.getElementById('video_background');
  //video1.load();

  var decimal_places = 1;
  var decimal_factor = decimal_places === 0 ? 1 : decimal_places * 10;

function animarNums(e){
  $('.ttDatos'+e).each(function(){
    var numberTotal = $(this).attr('data-numAn');
    $(this).find('p strong').stop().animateNumber(
      {
        number: numberTotal * decimal_factor,
        numberStep: function(now, tween) {
          var floored_number = Math.floor(now) / decimal_factor,
              target = $(tween.elem);
          target.text(floored_number);
        }
      },
      1500
    );
  });
}
/*$(window).scroll(function(){

				var windowHeight = $(window).scrollTop();
        console.log(windowHeight);
				$(".investor").each(function(){
          var miAltura = $(this).offset();
          console.log(miAltura);
          if (windowHeight >= 100 ) {
            animarNums();
          }
        });

				/*contenido2 = contenido2.top;

				    if(windowHeight >= contenido2  ){
					$('#aviso').fadeIn(500);

				    }else{
					$('#aviso').fadeOut(500);
				}*/
		              // });
                    var check = true;
                    var checkb = true;
                   $(window).scroll(function () {

                   var wScroll = $(this).scrollTop();
                      $(".investor").each(function(){
                         if (($(this).offset().top -300) <= wScroll && check) {
                                 animarNums('a');
                                 check = false;
                         }
                       });
                       $(".investorB").each(function(){
                          if (($(this).offset().top -300) <= wScroll && checkb) {
                                  animarNums('b');
                                  checkb = false;
                          }
                        });
                });

function trigg(e){
  $("div[data-solution='"+e+"']").click();
}
