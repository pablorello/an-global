function showCase(e){
  var ide = $(e).attr('data-solution');
  var url = $(e).parents('.casosEx').attr('data-url');
  var practica = $(e).parents('.casosEx').attr('data-unidad');
  var jqxhr = $.ajax(url+'?ide='+ide+'&unidad='+practica, 'jsonp')
  console.log(url+'?ide='+ide+'&unidad='+practica, 'jsonp');
  jqxhr.done(function(jsondata) {
    if (jsondata != null) {
      var media = "";
      var flechas = '';
      var pp = jsondata[0].prev;
      var nn = jsondata[0].next;
      var previ = (jsondata[0].prev != null)? '<span class="icon dripicons-chevron-left" onclick="trigg('+$.trim(pp)+')"></span>':'';
      var nexti = (jsondata[0].next != null)?'<span class="icon dripicons-chevron-right" onclick="trigg('+$.trim(nn)+')"></span>':'';
      if (jsondata[0].video_mp4 != false) {
        media = '<video id="video_case">'+
                  '<source src="'+jsondata[0].video_mp4+'" type="video/mp4">'+
                  '<source src="'+jsondata[0].video_ogg+'" type="video/ogg">'+
                  '<source src="'+jsondata[0].video_webm+'" type="video/webm">'+
                '</video>'
      }else if (jsondata[0].image != null) {
        media ='<div class="imageCaseM"><img src="'+jsondata[0].image+'"/></div>'
      }
      var soluciones = "";
      for (a in jsondata[0].soluciones) {
        var imgSol = (jsondata[0].soluciones[a].iconoS != null)? jsondata[0].soluciones[a].iconoS:'';
        soluciones += '<div class="col-12 col-sm-12 col-md-6 col-lg-6"> <div class="container card">'+
                        '<div class="iconSolU"><img src="'+imgSol+'"/></div>'+
                        '<div><div class="titleCont">'+
                            '<h3>'+jsondata[0].soluciones[a].tituloS+'</h3>'+
                          '</div></div>'+
                        '<a class="seeMore" href="'+jsondata[0].soluciones[a].link+'">'+jsondata[0].boton+'</a>'+
                    '</div>'+
                '</div>'
      }
      var titulo = '<span class="cerrar icon dripicons-cross" onclick="cerrarPop(this)"></span><div class="row gris titCE">'+
                      '<div class="col-12 col-sm-12 col-md-1 col-lg-1 col-xl-1"></div>'+
                      '<div class="col-12 col-sm-12 col-md-10 col-lg-10 col-xl-10"><h2>'+jsondata[0].titulo+'</h2></div>'+
                      '<div class="col-12 col-sm-12 col-md-1 col-lg-1 col-xl-1"></div>'+
                    '</div>';
      var imageM = '<div class="row">'+
                          '<div class="col-12 col-sm-12 col-md-1 col-lg-1 col-xl-1"></div>'+
                          '<div class="col-12 col-sm-12 col-md-10 col-lg-10 col-xl-10">'+
                          '<div class="mediaCase">'+media+'</div>'+
                          '</div>'+
                          '<div class="col-12 col-sm-12 col-md-1 col-lg-1 col-xl-1"></div>'+
                        '</div>';
      var proyecto = '<div class="row espR">'+
                          '<div class="col-12 col-sm-12 col-md-12 col-lg-1 col-xl-1"></div>'+
                          '<div class="col-12 col-sm-12 col-md-12 col-lg-2 col-xl-2">'+
                          '<span class="titCaseM">'+jsondata[0].titProy+'</span>'+
                          '</div>'+
                          '<div class="col-12 col-sm-12 col-md-12 col-lg-6 col-xl-6">'+
                          '<div class="textoCaseM">'+jsondata[0].proyecto+'</div>'+
                          '</div>'+
                          '<div class="col-12 col-sm-12 col-md-12 col-lg-2 col-xl-2">'+
                          '<div class="logoCaseM"><img src="'+jsondata[0].cliente+'"/></div>'+
                          '</div>'+
                          '<div class="col-12 col-sm-12 col-md-1 col-lg-1 col-xl-1"></div>'+
                        '</div>';
      var obj = '<div class="row eachRowM espR">'+
                          '<div class="col-12 col-sm-12 col-md-12 col-lg-1 col-xl-1"></div>'+
                          '<div class="col-12 col-sm-12 col-md-12 col-lg-2 col-xl-2">'+
                          '<span class="titCaseM">'+jsondata[0].titObj+'</span>'+
                          '</div>'+
                          '<div class="col-12 col-sm-12 col-md-12 col-lg-8 col-xl-8">'+
                          '<div class="textoCaseM">'+jsondata[0].objetivo+'</div>'+
                          '</div>'+
                          '<div class="col-12 col-sm-12 col-md-12 col-lg-1 col-xl-1"></div>'+
                        '</div>';
      var sol = '<div class="row eachRowM espR">'+
                          '<div class="col-12 col-sm-12 col-md-12 col-lg-1 col-xl-1"></div>'+
                          '<div class="col-12 col-sm-12 col-md-12 col-lg-2 col-xl-2">'+
                          '<span class="titCaseM">'+jsondata[0].titSol+'</span>'+
                          '</div>'+
                          '<div class="col-12 col-sm-12 col-md-12 col-lg-8 col-xl-8">'+
                          '<div class="row">'+soluciones+'</div>'+
                          '</div>'+
                          '<div class="col-12 col-sm-12 col-md-12 col-lg-1 col-xl-1"></div>'+
                        '</div>';

                        $('#pop #modalBox').html(titulo+imageM+proyecto+obj+sol+'<div class="flechasOv">'+previ+nexti+'</div>');
                        $('#pop').modal();
    }
  });
}

function cargarMasSol(e){
  var url = $(e).attr('data-url');
  var practica = $(e).parents('.solutionsDatos').attr('data-practica');
  var total = $(e).parents('.solutionsDatos').attr('data-total');
  var jqxhr = $.ajax(url+'?total='+total+'&unidad='+practica, 'jsonp')
  console.log(url+'?total='+total+'&unidad='+practica);
  jqxhr.done(function(jsondata) {
    if(jsondata != null){
      for( a in jsondata){
        $('.inicial').append('<div class="col-12 col-sm-12 col-md-6 col-lg-4">'+
        '<div class="container card" onclick="showSolutions(this)" data-solution="'+jsondata[a].id+'">'+
                '<div class="iconSolU">'+
                    '<img src="'+jsondata[a].icono+'" alt="" class="svgImage"> '+
                '</div>'+
                '<div>'+
                  '<div class="titleCont">'+
                    '<h3>'+jsondata[a].titulo+'</h3>'+
                  '</div>'+
                    '<div class="txtSolUn">'+jsondata[a].desc+
                    '</div>'+
                '</div>'+
                '<a class="seeMore">'+jsondata[a].boton+'</a>'+
            '</div>'+
        '</div>');
      }
      if (jsondata[0].totalF != null) {
        $(e).parents('.solutionsDatos').attr('data-total',jsondata[0].totalF);
      }else{
        $('#moreSl').hide();
      }

    }
  });
}
