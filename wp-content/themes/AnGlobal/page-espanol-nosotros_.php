<?php get_header(); ?>
<div class="bannerInterna">
  <?php
    $img_desk= get_field('imagen_nosotros_desk');
    $img_mob= get_field('imagen_nosotros_mobile');
  ?>
  <img src="<?php echo $img_desk['url'] ?>" alt="<?php echo $img_desk['alt'] ?>" class="d-none d-sm-none d-md-none d-lg-block d-xl-block">
  <img src="<?php echo $img_mob['url'] ?>" alt="<?php echo $img_desk['alt'] ?>" class="d-block d-sm-block d-md-block d-lg-none d-xl-none">
  <div class="container">
    <h1><?php echo get_the_title(); ?></h1>
    <?php echo get_field('texto_nosotros_banner'); ?>
  </div>
</div>
<div class="fact">
  <div class="container">
    <div class="row">

      <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
        <h2><?php echo get_field('titulo_innovemos'); ?></h2>
      </div>
      <div class="col-12 col-sm-12 col-md-12 col-lg-1 col-xl-1"></div>
      <div class="col-12 col-sm-12 col-md-12 col-lg-5 col-xl-5">
        <?php
          $iconoFact = get_field('icono_innovemos');
        ?>
        <div class="icono">
          <img src="<?php echo $iconoFact['url'] ?>" alt="<?php echo $iconoFact['alt'] ?>" class="svgImage">
        </div>
      </div>
      <div class="col-12 col-sm-12 col-md-12 col-lg-5 col-xl-5">
        <?php echo get_field('texto_innovemos'); ?>
      </div>
      <div class="col-12 col-sm-12 col-md-12 col-lg-1 col-xl-1"></div>
    </div>
  </div>
</div>
<div class="historia">
  <div class="container">
    <div class="row">

      <div class="col-12 col-sm-12 col-md-12 col-lg-4 col-xl-4">
        <h2><?php echo get_field('titulo_historia'); ?></h2>
        <?php echo get_field('texto_historia'); ?>
      </div>

      <div class="col-12 col-sm-12 col-md-12 col-lg-2 col-xl-2"></div>
      <div class="col-12 col-sm-12 col-md-12 col-lg-5 col-xl-5">
        <?php
        $args = array(
                'post_type' => 'historia',
                'order' => 'ASC'
                );
                $populares = query_posts($args);
                $conteoProyectos= count($populares);
                $a= 0;
                $calseArr = array('a','b');
                for ($i=0; $i < $conteoProyectos ; $i++) {
                  $primo = $a++ %2;
                ?>
                <div class="row">
                  <div class="col-3 col-sm-3 col-md-3 col-lg-6 col-xl-6">
                      <?php
                        echo ($primo == 0)?'<strong class="anio'.$calseArr[$primo]'">'.get_the_title($populares[$i]->ID).'</strong>':'<div> class="texto'.$calseArr[$primo]'"'.get_field('contenido_historia',$populares[$i]->ID).'</div>';
                      ?>
                  </div>
                  <div class="col-3 col-sm-3 col-md-3 col-lg-6 col-xl-6">
                    <?php
                      echo ($primo == 0)? '<div class="texto'.$calseArr[$primo]'">'.get_field('contenido_historia',$populares[$i]->ID).'</div>':'<strong class="anio'.$calseArr[$primo]'">'.get_the_title($populares[$i]->ID).'</strong>';
                    ?>
                  </div>
                </div>
                <?php
                }
        ?>
      </div>
      <div class="col-12 col-sm-12 col-md-12 col-lg-1 col-xl-1"></div>
    </div>
  </div>
</div>
<div class="insights">
    <div class="container">
      <?php
      $argsb = array(
              'post_type' => 'insights',
              'order' => 'ASC'
              );
              $popularesb = query_posts($argsb);
              $conteoProyectosb= count($popularesb);
    		  $chunkResult = array_chunk($popularesb, 2);
              $idesTotal = array();
              $chunkResult[0][2]["ID"]='especial';
    			for ($a=0; $a < count($chunkResult); $a++) {
           $idesTotal =  array_merge_recursive($idesTotal,$chunkResult[$a]);
          }
          $conteoIdes = count($idesTotal);
          $numeroFor = round($conteoIdes / 7);
          $numActual = 0;
          $principal = 0;
          $newIdes = array_chunk($idesTotal, 7);
          $columnas = array(4,4,4,3,2,3,4);
          $posicion = array('order-2 order-sm-2 order-md-1 order-lg-1 order-xl-1','order-3 order-sm-3 order-md-2 order-lg-2 order-xl-2','order-1 order-sm-1 order-md-3 order-lg-3 order-xl-3','order-4 order-sm-4 order-md-4 order-lg-4 order-xl-4','order-5 order-sm-5 order-md-5 order-lg-5 order-xl-5','order-6 order-sm-6 order-md-6 order-lg-6 order-xl-6','order-7 order-sm-7 order-md-7 order-lg-7 order-xl-7');
            for ($e=0; $e < $numeroFor; $e++) {

              $principal += 7;
              $numTotal = ($principal > $conteoIdes)? $conteoIdes - ($principal - 7) : 7;
              ?>
            <div class="row">
              <?php
              for ($f=0; $f < $numTotal; $f++) {
                //var_dump($newIdes[$e][$f]->ID);
                  ?>
                  <div class="col-12 col-sm-12 col-md-<?php echo $columnas[$f] ?> col-lg-<?php echo $columnas[$f] ?> col-xl-<?php echo $columnas[$f]; echo ($e == 0)? $posicion[$f] : ''; ?> >">
                          <?php
                          if ($newIdes[$e][$f]->ID == null) {
                            ?>

                                <div class="tittleSeccion LadoA">
                                                <h2><?php echo get_field('titulo_insights'); ?></h2>
                                                <div class="clearfix"></div>
                                            </div>
                                            <div class="txtInsTitle">
                                              <?php echo get_field('texto_insights'); ?>
                                            </div>
                            <?php
                          }else{
                            ?>
                                <div class="eachIns">
                                      <strong><?php echo get_the_title($newIdes[$e][$f]->ID); ?></strong>
                                      <div class="textIns">
                                        <?php echo get_field('contenido_historia',$newIdes[$e][$f]->ID); ?>
                                      </div>
                                  </div>
                            <?php
                          }
                          ?>
                    </div>
                  <?php
                }

              ?>
            </div>
              <?php
            }
                ?>
    </div>
</div>
<div class="principios">
  <div class="container">
    <div class="row">
      <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
        <h2><?php echo get_field('titulo_seccion_princ'); ?></h2>
      </div>
      <?php
      $argsd = array(
              'post_type' => 'principios'
              );
              $popularesd = query_posts($argsd);
              $conteoProyectosd= count($popularesd);
        for ($g=0; $g < $conteoProyectosd; $g++) {
          $imagen = get_field('icono_princ',$popularesd[$g]->ID);
          ?>
          <div class="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">asdsadas
            <div class="iconoPrin">
              <?php if(!empty($imagen)){ ?>
                <img src="<?php echo $imagen['url'] ?>" alt="<?php echo $imagen['alt'] ?>" class="svgImage">
              <?php } ?>
            </div>
            <h3><?php echo get_field('titulo_princ',$popularesd[$g]->ID); ?></h3>
            <div class="contentPrincipios">
              <?php echo get_field('descripcion_princ',$popularesd[$g]->ID); ?>
            </div>
          </div>
          <?php
        }
      ?>
    </div>
  </div>
</div>
<div class="practica">
  <div class="container">
    <div class="row">
      <div class="col-12 col-sm-12 col-md-5 col-lg-5 col-xl-5">
        <div class="tittleSeccion LadoA">
                        <h2><?php echo get_field('titulo_seccion_practicas'); ?></h2>
                        <div class="clearfix"></div>
                    </div>
                    <div class="txtInsTitle">
                      <?php echo get_field('texto_seccion_practicas'); ?>
                    </div>
      </div>
      <div class="col-12 col-sm-12 col-md-1 col-lg-1 col-xl-1"></div>
      <div class="col-12 col-sm-12 col-md-7 col-lg-7 col-xl-6">
        <div class="row">
          <?php
          $argsc = array(
                  'post_type' => 'business_units',
                  'order' => 'ASC'
                  );
                  $popularesc = query_posts($argsc);
                  $conteoProyectosc= count($popularesc);
                  for ($h=0; $h < $conteoProyectosc; $h++) {
                    ?>
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                      <div id="<?php echo $popularesc[$h]->post_name;?>" class="eachPractica">
                        <?php
                          $imgUnidad = get_field('logotipo_experience',$popularesc[$h]->ID);
                          $claimUnidad = get_field('claim_unidades',$popularesc[$h]->ID);
                          if(!empty($imgUnidad) && !empty($claimUnidad)){
                        ?>
                        <img src="<?php echo $imgUnidad['url']?>" alt="<?php echo $imgUnidad['alt']?>" class="svgImage">
                        <img src="<?php echo $claimUnidad['url']?>" alt="<?php echo $claimUnidad['alt']?>" class="svgImage">
                      <?php } ?>
                      </div>
                    </div>
                    <?php
                  }
                ?>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="contacta">
    <div class="container">
        <div class="row">
            <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                <div class="txtContacta">
                    <h3>CONTACTA CON UN<br>ESPECIALISTA</h3></div>
            </div>
            <div class="col-12 col-sm-12 col-md-6 col-lg-3">
                <div class="txtContacta">
                    <p>Somos tu próximo socio de negocios</p></div>
            </div>
            <div class="col-12 col-sm-12 col-md-12 col-lg-12">
                    <a href="">Conócenos</a>
            </div>
        </div>
    </div>
</div>
<style media="screen">
  <?php
  var_dump($conteoProyectosc);
    for ($j=0; $j < $conteoProyectosc; $j++) {
      $color = get_field('color_de_unidad',$popularesc[$j]->ID);
      echo "#".$popularesc[$j]->post_name.' svg path{fill: '.$color.';stroke: '.$color.'; }';
    }
  ?>
</style>
<?php get_footer(); ?>
